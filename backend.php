<?php
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
require_once($yii);

require_once(dirname( __FILE__ ) . '/protected/helpers/EarlyErrorHandler.php');
require_once(dirname( __FILE__ ) . '/protected/helpers/HelperFunctions.php');

function getMainConfig()
{
    $configMain = include(dirname( __FILE__ ) . '/protected/backend/config/main.php');

    $configLocal = array();
    if (IS_DEVELOPMENT && file_exists(dirname(__FILE__) . '/protected/backend/config/main.local.php'))
    {
        $configLocal = include(dirname( __FILE__ ) . '/protected/backend/config/main.local.php');
    }

    // If $configLocal exists it will overwrite the main config settings
    return array_replace_recursive($configMain, $configLocal);
}

function getDbConfig()
{
    // If the user has set a database.local.php file then include that (overwrite the main db config)
    if (file_exists(dirname(__FILE__) . '/protected/backend/config/database.local.php'))
    {
        $dbConfig = require_once(dirname( __FILE__ ) . '/protected/backend/config/database.local.php');
    }
    // If backend config exists then use it otherwise use the frontend DB
    elseif (file_exists(dirname(__FILE__) . '/protected/backend/config/database.php'))
    {
        $dbConfig = require_once(dirname( __FILE__ ) . '/protected/backend/config/database.php');
    }
    else
    {
        if (file_exists(dirname(__FILE__) . '/protected/config/database.local.php'))
        {
            $dbConfig = require_once(dirname( __FILE__ ) . '/protected/config/database.local.php');
        }
        else
        {
            $dbConfig = require_once(dirname( __FILE__ ) . '/protected/config/database.php');
        }
    }

    return $dbConfig;
}

$configMain = getMainConfig();
$dbConfig = getDbConfig();

// Run application
$config = CMap::mergeArray($configMain, $dbConfig);
Yii::createWebApplication($config)->run();

// Set these session vars to be used in main CSS file
$_SESSION['MAIN_ADMIN_HEX'] = Yii::app()->webSettings->getValue('MAIN_ADMIN_HEX');
$_SESSION['MAIN_ADMIN_RGB'] = Yii::app()->webSettings->getValue('MAIN_ADMIN_RGB');