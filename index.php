<?php
// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
require_once($yii);

require_once(dirname( __FILE__ ) . '/protected/helpers/HelperFunctions.php');
require_once(dirname( __FILE__ ) . '/protected/helpers/EarlyErrorHandler.php');

function getMainConfig()
{
    $config = include(dirname( __FILE__ ) . '/protected/config/main.php');

    $configLocal = array();
    if (file_exists(dirname(__FILE__) . '/protected/config/main.local.php'))
    {
        $configLocal = include(dirname( __FILE__ ) . '/protected/config/main.local.php');
    }

    // If $configLocal exists it will overwrite the main config settings
    return array_replace_recursive($config, $configLocal);
}

function getDbConfig()
{
    // If the user has set a database.local.php file then include that (overwrite the main db config)
    if (file_exists(dirname(__FILE__) . '/protected/config/database.local.php'))
    {
        return include(dirname( __FILE__ ) . '/protected/config/database.local.php');
    }
    else
    {
        return include(dirname( __FILE__ ) . '/protected/config/database.php');
    }
}

$configMain = getMainConfig();
$dbConfig = getDbConfig();

// Run application
$config = CMap::mergeArray($configMain, $dbConfig);
Yii::createWebApplication($config)->run();