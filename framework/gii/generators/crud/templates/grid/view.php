<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
?>

$this->menu=array(
	array('label'=>'All <?php echo $this->pluralize($this->class2name($this->modelClass)); ?>', 'url'=>array('index')),
	array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
        array('label'=>'View <?php echo $this->modelClass; ?>', 'url'=>array('view', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
	array('label'=>'Update <?php echo $this->modelClass; ?>', 'url'=>array('update', 'id'=>$model->getPrimaryKey())),
	array('label'=>'Delete <?php echo $this->modelClass; ?>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View <?php echo $this->modelClass; ?></h1>

<?php echo "<?php"; ?> $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
<?php
foreach($this->tableSchema->columns as $column)
	echo (($column->name !== 'Created' && $column->name !== 'Modified') ? "\t'".$column->name."',\n" : '');
?>
            array(
                'label' => 'Modified',
                'type' => 'raw',
                'value' => (!empty($model->Modified) ? date('d-M-Y', strtotime($model->Modified)) : null),
            ),
            array(
                'label' => 'Created',
                'type' => 'raw',
                'value' => date('d-M-Y', strtotime($model->Created)),
            ),
	),
)); ?>
