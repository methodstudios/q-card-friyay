<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $dataProvider CActiveDataProvider */
$this->showSwitchTiles=true;
$this->showSearchForm=true;

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
?>

$this->menu=array(
    array('label'=>'All <?php echo $this->pluralize($this->class2name($this->modelClass)); ?>', 'url'=>array('index'), 'itemOptions' => array('class' => 'active')),
    array('label'=>'Create <?php echo $this->modelClass; ?>', 'url'=>array('create')),
);
?>

<h1>All <?php echo $label; ?></h1>

<div class="clear"></div>

<div class="view-tiles" style="display:none;">
<?php echo "<?php"; ?> $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'_view',
            'afterAjaxUpdate'=>'initiateIsotope',
            'pager'=>array(
                'header'=>'',
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
            ),
    )); ?>
</div>

<div class="view-list">
    <?php echo "<?php"; ?>
    echo $this->getBulkActions();

    $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $dataProvider,
            //'filter' => $model,
            'columns' => array(
                array(
                    'type' => 'raw',
                    'value' => 'BackendController::getListCheckbox($data->getPrimaryKey())',
                ),
                <?php
                foreach($this->tableSchema->columns as $column)
                    echo (($column->name !== 'Created' && $column->name !== 'Modified') ? "\t\t'".$column->name."',\n" : '');
                ?>
                array(
                    'name' => 'Modified',
                    'type' => 'raw',
                    'value' => '(!empty($data->Modified) ? date("d-M-Y", strtotime($data->Modified)) : null)',
                ),
                array(
                    'name' => 'Created',
                    'type' => 'raw',
                    'value' => 'date("d-M-Y", strtotime($data->Created))',
                ),
                array(
                    'name' => '',
                    'type' => 'raw',
                    'htmlOptions' => array('class' => 'actionLinks'),
                    'value' => '
                        CHtml::link("View", array("view","id"=>$data->getPrimaryKey())) . "&nbsp;&nbsp;|&nbsp;&nbsp;" .
                        CHtml::link("Edit", array("update","id"=>$data->getPrimaryKey())) . "&nbsp;&nbsp;|&nbsp;&nbsp;" .
                        CHtml::link("Delete", array("delete","id"=>$data->getPrimaryKey()), array("onclick" => "deleteItem(\"" . $data->getPrimaryKey() . "\", \"" . BackendController::getControllerUrl() . "\"); return false;"))',
                ),
            ),
            'pager'=>array(
                'header'=>'',
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
            ),
    )); ?>
</div>