<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $data <?php echo $this->getModelClass(); ?> */
?>

<div class="view">

<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if($column->isPrimaryKey)
		continue;
	if(++$count==7)
		echo "\t<?php /*\n";
	echo "\t<p><b><?php echo CHtml::encode(\$data->getAttributeLabel('{$column->name}')); ?>:</b>\n";
	echo "\t<?php echo CHtml::encode(\$data->{$column->name}); ?>\n\t</p>\n\n";
}
if($count>=7)
	echo "\t*/ ?>\n";

echo "\n";
echo '  <p>
            <a href="<?php echo $this->createUrl(\'view\', array(\'id\' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-eye-open"></span> View
                </button>
            </a>
            <a href="<?php echo $this->createUrl(\'update\', array(\'id\' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> Edit
                </button>
            </a>
            <a href="#" onclick="deleteItem(\'<?php echo $data->getPrimaryKey(); ?>\', \'<?php echo $this->getControllerUrl() ?>\'); return false;">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </a>
        </p>';
?>

</div>