<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new <?php echo $this->modelClass; ?>;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['<?php echo $this->modelClass; ?>']))
        {
            $model->attributes=$_POST['<?php echo $this->modelClass; ?>'];

            if ($model->validate())
                $this->saveItem($model);
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['<?php echo $this->modelClass; ?>']))
        {
            $model->attributes=$_POST['<?php echo $this->modelClass; ?>'];

            if ($model->validate())
                $this->saveItem($model);
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id = null)
    {
        if ($id)
        {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        elseif (!empty($_POST['ids']))
        {
            foreach ($_POST['ids'] as $id)
                $this->loadModel($id)->delete();
        }
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria();

        if (isset($_GET['search']))
        {
            $search = trim($_GET['search']);
            // Add the below line for each of your searchable fields
            //$criteria->compare('Title', $search, true, 'OR');
        }

        $dataProvider=new CActiveDataProvider('<?php echo $this->modelClass; ?>', array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder' => 'Created DESC',
            ),
            'pagination'=>array(
                'pageSize'=>$this->pageLimit,
            ),
        ));

        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    private function saveItem($model)
    {
        if ($model->save())
            $this->redirect(array('view','id'=>$model->getPrimaryKey()));
        else
            throw new Exception('Unable to save item. Attributes: <pre>' . print_r($model->attributes,1) . '</pre><br>Errors: ' . CHtml::errorSummary($model));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return <?php echo $this->modelClass; ?> the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=<?php echo $this->modelClass; ?>::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param <?php echo $this->modelClass; ?> $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='<?php echo $this->class2id($this->modelClass); ?>-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
