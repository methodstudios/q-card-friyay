(function() {
  $(function() {
    var $body, $closePopupLink, $countAnimate, $countdown, $dayCounter, $formErrorMsg, $hideStoresLink, $instructions, $likeLink, $nextLink, $popup, $prevLink, $scrollStoreBtn, $showStoresLink, $slider, $stage1, $stage2, $stores, closePopup, competitionOn, endDate, hideStores, initSlider, interval, minutesRegex, numberRegex, scrollStore, secondsRegex, setCountdown, showPopup, showStores, sliderNext, sliderPrev, totalDays;
    $body = $('body');
    $slider = $('.slider');
    $nextLink = $('.next');
    $prevLink = $('.prev');
    $popup = $('.popup-overlay');
    $stage1 = $('.stage1');
    $stage2 = $('.stage2');
    $countdown = $('.do-change-countdown');
    $showStoresLink = $('.do-show-stores');
    $hideStoresLink = $('.do-hide-stores');
    $closePopupLink = $('.do-close-popup');
    $likeLink = $('.do-like');
    $formErrorMsg = $('.error-message');
    $instructions = $('.instructions');
    $scrollStoreBtn = $('.scroll-store-btn');
    $stores = $('.stores');
    $dayCounter = $('.counter');
    $countAnimate = $('.count-animate');
    numberRegex = /(\d+)/g;
    minutesRegex = /(minutes)/i;
    secondsRegex = /(seconds)/i;
    endDate = App.endDate;
    totalDays = App.total;
    competitionOn = App.competitionOn;
    setCountdown = function() {
      var endShopping, now, range;
      now = moment();
      endDate = moment(App.endDate);
      endShopping = moment('2015-06-19 00:01:00');
      if (now < endDate) {
        range = moment.preciseDiff(now, endDate);
        range = range.replace(numberRegex, '<strong class="strong">$1</strong>');
        range = range.replace(minutesRegex, 'mins');
        range = range.replace(secondsRegex, 'secs');
        return $countdown.html(range);
      } else if ((endDate <= now && now < endShopping)) {
        $countdown.parent('p').html('Q Card online extravaganza – today only!');
        return clearInterval(interval);
      } else {
        $countdown.parent('p').html('Q Card online extravaganza!');
        return clearInterval(interval);
      }
    };
    initSlider = function() {
      return $slider.owlCarousel({
        autoPlay: 4000,
        items: 5,
        itemsDesktop: [1024, 4],
        itemsDesktopSmall: [768, 4],
        itemsTablet: [640, 2],
        itemsMobile: [false],
        pagination: false,
        scrollPerPage: true
      });
    };
    sliderNext = function() {
      return $slider.trigger('owl.next');
    };
    sliderPrev = function() {
      return $slider.trigger('owl.prev');
    };
    showStores = function() {
      $showStoresLink.hide();
      $hideStoresLink.show();
      $nextLink.hide();
      $prevLink.hide();
      return $slider.data('owlCarousel').destroy();
    };
    hideStores = function() {
      $hideStoresLink.hide();
      $showStoresLink.show();
      $nextLink.show();
      $prevLink.show();
      return initSlider();
    };
    showPopup = function() {
      $popup.show();
      return $body.css('overflow', 'hidden');
    };
    closePopup = function(e) {
      e.preventDefault();
      $popup.fadeOut();
      return $body.css('overflow', 'auto');
    };
    scrollStore = function(e) {
      e.preventDefault();
      return $('html,body').animate({
        scrollTop: $stores.offset().top
      }, 500);
    };
    setCountdown();
    interval = setInterval(function() {
      return setCountdown();
    }, 1000);
    initSlider();
    $nextLink.on('click', sliderNext);
    $prevLink.on('click', sliderPrev);
    $showStoresLink.on('click', showStores);
    $hideStoresLink.on('click', hideStores);
    $closePopupLink.on('click', closePopup);
    return $scrollStoreBtn.on('click', scrollStore);
  });

}).call(this);
