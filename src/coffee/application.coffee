$ ->

  # DOM Elements
  $body = $('body')
  $slider = $('.slider')
  $nextLink = $('.next')
  $prevLink = $('.prev')
  $popup = $('.popup-overlay')
  $stage1 = $('.stage1')
  $stage2 = $('.stage2')

  $countdown = $('.do-change-countdown')
  $showStoresLink = $('.do-show-stores')
  $hideStoresLink = $('.do-hide-stores')
  $closePopupLink = $('.do-close-popup')
  $likeLink = $('.do-like')

  $formErrorMsg = $('.error-message')
  $instructions = $('.instructions')
  $scrollStoreBtn = $('.scroll-store-btn')
  $stores = $('.stores')
  $dayCounter = $('.counter')
  $countAnimate = $('.count-animate')

  # Variables
  numberRegex = /(\d+)/g
  minutesRegex = /(minutes)/i
  secondsRegex = /(seconds)/i
  endDate = App.endDate
  totalDays = App.total
  competitionOn = App.competitionOn

  # Functions
  setCountdown = ->
    now = moment()
    endDate = moment(App.endDate)
    endShopping = moment('2015-06-19 00:01:00')
    if now < endDate
      range = moment.preciseDiff now, endDate
      range = range.replace numberRegex, '<strong class="strong">$1</strong>'
      range = range.replace minutesRegex, 'mins'
      range = range.replace secondsRegex, 'secs'
      $countdown.html range
    else if endDate <= now < endShopping
      $countdown.parent('p').html 'Q Card online extravaganza – today only!'
      clearInterval interval
    else
      $countdown.parent('p').html 'Q Card online extravaganza!'
      clearInterval interval

  initSlider = ->
    $slider.owlCarousel
      autoPlay: 4000
      items: 5
      itemsDesktop: [1024,4]
      itemsDesktopSmall: [768,4]
      itemsTablet: [640, 2]
      itemsMobile: [false]
      pagination: false
      scrollPerPage: true

  sliderNext = ->
    $slider.trigger 'owl.next'

  sliderPrev = ->
    $slider.trigger 'owl.prev'

  showStores = ->
    $showStoresLink.hide()
    $hideStoresLink.show()
    $nextLink.hide()
    $prevLink.hide()
    $slider.data('owlCarousel').destroy()

  hideStores = ->
    $hideStoresLink.hide()
    $showStoresLink.show()
    $nextLink.show()
    $prevLink.show()
    initSlider()

  showPopup = ->
    $popup.show()
    $body.css 'overflow', 'hidden'

  closePopup = (e) ->
    e.preventDefault()
    $popup.fadeOut()
    $body.css 'overflow', 'auto'

  scrollStore = (e) ->
    e.preventDefault()
    $('html,body').animate { scrollTop: $stores.offset().top }, 500

  setCountdown()
  interval = setInterval ->
    setCountdown()
  , 1000

  initSlider()
  $nextLink.on 'click', sliderNext
  $prevLink.on 'click', sliderPrev
  $showStoresLink.on 'click', showStores
  $hideStoresLink.on 'click', hideStores
  $closePopupLink.on 'click', closePopup
  $scrollStoreBtn.on 'click', scrollStore


