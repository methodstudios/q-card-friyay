<?php
if (!function_exists('array_replace_recursive'))
{
    function array_replace_recursive($array, $array1)
    {
        function recurse($array, $array1)
        {
            foreach ($array1 as $key => $value)
            {
                // create new key in $array, if it is empty or not an array
                if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key])))
                {
                    $array[$key] = array();
                }

                // overwrite the value in the base array
                if (is_array($value))
                {
                    $value = recurse($array[$key], $value);
                }
                $array[$key] = $value;
            }
            return $array;
        }

        // handle the arguments, merge one by one
        $args = func_get_args();
        $array = $args[0];
        if (!is_array($array))
        {
            return $array;
        }
        for ($i = 1; $i < count($args); $i++)
        {
            if (is_array($args[$i]))
            {
                $array = recurse($array, $args[$i]);
            }
        }
        return $array;
    }
}

if (!function_exists('lcfirst'))
{
    function lcfirst($str)
    {
        $str[0] = strtolower($str[0]);
        return $str;
    }
}

function generateString($length = 10)
{
    $string = sha1(uniqid(mt_rand(), true));
    return substr($string, 0, $length);
}

// Set configurations based on environment
if (strpos($_SERVER['SERVER_NAME'], 'localhost') !== FALSE || strpos($_SERVER['SERVER_NAME'], '.local') !== FALSE || strpos($_SERVER['SERVER_NAME'], '10.10') !== FALSE)
{
    // Enable debug mode for development environment
    defined('YII_DEBUG') or define('YII_DEBUG',true);

    // Specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

    define ('IS_DEVELOPMENT', true);
}
else
{
    define ('IS_DEVELOPMENT', false);
}

// Define separate STAGING constant incase we need to prevent live API calls etc
if (strpos($_SERVER['SERVER_NAME'], 'dev.') !== FALSE || strpos($_SERVER['SERVER_NAME'], 'service.') !== FALSE || strpos($_SERVER['SERVER_NAME'], 'staging.') !== FALSE)
{
    define('IS_STAGING', true);
}
else
{
    define('IS_STAGING', false);
}

// Layout (mobile detection)
include(dirname(__FILE__) . '/MobileDetect.php');
$detect = new MobileDetect;
if ($detect->isMobile() && !$detect->isTablet())
{
    define('IS_DESKTOP', false);
}
else
{
    define('IS_DESKTOP', true);
}