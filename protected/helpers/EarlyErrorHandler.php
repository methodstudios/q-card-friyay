<?php

// Early fatal errors handler, it will be replaced by a full featured one in Controller class
// (given it does not have any parse or fatal errors of its own)
function earlyFatalErrorHandler($unregister = false, $config = array())
{
    // Functionality for "unregistering" shutdown function
    static $unregistered;
    if ($unregister) $unregistered = true;
    if ($unregistered) return;

    // 1. error_get_last() returns NULL if error handled via set_error_handler
    // 2. error_get_last() returns error even if error_reporting level less then error
    $error = error_get_last();

    // Fatal errors
    $errorsToHandle = E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING;

    if (!IS_DEVELOPMENT && !is_null($error) && ($error['type'] & $errorsToHandle))
    {
        $message = 'FATAL ERROR: <pre>' . print_r($error,1) . '</pre>';
        $message .= getGlobals();
        if (!empty($config['params']['mailKey']))
             $message .= '<br><br>Mail key: ' . $config['params']['mailKey'] . ' - Use this key to set up mail rules in your inbox.';

        include(dirname(__FILE__) . '/../extensions/YiiMailer/YiiMailer.php');
        $mail = new YiiMailer();
        $mail->setTo('errors@methodstudios.co.nz');
        $mail->setSubject('A fatal error occurred.');
        $mail->setBody($message);
        $mail->setFrom($config['params']['adminEmail']);
        // Use Gmail SMTP since normal smtp was not sending emails to @method addresses
        if (IS_STAGING)
        {
            $mail->IsSMTP();
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465;
            $mail->IsHTML(true);
            $mail->SMTPAuth = true;
            $mail->SMTPSecure= 'ssl';
            $mail->Username = "methodstudios@gmail.com";
            $mail->Password = "method66";
        }
        // Disable BCC for errors
        $mail->MethodBcc = '';
        $mail->setBcc(array());

        if ($mail->send())
        {
            // Tell customer that you are aware of the issue and will take care of things
            echo 'A fatal error has occurred. We have been emailed about this issue and will fix it as soon as possible.';
        }
        else
        {
            echo 'A fatal error has occurred.';
        }
    }
    // If DEV then echo the error
    elseif (IS_DEVELOPMENT && !is_null($error) && ($error['type'] & $errorsToHandle))
    {
        echo 'FATAL ERROR: <pre>' . print_r($error,1) . '</pre>';
    }
}

function getGlobals()
{
    $globs = '';

    $allowed = array('_GET', '_POST', '_SERVER', '_SESSION', '_COOKIE');
    foreach ($GLOBALS as $key => $value)
    {
        if (in_array($key, $allowed) && !empty($value))
        {
            $globs .= '$' . $key . ':<br>';
            $globs .= '<pre>' . print_r($value,1) . '</pre>';
            $globs .= '<br>';
        }
    }

    return $globs;
}

$config = include(dirname(__FILE__) . '/../config/main.php');
register_shutdown_function('earlyFatalErrorHandler', false, $config);