<?php

/**
 * This is the model class for table "login_attempts".
 *
 * The followings are the available columns in table 'login_attempts':
 * @property integer $LoginAttemptId
 * @property string $IpAddress
 * @property string $Email
 * @property string $Modified
 * @property string $Created
 */
class LoginAttempts extends BaseModel
{
    const MAX_LOGIN_ATTEMPTS = 5;
    const ATTEMPT_TIMEFRAME = 10; //minutes

    public function tableName()
    {
        return 'login_attempts';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IpAddress, Email', 'required'),
            array('IpAddress', 'length', 'max'=>20),
            array('IpAddress', 'match', 'pattern'=>'/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/'),
            array('Email', 'length', 'max'=>50),
            array('Modified, Created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('LoginAttemptId, IpAddress, Email, Modified, Created', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'LoginAttemptId' => 'Login Attempt',
            'IpAddress' => 'Ip Address',
            'Email' => 'Email',
            'Modified' => 'Modified',
            'Created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

	$criteria->compare('LoginAttemptId',$this->LoginAttemptId);
	$criteria->compare('IpAddress',$this->IpAddress,true);
	$criteria->compare('Email',$this->Email,true);
	$criteria->compare('Modified',$this->Modified,true);
	$criteria->compare('Created',$this->Created,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function countFailedAttempts($ipAddress)
    {
        $fromDate = date('Y-m-d H:i:s', strtotime('-' . self::ATTEMPT_TIMEFRAME . ' minutes'));
        $toDate = date('Y-m-d H:i:s');

        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('Created', $fromDate, $toDate);

        $attempts = self::model()->findAllByAttributes(array('IpAddress'=>$ipAddress), $criteria);

        return count($attempts);
    }

    public static function validateLoginAttempt($ipAddress)
    {
        self::clearOldAttempts();

        $count = self::countFailedAttempts($ipAddress);
        if ($count >= self::MAX_LOGIN_ATTEMPTS)
            throw new Exception('You have reached the maximum number of login attempts.<br>Please try again in ' . self::ATTEMPT_TIMEFRAME . ' minutes.');
    }

    public static function clearOldAttempts()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('Created < DATE_SUB(NOW(), INTERVAL ' . self::ATTEMPT_TIMEFRAME . ' MINUTE)');
        self::model()->deleteAll($criteria);
    }
}
