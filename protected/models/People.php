<?php

/**
 * This is the model class for table "people".
 *
 * The followings are the available columns in table 'people':
 * @property integer $PeopleId
 * @property string $Name
 * @property string $Title
 * @property string $Content
 * @property string $Image
 * @property string $Status
 * @property integer $ModifiedBy
 * @property string $Modified
 * @property string $Created
 */
class People extends BaseModel
{
    public static $sizes = array(
        array(
            'width' => 300,
            'height' => 300,
            'msg' => 'Main Image',
        ),
    );

    public function tableName()
    {
        return 'people';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Name, Title, Content', 'required'),
            array('ModifiedBy', 'numerical', 'integerOnly'=>true),
            array('Name, Title', 'length', 'max'=>80),
            array('Image', 'length', 'max'=>100),
            array('Status', 'length', 'max'=>8),
            array('Modified', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('PeopleId, Name, Title, Content, Image, Status, ModifiedBy, Modified, Created', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'PeopleId' => 'People',
            'Name' => 'Name',
            'Title' => 'Title',
            'Content' => 'Content',
            'Image' => 'Image',
            'Status' => 'Status',
            'ModifiedBy' => 'Modified By',
            'Modified' => 'Modified',
            'Created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

	$criteria->compare('PeopleId',$this->PeopleId);
	$criteria->compare('Name',$this->Name,true);
	$criteria->compare('Title',$this->Title,true);
	$criteria->compare('Content',$this->Content,true);
	$criteria->compare('Image',$this->Image,true);
	$criteria->compare('Status',$this->Status,true);
	$criteria->compare('ModifiedBy',$this->ModifiedBy);
	$criteria->compare('Modified',$this->Modified,true);
	$criteria->compare('Created',$this->Created,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeDelete()
    {
        if (!empty($this->Image))
        {
            foreach (self::$sizes as $size)
            {
                $uploadsDir = self::getUploadsDir($size['width'], $size['height']);
                if (file_exists($uploadsDir . $this->Image))
                    unlink($uploadsDir . $this->Image);
            }
        }

        return parent::beforeDelete();
    }
}
