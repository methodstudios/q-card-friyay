<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $NewsId
 * @property string $Title
 * @property string $Slug
 * @property string $ArticlePreview
 * @property string $Article
 * @property string $Status
 * @property string $Date
 * @property string $Image
 * @property integer $ModifiedBy
 * @property string $Modified
 * @property string $Created
 */
class News extends BaseModel
{
    public static $sizes = array(
        array(
            'width' => 400,
            'height' => 300,
            'msg' => 'Main Image',
        ),
    );

    public function tableName()
    {
        return 'news';
    }

    public function rules()
    {
        return array(
            array('Title, Slug, Date, ArticlePreview, Article', 'required'),
            array('Title, Slug', 'length', 'max'=>100),
            array('Image', 'length', 'max'=>150),
            array('ArticlePreview', 'length', 'max'=>145),
            array('Status', 'length', 'max'=>8),
            array('Date, ModifiedBy, Modified, Created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('NewsId, Title, Slug, ArticlePreview, Article, Status, Date', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'NewsId' => 'News',
            'Title' => 'Title',
            'Slug' => 'Slug',
            'ArticlePreview' => 'Article Preview',
            'Article' => 'Article',
            'Status' => 'Status',
            'Date' => 'Date',
            'Image' => 'Image',
            'ModifiedBy' => 'Modified By',
            'Modified' => 'Modified',
            'Created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('NewsId',$this->NewsId);
        $criteria->compare('Title',$this->Title,true);
        $criteria->compare('Slug',$this->Slug,true);
        $criteria->compare('ArticlePreview',$this->ArticlePreview,true);
        $criteria->compare('Article',$this->Article,true);
        $criteria->compare('Status',$this->Status,true);
        $criteria->compare('Date',$this->Date,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (!empty($this->Date))
        {
            if (strpos($this->Date, ':') !== FALSE)
                $format = 'Y-m-d H:i:s';
            else
                $format = 'd-m-Y';
            $date = DateTime::createFromFormat($format, $this->Date);
            $this->Date = $date->format('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }

    public function beforeDelete()
    {
        if (!empty($this->Image))
        {
            foreach (self::$sizes as $size)
            {
                $uploadsDir = self::getUploadsDir($size['width'], $size['height']);
                if (file_exists($uploadsDir . $this->Image))
                    unlink($uploadsDir . $this->Image);
            }
        }

        return parent::beforeDelete();
    }

    public function convertDates()
    {
        if (!empty($this->Date))
        {
            if (strpos($this->Date, ':') !== FALSE)
                $format = 'Y-m-d H:i:s';
            else
                $format = 'd-m-Y';
            $date = DateTime::createFromFormat($format, $this->Date);
            $this->Date = $date->format('d-m-Y');
        }
    }
}
