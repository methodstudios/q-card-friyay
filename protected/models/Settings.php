<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $SettingsId
 * @property string $Name
 * @property string $Value
 * @property string $Key
 * @property string $Visible
 * @property string $Type
 * @property string $Modified
 * @property string $Created
 */
class Settings extends BaseModel
{
    const TYPE_TEXT = 'Text';
    const TYPE_CHECKBOX = 'Checkbox';

    const KEY_MAIN_ADMIN_HEX = 'MAIN_ADMIN_HEX';
    const KEY_MAIN_ADMIN_RGB = 'MAIN_ADMIN_RGB';
    const KEY_BACKEND_LOGO = 'BACKEND_LOGO';
    const KEY_GA_CODE = 'GA_CODE';
    const KEY_IP_WHITELIST = 'IP_WHITELIST';
    const KEY_ENABLE_NEWS = 'ENABLE_NEWS';
    const KEY_ENABLE_PAGES = 'ENABLE_PAGES';
    const KEY_ENABLE_PEOPLE = 'ENABLE_PEOPLE';

    public $Logo;

    public function tableName()
    {
        return 'settings';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Name', 'required'),
            array('Name', 'length', 'max'=>100),
            array('Value', 'length', 'max'=>255),
            array('Visible', 'length', 'max'=>1),
            array('Value', 'validateIpAddress'),
            array('Logo','file', 'types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'maxSize'=>1024 * 512, 'tooLarge'=>'Logo has to be smaller than 500kb'),
            array('Key, Type, Visible, Modified, Created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('SettingsId, Name, Value, Key, Visible, Modified, Created', 'safe', 'on'=>'search'),
        );
    }

    public function validateIpAddress($attribute, $params)
    {
        if ($this->Key == self::KEY_IP_WHITELIST)
        {
            // If they have entered an IP address whitelist but this IP is not in the list
            if (!empty($this->Value) && strpos($this->Value, $_SERVER['REMOTE_ADDR']) === FALSE)
                $this->addError($attribute, 'Your current IP address of ' . $_SERVER['REMOTE_ADDR'] . ' is not in the list of allowed IP addresses.');

            if (!empty($this->Value) && strpos($this->Value, ' ') !== FALSE)
                $this->addError($attribute, 'Not allowed spaces in the list of allowed IP addresses.');
        }

        return true;
    }

    public function beforeSave()
    {
        $this->Modified = date('Y-m-d H:i:s');
        parent::beforeSave();
        return true;
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'SettingsId' => 'Settings',
            'Name' => 'Name',
            'Value' => 'Value',
            'Key' => 'Key',
            'Visible' => 'Visible',
            'Modified' => 'Modified',
            'Created' => 'Created',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('SettingsId',$this->SettingsId);
        $criteria->compare('Name',$this->Name,true);
        $criteria->compare('Value',$this->Value,true);
        $criteria->compare('Key',$this->Key,true);
        $criteria->compare('Visible',$this->Visible,true);
        $criteria->compare('Modified',$this->Modified,true);
        $criteria->compare('Created',$this->Created,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
