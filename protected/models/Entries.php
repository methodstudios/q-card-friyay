<?php

/**
 * This is the model class for table "entries".
 *
 * The followings are the available columns in table 'entries':
 * @property integer $EntryId
 * @property string $Name
 * @property string $IncreaseAmount
 * @property string $NewTotal
 * @property string $IpAddress
 * @property string $Modified
 * @property string $Created
 */
class Entries extends BaseModel
{
    const TOTAL_ENTRIES = 20; // max entries by IP
    const ENTRY_INCREMENT = 0.1;
    const STARTING_TOTAL = 90;
    const FIRST_MAX_ENTRIES = 200;
    const MAX_ENTRIES = 200;
    const END_DATE = '2015-06-16 20:00:00';
    const SHOPPING_DATE = '2015-06-19 00:01:00';

    public function tableName()
    {
        return 'entries';
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IncreaseAmount, NewTotal, IpAddress', 'required'),
            array('Name', 'length', 'max'=>100),
            array('IncreaseAmount, NewTotal', 'length', 'max'=>10),
            array('IpAddress', 'length', 'max'=>20),
            array('IpAddress', 'match', 'pattern'=>'/^(([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([1-9]?[0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/'),
            array('Modified, Created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('EntryId, Name, IncreaseAmount, NewTotal, IpAddress, Modified, Created', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'EntryId' => 'Entry',
            'Name' => 'Name',
            'IncreaseAmount' => 'Increase Amount',
            'NewTotal' => 'New Total',
            'IpAddress' => 'Ip Address',
            'Modified' => 'Modified',
            'Created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

	$criteria->compare('EntryId',$this->EntryId);
	$criteria->compare('Name',$this->Name,true);
	$criteria->compare('IncreaseAmount',$this->IncreaseAmount,true);
	$criteria->compare('NewTotal',$this->NewTotal,true);
	$criteria->compare('IpAddress',$this->IpAddress,true);
	$criteria->compare('Modified',$this->Modified,true);
	$criteria->compare('Created',$this->Created,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function validateIp()
    {
        if (!empty($this->IpAddress))
        {
            $entries = self::model()->findAllByAttributes(array('IpAddress'=>$this->IpAddress));

            if (empty($entries) || count($entries) < self::TOTAL_ENTRIES)
                return true;
        }

        $this->addError('IpAddress', 'Sorry you have reached the maximum number of likes.');
        Controller::sendErrorEmail('QCard Friyay - Max reached', 'Email to notify of a user reaching the maximum of ' . self::TOTAL_ENTRIES . ' entries for IP: ' . $this->IpAddress);
        return false;
    }

    public function beforeSave()
    {
        if (empty($this->Name))
            $this->Name = null;

        return parent::beforeSave();
    }

    public static function getLatestTotal()
    {
        $latestEntry = self::model()->find(array('order'=>'EntryId DESC'));

        if (!empty($latestEntry) && $latestEntry->NewTotal > 0 && $latestEntry->NewTotal < self::MAX_ENTRIES)
            return round($latestEntry->NewTotal,1);
        elseif (!empty($latestEntry) && $latestEntry->NewTotal >= self::MAX_ENTRIES)
            return self::MAX_ENTRIES;
        else
            return self::STARTING_TOTAL;
    }

    public static function getLatestTotalAsString()
    {
        $latestTotal = self::getLatestTotal();
        $latestString = number_format($latestTotal,1);

        if (strlen(round($latestTotal)) < 3)
            $latestString = '0' . $latestString;

        return $latestString;
    }

    public static function competitionActive()
    {
        $today = date("Y-m-d H:i:s");

        if ($today < self::END_DATE && self::validateTotal())
            return true;

        return false;
    }

    public static function shoppingActive()
    {
        $today = date("Y-m-d");
        $shoppingDate = date('Y-m-d', strtotime(self::SHOPPING_DATE));

        if (!self::competitionActive() && $today == $shoppingDate)
            return true;

        return false;
    }

    public static function validateTotal()
    {
        $total = self::getLatestTotal();

        if ($total < self::MAX_ENTRIES)
            return true;

        return false;
    }

    public static function getRoundedTotals()
    {
        $latestTotal = self::getLatestTotal();

        $data = array();
        $data['months'] = round(($latestTotal / 30));
        $data['days'] = $latestTotal;

        return $data;
    }

    public static function getDaysArray()
    {
        $roundedTotals = self::getRoundedTotals();
        $daysText = $roundedTotals['days'];
        if (strlen($roundedTotals['days']) < 3)
            $daysText = '0' . $roundedTotals['days'];

        $days = array();
        for ($i = 0; $i <= 2; $i++)
            $days[] = substr($daysText, $i, 1);

        return $days;
    }
}
