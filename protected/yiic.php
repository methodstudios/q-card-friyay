<?php

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../framework/yiic.php';
$consoleConfig=require_once(dirname(__FILE__).'/config/console.php');

$dbConfig = array();
if (file_exists(dirname(__FILE__).'/config/database.local.php'))
{
    $dbConfig = require_once(dirname( __FILE__ ).'/config/database.local.php');
}
elseif (file_exists(dirname(__FILE__).'/config/database.php'))
{
    $dbConfig = require_once(dirname( __FILE__ ).'/config/database.php');
}

// If $db settings exist it will overwrite the console config settings
$config = array_replace_recursive($consoleConfig, $dbConfig);

require_once($yiic);
