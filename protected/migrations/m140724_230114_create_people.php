<?php

class m140724_230114_create_people extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `people` (
                  `PeopleId` int(11) NOT NULL AUTO_INCREMENT,
                  `Name` varchar(80) NOT NULL,
                  `Title` varchar(80) NOT NULL,
                  `Content` text NOT NULL,
                  `Image` varchar(100) DEFAULT NULL,
                  `Status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
                  `ModifiedBy` int(11) NULL,
                  `Modified` timestamp NULL,
                  `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`PeopleId`),
                  INDEX (`ModifiedBy`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;
            ");
	}

	public function down()
	{
            $this->execute("DROP TABLE IF EXISTS `people`;");
	}
}