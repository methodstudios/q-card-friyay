<?php

class m140724_225537_create_settings extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `settings` (
                  `SettingsId` int(11) NOT NULL AUTO_INCREMENT,
                  `Name` varchar(100) NOT NULL,
                  `Key` varchar(100) NOT NULL,
                  `Value` varchar(255) NOT NULL,
                  `Visible` tinyint(1) NOT NULL DEFAULT 1,
                  `Type` enum('Text', 'Checkbox') DEFAULT 'Text' NOT NULL,
                  `Modified` datetime DEFAULT NULL,
                  `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`SettingsId`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;
            ");

            $this->execute("
                INSERT INTO `settings` (`Name`, `Key`, `Value`, `Visible`, `Type`) VALUES
                ('Main Admin Hex Colour eg. #4491e8', 'MAIN_ADMIN_HEX', '#4491e8', 1, 'Text'),
                ('Sub Menu RGBA Colour eg. rgba(68,145,232,0.4)', 'MAIN_ADMIN_RGB', 'rgba(68,145,232,0.4)', 1, 'Text'),
                ('Site Logo', 'BACKEND_LOGO', 'grid_logo.png', 0, 'Text'),
                ('Google Analytics Code eg. UA-12345678-1', 'GA_CODE', '', 1, 'Text'),
                ('IP Whitelist for Backend (Method: 114.23.249.150)', 'IP_WHITELIST', '', 1, 'Text'),
                ('Enable Module - News', 'ENABLE_NEWS', '0', 1, 'Checkbox'),
                ('Enable Module - Pages', 'ENABLE_PAGES', '0', 1, 'Checkbox'),
                ('Enable Module - People', 'ENABLE_PEOPLE', '0', 1, 'Checkbox');
            ");
	}

	public function down()
	{
            $this->execute("DROP TABLE IF EXISTS `settings`;");
	}
}