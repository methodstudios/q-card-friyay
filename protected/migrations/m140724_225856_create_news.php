<?php

class m140724_225856_create_news extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `news` (
                  `NewsId` int(11) NOT NULL AUTO_INCREMENT,
                  `Title` varchar(100) NOT NULL,
                  `Slug` varchar(100) NOT NULL,
                  `ArticlePreview` varchar(145) NULL,
                  `Article` longtext NOT NULL,
                  `Date` datetime NULL,
                  `Image` varchar(150) NULL,
                  `Status` enum('Enabled','Disabled') NULL DEFAULT 'Enabled',
                  `ModifiedBy` int(11) NULL,
                  `Modified` timestamp NULL,
                  `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`NewsId`),
                  UNIQUE KEY `Slug` (`Slug`),
                  INDEX (`ModifiedBy`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;
            ");
	}

	public function down()
	{
            $this->execute("DROP TABLE IF EXISTS `news`;");
	}
}