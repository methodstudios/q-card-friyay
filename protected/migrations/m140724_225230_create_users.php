<?php

class m140724_225230_create_users extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `users` (
                  `UserId` int(11) NOT NULL AUTO_INCREMENT,
                  `Username` varchar(100) NOT NULL COMMENT 'Email Address',
                  `Password` varchar(255) NOT NULL,
                  `FirstName` VARCHAR(30) NOT NULL,
                  `LastName` VARCHAR(30) NOT NULL,
                  `AdminLevel` enum('Super','Admin','Other') NOT NULL DEFAULT 'Admin',
                  `Status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
                  `PasswordToken` VARCHAR(60) NULL,
                  `PasswordTokenExpiry` DATETIME NULL,
                  `LastLogin` DATETIME NULL DEFAULT NULL,
                  `SessionToken` VARCHAR(12) NULL,
                  `ModifiedBy` INT(11) NULL,
                  `Modified` TIMESTAMP NULL,
                  `Created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`UserId`),
                  UNIQUE KEY (`Username`),
                  INDEX (`ModifiedBy`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
            ");

            $this->execute("
                INSERT INTO `users` (`UserId`, `Username`, `Password`, `FirstName`,`LastName`,`AdminLevel`, `Status`, `LastLogin`, `Created`) VALUES
                (NULL, 'dev@methodstudios.co.nz', '02ec55764df20e83869c6bf5bc99f8d5aa2db97b7963b33db004ac3e1d1100a2','Method','Development','Super', 'Enabled', NULL, CURRENT_TIMESTAMP);
            ");

            $this->execute("CREATE TABLE `login_attempts` (
                `LoginAttemptId` int(11) NOT NULL AUTO_INCREMENT,
                `IpAddress` varchar(20) NOT NULL,
                `Email` varchar(50) NOT NULL,
                `Modified` timestamp NULL DEFAULT NULL,
                `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`LoginAttemptId`)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
	}

	public function down()
	{
            $this->execute("DROP TABLE IF EXISTS `users`;");
            $this->execute("DROP TABLE IF EXISTS `login_attempts`;");
	}
}