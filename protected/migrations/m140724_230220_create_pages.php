<?php

class m140724_230220_create_pages extends CDbMigration
{
	public function up()
	{
            $this->execute("
                CREATE TABLE IF NOT EXISTS `pages` (
                  `PageId` int(11) NOT NULL AUTO_INCREMENT,
                  `Title` varchar(100) NOT NULL,
                  `Slug` varchar(100) NOT NULL,
                  `LeadCopy` varchar(150) NULL,
                  `Content` longtext NOT NULL,
                  `Image` varchar(100) DEFAULT NULL,
                  `Status` enum('Enabled','Disabled') NOT NULL DEFAULT 'Enabled',
                  `ModifiedBy` int(11) NULL,
                  `Modified` timestamp NULL,
                  `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`PageId`),
                  UNIQUE KEY `Slug` (`Slug`),
                  INDEX (`ModifiedBy`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
            ");
	}

	public function down()
	{
            $this->execute("DROP TABLE IF EXISTS `pages`;");
	}
}