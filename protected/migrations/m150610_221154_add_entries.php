<?php

class m150610_221154_add_entries extends CDbMigration
{
	public function up()
	{
            $this->execute("CREATE TABLE IF NOT EXISTS `entries` (
                `EntryId` int(11) NOT NULL AUTO_INCREMENT,
                `Name` varchar(100) DEFAULT NULL,
                `IncreaseAmount` decimal(10,2) NOT NULL,
                `NewTotal` date NOT NULL,
                `IpAddress` varchar(20) NOT NULL,
                `Modified` timestamp NULL DEFAULT NULL,
                `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`EntryId`)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
	}

	public function down()
	{
            echo "m150610_221154_add_entries does not support migration down.\n";
            return false;
	}
}