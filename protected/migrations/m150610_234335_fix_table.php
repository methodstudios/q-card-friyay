<?php

class m150610_234335_fix_table extends CDbMigration
{
	public function up()
	{
            $this->execute("ALTER TABLE `entries` CHANGE `NewTotal` `NewTotal` DECIMAL(10,2) NOT NULL;");
	}

	public function down()
	{
            echo "m150610_234335_fix_table does not support migration down.\n";
            return false;
	}
}