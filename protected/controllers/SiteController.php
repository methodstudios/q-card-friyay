<?php

class SiteController extends FrontendController
{
    public $redirectTrue;

    public function actionIndex()
    {
        $this->layout = 'main';
        $this->pageTitle = '';
        $this->pageDescription = '';

        if (!empty($_GET['redirect']))
            $this->redirectTrue = true;

        $this->render('index');
    }

    public function actionView($file)
    {
        $this->layout = 'main';
        $this->pageTitle = '';
        $this->pageDescription = '';

        if (file_exists($this->getBaseDir() . 'protected/views/site/' . $file . '.php'))
            $this->render($file);
        else
            throw new CHttpException(404, 'The page you are looking for does not exist.');
    }

    public function actionSendLike()
    {
        if (isset($_POST['Entries']))
        {
            $latestTotal = Entries::getLatestTotal();

            $entry = new Entries;
            $entry->attributes = $_POST['Entries'];
            $entry->IpAddress = $this->getIpAddress();
            $entry->IncreaseAmount = Entries::ENTRY_INCREMENT;
            $entry->NewTotal = ($latestTotal + $entry->IncreaseAmount);

            if ($entry->validate() && $entry->validateIp())
            {
                $entry->save();

                $latestString = Entries::getLatestTotalAsString();
                $days = array();
                for ($i = 0; $i <= 4; $i++)
                    $days[] = substr($latestString, $i, 1);

                $data = array(
                    'message'=>'success',
                    'digits'=>$days,
                    'amount'=>$latestString,
                );
            }
            else
            {
                $data = array(
                    'message'=>CHtml::errorSummary($entry),
                );
            }

            echo json_encode($data);
        }
    }

    private function getIpAddress()
    {
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            return $_SERVER['REMOTE_ADDR'];
    }
}