<?php
class ValidationException extends Exception {}

class ApiController extends FrontendController
{
    public $data;

    // Attribute fields which should not have HTML in them
    private $noHtml = array('Title', 'Name');

    public function init()
    {
        CkEditor::checkAuthorisation();

        if (!Yii::app()->request->isAjaxRequest)
            $this-throw404();

        parent::init();
    }

    public function actionIndex()
    {
        if (!empty($_POST))
        {
            try
            {
                $this->data = $_POST;
                $this->processData();
                $this->validateData();

                if ($model = $this->processModel())
                    echo '1';
            }
            catch (ValidationException $e)
            {
                echo $e->getMessage();
            }
            catch (Exception $e)
            {
                echo '<p>' . $e->getMessage() . '</p>';
            }
        }
        else
        {
            $this->throw404();
        }
    }

    private function throw404()
    {
        throw new CHttpException(404, 'The page you have requested does not exist.');
    }

    /*
     * Retrieves the data and processes it before we start using it
     */
    private function processData()
    {
        foreach ($this->data as $key => $value)
            $this->filterFields($key, $value);
    }

    /*
     * Filter the data fields if reqd
     */
    private function filterFields($key, $value)
    {
        if (in_array($key, $this->noHtml))
            $value = strip_tags($value);

        $filter = new CHtmlPurifier;
        $this->data[$key] = $filter->purify($value);

        //$this->data[$key] = $value;
    }

    /*
     * Make sure we have the minimum amount of info required
     */
    private function validateData()
    {
        if (empty($this->data))
            throw new Exception('No data was sent with your request.');

        if (empty($this->data['Id']))
            throw new Exception('The Id (data-id) is missing from your request.');

        if (empty($this->data['Field']))
            throw new Exception('The Field (data-field) is missing from your request.');

        if (empty($this->data['Type']))
            throw new Exception('The Type (data-type) is missing from your request.');

        if (empty($this->data['InPlaceValue']))
            throw new Exception('The Value (data-value) is missing from your request.');

        CkEditor::classExists($this->data['Type']);
    }

    /*
     * Fetch the model, update and save it
     */
    private function processModel()
    {
        $id = $this->data['Id'];
        $field = $this->data['Field'];
        $className = $this->data['Type'];

        if ($model = CkEditor::getItem($id, $className))
        {
            CkEditor::validateModel($model, $field);
            $model->$field = $this->data['InPlaceValue'];

            if (!$model->save())
                throw new ValidationException(CHtml::errorSummary($model));

            return $model;
        }

        return false;
    }

}