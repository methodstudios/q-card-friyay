CKEditor Class - Readme
04/05/2015
By Josh Warner

To make an element editable you need to add the below line of code to the HTML container which contains the content to be edited.
NB: The functionality requires use of the ID attribute. So if your HTML element is using the ID field this will need to be removed and replaced with a class.
<?php CkEditor::makeEditable('[Field Name]', '[The model object]'); ?>

#####-------------- INDIVIDUAL ELEMENT EXAMPLE --------------#####

For example, if you have two divs (Title and Content) which belongs to the "articles" table (and the "Articles" model) you would do the following:

<?php
// Pulls your article from the DB (this will normally be done in the controller but added here for an example)
$article = Articles::model()-findByPk($id); 
?>

<h1 class="my-title" <?php CkEditor::makeEditable('Title', $article); ?>>
    <?php echo $article->Title; ?>
</h1>

<div class="my-content" <?php CkEditor::makeEditable('Content', $article); ?>>
    <?php echo $article->Content; ?>
</div>

#####--------------------------------------------------------#####


#####-------------- LOOP THROUGH EXAMPLE --------------#####

For example, if you have an array of articles (Title, LeadCopy and Content) which belongs to the "articles" table (and the "Articles" model) you would do the following:

<?php
// Pulls your articles from the DB (this will normally be done in the controller but added here for an example)
$articles = Articles::model()-findAll();

foreach ($articles as $article): 
?>
    <h1 class="my-title" <?php CkEditor::makeEditable('Title', $article); ?>>
        <?php echo $article->Title; ?>
    </h1>

    <div class="my-lead-copy" <?php CkEditor::makeEditable('LeadCopy', $article); ?>>
        <?php echo $article->LeadCopy; ?>
    </div>

    <div class="my-content" <?php CkEditor::makeEditable('Content', $article); ?>>
        <?php echo $article->Content; ?>
    </div>
<?php endforeach; ?>

#####--------------------------------------------------------#####


#####-------------- STATIC (NO PHP) EXAMPLE --------------#####

If for some reason we do not want to use PHP inside the HTML you can add the attributes manually as below:

<h1 data-field="Title" data-id="your-article-slug-or-id" data-type="Articles" id="must-be-unique-id" contenteditable="true">
    <!-- YOUR TITLE -->
</h1>

<div data-field="Content" data-id="your-article-slug-or-id" data-type="Articles" id="must-be-unique-id" contenteditable="true">
    <!-- YOUR CONTENT -->
</div>

<script>
/*
* Example function you can use to set the variables using jQuery 
* If the data is coming from a JSON array for example then loop through the array and run this function
*/
function setAttributes(selector, field, id, type)
{
    $(selector).attr('data-field', field);
    $(selector).attr('data-id', id);
    $(selector).attr('data-type', type);
    $(selector).attr('data-option', 'simple');

    // Generate unique ID
    var uniqueId = type.toLowerCase() + '-' + field.toLowerCase() + '-' + id.toLowerCase() + '-' + Math.random().toString(36).substring(2, 6);
    $(selector).attr('id', uniqueId);

    $(selector).prop('contenteditable', true);
}
</script>

#####--------------------------------------------------------#####


#####-------------- IMAGE EDITABLE EXAMPLE --------------#####

To make an image editable you need to put the CkEditor::makeEditableImage command on the image CONTAINER. 
 - It will add a link to the update page in the backend
 - You may also specify the location of the link (top-left, top-right, bottom-left, bottom-right)
 - If your module is not a standard name, you may specify it. Format is [controller]/[action] ie. pages/update.

<?php
// Pulls your article from the DB (this will normally be done in the controller but added here for an example)
$article = Articles::model()-findByPk($id); 
?>

// EXAMPLE 1 - Standard
<div class="image-1" <?php CkEditor::makeEditableImage($article); ?>>
    <img src="<?php echo $this->getImagesPath() . $article->Image ?>" alt="" />
</div>

// EXAMPLE 2 - Special Position
<div class="image-2" <?php CkEditor::makeEditableImage($article, 'bottom-left'); ?>>
    <img src="<?php echo $this->getImagesPath() . $article->Image ?>" alt="" />
</div>

// EXAMPLE 3 - Special Position and Module
<div class="image-3" <?php CkEditor::makeEditableImage($article, 'top-left', 'articles/updateSpecial'); ?>>
    <img src="<?php echo $this->getImagesPath() . $article->Image ?>" alt="" />
</div>

#####--------------------------------------------------------#####