<?php

class CkEditor
{
    const POSITION_TOP_LEFT = 'top-left';
    const POSITION_TOP_RIGHT = 'top-right';
    const POSITION_BOTTOM_LEFT = 'bottom-left';
    const POSITION_BOTTOM_RIGHT = 'bottom-right';

    private static $positions = array(
        self::POSITION_TOP_LEFT,
        self::POSITION_TOP_RIGHT,
        self::POSITION_BOTTOM_LEFT,
        self::POSITION_BOTTOM_RIGHT,
    );

    /*
     * Make sure this user is logged into the backend
     * Also ensure that inplace editing is enabled
     */
    public static function checkAuthorisation()
    {
        if (empty(Yii::app()->session['LoggedIn']))
            throw new CHttpException(401, 'You may have been logged out, try logging in again <a href="' . Yii::app()->baseUrl . '/backend/" target="_blank">here</a> to continue.');

        if (empty(Yii::app()->params['inplaceEditing']))
            throw new CHttpException(406, 'Inplace Editing must be enabled in your site config.');
    }

    /*
     * This function will make a regular HTML tag editable via the CKEditor Inline plugin and our /api controller
     * Params:
     * @field - Field name of the item (in the model)
     * @model - The model for the selected item
     * @return - Return the attributes instead of echo'ing them
     * @simple - Display only the simple editor
     */
    public static function makeEditable($field, $model, $return = false, $simple = true)
    {
        try
        {
            if (self::checkMobile())
                return '';
            self::checkAuthorisation();
            self::validateModel($model, $field);

            $id = (!empty($model->Slug) ? $model->Slug : $model->getPrimaryKey());
            $className = get_class($model);

            $attrs = array();
            $attrs['contenteditable'] = 'true';
            $attrs['data-id'] = $id;
            $attrs['data-field'] = $field;
            $attrs['data-type'] = $className;
            $attrs['data-option'] = ($simple ? 'simple' : '');
            // Generate unique ID
            $attrs['id'] = strtolower($className) . '-' . strtolower($field) . '-' . $id . '-' . self::generateString(6);

            $html = self::processAttrs($attrs);
            if ($return)
                return $html;
            else
                echo $html;
        }
        catch (CHttpException $e)
        {
            return;
        }
    }

    /*
     * This function will add a link to an image container for the user to edit the item in the backend
     * Note: This must be called on a CONTAINER of the image such as a parent div etc.
     * Params:
     * @model - The model for the selected item
     * @position - The position to display the image top-left, top-right, bottom-left, bottom-right
     * @module - URL to edit the page. You only need to specify the part BETWEEN /backend/ and /id?= ie. /backend/[pages/update]?id=1
     * @return - Return the attributes instead of echo'ing them
     */
    public static function makeEditableImage($model, $position = 'top-right', $module = null, $return = false)
    {
        try
        {
            if (self::checkMobile())
                return '';
            self::checkAuthorisation();
            self::validateModel($model);
            self::validatePosition($position);

            // Set the default to be $modelName/update - will need to be specified if URL is different
            if (empty($module))
                $module = lcfirst(get_class($model)) . '/update';

            self::validateModule($module);

            $url = Yii::app()->baseUrl . '/backend/' . $module . '?id=' . $model->getPrimaryKey();

            $attrs = array();
            $attrs['data-image-editable'] = 'true';
            $attrs['data-url'] = $url;
            $attrs['data-position'] = $position;

            $html = self::processAttrs($attrs);
            if ($return)
                return $html;
            else
                echo $html;
        }
        catch (CHttpException $e)
        {
            return;
        }
    }

    /*
     * Disable the editing functionality on mobile devices
     */
    private static function checkMobile()
    {
        if (!IS_DESKTOP) // Dont show edit button on mobile
            return true;

        return false;
    }

    private static function processAttrs($attrs)
    {
        $html = ' ';
        foreach ($attrs as $attr => $value)
            $html .= ' ' . $attr . '="' . $value . '"';

        return $html;
    }

    public static function validateModel($model, $field = null)
    {
        if (!is_object($model))
            throw new Exception('The model parameter you supplied to madeEditable() is not a valid object.');

        $id = $model->getPrimaryKey();

        if (!empty($field) && !$model->hasAttribute($field))
            throw new Exception('The field "' . $field . '" does not exist in the ' . get_class($model) . ' model.');
        elseif (empty($id))
            throw new Exception('"' . $id . '" is not a valid primary key for the supplied model.');
    }

    private static function validatePosition($position)
    {
        if (!in_array($position, self::$positions))
            throw new Exception('"' . $position . '" is not a valid position. It must be one of the following: ' . implode(',', self::$positions) . '.');
    }

    private static function validateModule($module)
    {
        $baseDir = dirname(Yii::app()->request->scriptFile) . '/protected/backend/views/';
        if (!is_readable($baseDir . $module . '.php'))
            throw new Exception('The module "' . $module . '" does not exist in the backend views directory: ' . $baseDir . $module . '.php');
    }

    public static function getItem($id, $className)
    {
        if (is_numeric($id))
            return $className::model()->findByPk((int)$id);
        elseif (!is_numeric($id) && is_string($id))
            return $className::model()->findByAttributes(array('Slug'=>$id));

        return false;
    }

    public static function classExists($className)
    {
        if (!@class_exists($className))
            throw new Exception('The type "' . $className . '" is not a valid model name.');

    }

    public static function generateString($length = 10)
    {
        $string = sha1(uniqid(mt_rand(), true));
        return substr($string, 0, $length);
    }
}