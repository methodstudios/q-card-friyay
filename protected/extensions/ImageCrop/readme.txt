#####-------------- Add the required image sizes to your model and functions - you can add as many sizes as you require --------------#####

    public static $sizes = array(
        array(
            'width' => 200,
            'height' => 100,
            'msg' => '[Your Message eg. Photo used on Event Page]', // not reqd
        ),
        array(
            'width' => 500,
            'height' => 500,
        ),
    );

    public function beforeDelete()
    {
        if (!empty($this->Image))
        {
            foreach (self::$sizes as $size)
            {
                $uploadsDir = self::getUploadsDir($size['width'], $size['height']);
                if (file_exists($uploadsDir . $this->Image))
                    unlink($uploadsDir . $this->Image);
            }
        }

        return parent::beforeDelete();
    }

#####----------------------------------------------------------------------------------------------------------------------------------#####



#####-------------- Add the following to your view file where you want the cropper to appear --------------#####

<div class="row">
        <?php echo $form->labelEx($model,'Image'); ?>
        <?php echo $form->hiddenField($model, 'Image'); ?>
        <button type="button" class="show-all-lightboxes" data-id="0"><?php echo ($model->isNewRecord ? 'Add Images' : 'Change All Images'); ?></button>
</div>

<?php
// Display list of images if updating record
echo $this->getImagesList($model, 'Image');
?>

#####----------------------------------------------------------------------------------------------------------------------------------#####



#####-------------- Add the following to the bottom of your view file where the cropper is located --------------#####

<?php
// Initialise the cropper
$attribute = 'Image'; // Change attribute if required
echo $this->renderPartial('/includes/_cropInitialize', array(
    'attribute'=>$attribute,
    'model'=>$model,
));
?>

#####----------------------------------------------------------------------------------------------------------------------------------#####



#####-------------- Add the following PHP to your controller so the AJAX call can upload the image --------------#####

    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS START
    #####---------------------------------------------------------------------------------------------------------------------#####
    private function deleteOldImages(Pages $model)
    {
        $this->sizes = Pages::$sizes;
        $this->parentDeleteOldImages($model, 'Image');
    }

    private function checkImages(Pages $model)
    {
        $this->sizes = Pages::$sizes;
        return $this->parentCheckImages($model, 'Image', [SlugAttribute (not reqd)]);
    }

    public function actionImageCrop()
    {
        $model = new Pages;
        echo $this->parentImageCrop($model);
    }
    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS END
    #####---------------------------------------------------------------------------------------------------------------------#####

#####----------------------------------------------------------------------------------------------------------------------------------#####



#####-------------- Add the following 2 lines of PHP to your controllers methods where applicable --------------#####

	public function actionUpdate($id)
	{
            $model=$this->loadModel($id);

            if(isset($_POST['Locations']))
            {
----------->>>> $this->deleteOldImages($model); <<<<------------------
                $model->attributes=$_POST['Locations'];
                $model = $this->saveLocation($model);
            }

            $this->render('update',array(
                'model'=>$model,
            ));
	}

        private function saveLocation(Locations $model)
        {
            if ($model->validate())
            {
                // Rename images if they have hash filenames
----------->>>> $model = $this->checkImages($model); <<<<------------------

                if($model->save())
                    $this->redirect(array('view','id'=>$model->getPrimaryKey()));
            }

            return $model;
        }

#####----------------------------------------------------------------------------------------------------------------------------------#####



#####-------------- Adding the images to your CRUD pages --------------#####

// Add the following to your view.php file CDetalView array to display the image on the view page
array(
    'label' => 'Image',
    'type' => 'raw',
    'value' => '<img src="' . Locations::getUploadsPath() . $model->Image . '" alt="" />',
),

// Add the following to your index.php file CGridView array to display the image in the index list
array(
    'name' => 'Image',
    'type'=>'raw',
    'value' => 'CHtml::image("' . Locations::getUploadsPath() . '" . $data->Image)',
),

// Add the following to your _view.php file to display the image on the index panel view
<?php if (!empty($data->Image)): ?>
    <p style="text-align:center"><img src="<?php echo Locations::getUploadsPath() . $data->Image; ?>" alt="" /></p>
<?php endif; ?>

#####----------------------------------------------------------------------------------------------------------------------------------#####



#####-------------- To pull the images in the frontend use the following code --------------#####

<img src="<?php echo Locations::getUploadsPath([Width of image], [Height of image]) . $model->Image ?>" alt="" />

// If you just want to select the default (first) image from the $sizes array then leave the width and height empty ie:

<img src="<?php echo Locations::getUploadsPath() . $model->Image ?>" alt="" />

#####----------------------------------------------------------------------------------------------------------------------------------#####