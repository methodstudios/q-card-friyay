<?php
/*
 * This class uses the jQuery Image Crop plugin which was purchased from codecanyon.net
 * Please see the following link to find documentation etc
 * http://codecanyon.net/item/image-crop/full_screen_preview/5348464
 */
class ImageCrop {

    public $filename;
    public $directory;
    public $extension;

    public function __construct($directory, $filename = null, $extension = 'jpg')
    {
        if (empty($filename)) $filename = sha1(uniqid(mt_rand(), true));

        $this->directory = $directory;
        $this->filename = $filename;
        $this->extension = $extension;
    }

    public function resizeImage($imageBase64)
    {
	// location to save cropped image
        if (strpos($this->directory, 'www') !== FALSE)
            $filePath = $this->directory;
        else
            $filePath = dirname(Yii::app()->request->scriptFile) . '/www/' . $this->directory . '/';
        $filename = $this->filename . '.' . $this->extension;
        $url = $filePath . $filename;

	// remove the base64 part
	$base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $imageBase64);
	$base64 = base64_decode($base64);

	$source = imagecreatefromstring($base64); // create

	imagejpeg($source, $url, 100); // save image

        return array(
            'filename' => $filename,
        );
    }

}