<?php
/**
 * Created by Josh Warner - Method Studios
 * Date: 13.06.14
 */

class GoogleApi extends CApplicationComponent {

    // Credentials can be obtained at https://code.google.com/apis/console
    // See http://code.google.com/p/google-api-php-client/wiki/OAuth2 for more information
    const DRIVE_SCOPE = 'https://www.googleapis.com/auth/analytics';
    const SERVICE_CLIENT_ID = '682491475404-f8de5clm9i7ak8brcbhp58g17mnn017a.apps.googleusercontent.com';
    const SERVICE_ACCOUNT_EMAIL = '682491475404-f8de5clm9i7ak8brcbhp58g17mnn017a@developer.gserviceaccount.com';
    const SERVICE_ACCOUNT_PKCS12_FILE_NAME = '9694b912bcec738ac8ce733a4a6a08e468a23c66-privatekey.p12';

    public $webPropertyId; // Should be populated through main config
    public $client;
    public $analytics;

    public function init()
    {
        $path = dirname(__FILE__) . '/google-api-php-client/src/';
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);

        parent::init();
    }

    public function getAnalytics()
    {
        if (empty($this->analytics))
        {
            $this->setAnalytics();
        }

        return $this->analytics;
    }

    private function setAnalytics()
    {
        require_once('Google/Client.php');
        require_once('Google/Service/Analytics.php');

        $this->analytics = new Google_Service_Analytics($this->getClient());
    }

    private function getClient()
    {
        if (empty($this->client))
        {
            $this->setClient();
        }

        return $this->client;
    }

    private function setClient()
    {
        $this->client = $this->createClient();
    }

    private function createClient()
    {
        require_once 'Google/Client.php';

        $client = new Google_Client();
        $client->setApplicationName('Analytics');

        $key = dirname(__FILE__) . '/' . self::SERVICE_ACCOUNT_PKCS12_FILE_NAME;

        $client->setAssertionCredentials(
            new Google_Auth_AssertionCredentials(
                self::SERVICE_ACCOUNT_EMAIL,
                array('https://www.googleapis.com/auth/analytics.readonly'),
                file_get_contents($key)
            )
        );

        $client->setClientId(self::SERVICE_CLIENT_ID);
        $client->setAccessType('offline_access'); // May not be reqd

        return $client;
    }


}