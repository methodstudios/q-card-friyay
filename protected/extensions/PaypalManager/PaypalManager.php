<?php
/*
 * This class is used to proces paypal payments using the IPN method
 * See below link for more details
 * https://developer.paypal.com/webapps/developer/docs/classic/products/instant-payment-notification/
 *
 * NOTE: You need two actionMethod methods in your controller to use paypal.
 * 1. A method to sendPayment which can be done when your use submits their form or whatever the same time you do $model->save()
 * 2. A method to processPayment so the payment is verified and the status of the transaction updated. - the link to this action needs to be your $notifyUrl
 */
class PaypalManager
{
    const LOG_FILE = './protected/runtime/ipn.log'; // Populate error logs to a file if required
    const DEV_URL = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    const PROD_URL = 'https://www.paypal.com/cgi-bin/webscr';

    public $debug = false;
    public $paypalUrl;

    public $paypalEmail; // Your paypal account holder
    public $returnUrl; // Payment successful - http://example.com/payment-successful
    public $cancelUrl; // Payment cancelled - http://example.com/payment-cancelled
    public $notifyUrl; // Process payment page - http://example.com/process-payment

    public function __construct($paypalEmail = null, $returnUrl = null, $cancelUrl = null, $notifyUrl = null)
    {
        if (IS_DEVELOPMENT || IS_STAGING)
            $this->paypalUrl = self::DEV_URL;
        else
            $this->paypalUrl = self::PROD_URL;

        if ($paypalEmail) $this->paypalEmail = $paypalEmail;
        if ($returnUrl) $this->returnUrl = $returnUrl;
        if ($cancelUrl) $this->cancelUrl = $cancelUrl;
        if ($notifyUrl) $this->notifyUrl = $notifyUrl;
    }

######-------------------------------------------------------------------------------------------------------------#####
#                                                   STEP ONE
######-------------------------------------------------------------------------------------------------------------#####

    /*
     * sendPayment
     * This method will build the data for paypal, and redirect the client to the payment page
     * @param $items (array) - You can send either:
        $items = array('name'=>'Item Name','amount'=>'Item Price')
        OR
        $items = array(
            array(
              'name'=>'Item 1 Name',
              'amount'=>'Item 1 Price'
            ),
            array(
              'name'=>'Item 2 Name',
              'amount'=>'Item 2 Price'
            ),*
        )
     */
    public function sendPayment(array $items, $reference = null, $firstName = null, $lastName = null, $email = null)
    {
        if ($this->validateRequest())
        {
            $request = '';
            $request .= "?business=" . urlencode($this->paypalEmail) . "&";

            $request .= $this->processItems($items);

            $paypalData = $this->buildPaypalData($firstName, $lastName, $email);
            $request .= http_build_query($paypalData) . '&';

            $request .= "return=" . urlencode(stripslashes($this->returnUrl)) . "&";
            $request .= "cancel_return=" . urlencode(stripslashes($this->cancelUrl)) . "&";
            $request .= "notify_url=" . urlencode(stripslashes($this->notifyUrl)) . "&";

            if (!empty($reference))
                $request .= "custom=" . $reference;

            // Redirect to paypal IPN
            header('Location: ' . $this->paypalUrl . $request);
            exit();
        }
        else
        {
            throw new Exception('Paypal URL and Email variables must be populated before you can send a payment.');
        }
    }

    private function validateRequest()
    {
        if (empty($this->paypalEmail)) return false;
        if (empty($this->returnUrl)) return false;
        if (empty($this->cancelUrl)) return false;
        if (empty($this->notifyUrl)) return false;

        return true;
    }

    private function processItems($items)
    {
        if (!isset($items[0]) && !isset($items['name']))
        {
            throw new Exception('Your $items array is either empty or has invalid data.');
        }

        if (!isset($items[0]) && isset($items['name']) && isset($items['amount']))
        {
            $temp = $items;
            $items = array();
            $items[] = $temp;
        }

        $request = '';
        $cnt = 1;
        foreach ($items as $item)
        {
            $request .= "item_name_" . $cnt . "=" . urlencode($item['name']) . "&";
            $request .= "amount_" . $cnt . "=" . urlencode($item['amount']) . "&";
            $cnt++;
        }

        return $request;
    }

    private function buildPaypalData($firstName, $lastName, $email, $currency = 'NZD', $country = 'NZ')
    {
        $data = array();
        $data['cmd'] = '_cart';
        $data['upload'] = '1';
        $data['no_note'] = '1';
        $data['lc'] = $country;
        $data['currency_code'] = $currency;
        $data['bn'] = 'PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest';
        $data['first_name'] = $firstName;
        $data['last_name'] = $lastName;
        $data['payer_email'] = $email;
        $data['no_shipping'] = 1; // 0 is default to show shipping but not mandatory, 1 hides shipping address checkbox, 2 makes shipping details mandatory
        //$data['item_number'] = '';

        return $data;
    }

######-------------------------------------------------------------------------------------------------------------#####
#                                                   STEP TWO
######-------------------------------------------------------------------------------------------------------------#####

    public function processPayment()
    {
        // Clean session
        $_SESSION['Reference'] = '';

        // Check if paypal request or response
        if (isset($_POST["txn_id"]) && isset($_POST["txn_type"]))
        {
            // Read the post from PayPal system and add 'cmd'
            $request = 'cmd=_notify-validate';
            foreach ($_POST as $key => $value)
            {
                $value = urlencode(stripslashes($value));
                $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value); // IPN fix
                $request .= "&$key=$value";
            }

            // Assign posted variables to local variables
            $data['Transactions']['PaymentStatus']  = $_POST['payment_status'];
            $data['Transactions']['TotalAmount']    = $_POST['mc_gross'];
            $data['Transactions']['Currency']       = $_POST['mc_currency'];
            $data['Transactions']['PaypalTxnId']    = $_POST['txn_id'];
            $data['Transactions']['ReceiverEmail']  = $_POST['receiver_email'];
            $data['Transactions']['PayerEmail']     = $_POST['payer_email'];
            $data['Reference']                      = $_POST['custom'];

            // Add to session for use in controller
            $_SESSION['Reference'] = $data['Reference'];

            return $this->validatePayment($request, $data);
        }
    }

    // Validate the first response from paypal then send it back for verification
    private function validatePayment($request, $data)
    {
        $ch = curl_init($this->paypalUrl);
        if ($ch == false)
            return false;

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

        // Set TCP timeout to 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        $res = curl_exec($ch);
        if (curl_errno($ch) != 0) // cURL error
        {
            curl_close($ch);
            exit;
        }
        else
        {
            if ($this->debug)
            {
                // Log the entire HTTP response since we will only have a small number of requests anyway
                error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $request" . PHP_EOL, 3, self::LOG_FILE);
                error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, self::LOG_FILE);
            }

            curl_close($ch);
        }

        if (strcmp($res, "VERIFIED") == 0)
        {
            return $data;
        }
        //else if (strcmp ($res, "INVALID") == 0)
        else
        {
            if ($this->debug)
            {
                error_log(date('[Y-m-d H:i e] '). "Invalid Response: HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $request" . PHP_EOL, 3, self::LOG_FILE);
                error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, self::LOG_FILE);
            }

            throw new Exception('Invalid Response<br />Response: ' . $res . '<br />Data sent = ' . $request);
        }
    }

}