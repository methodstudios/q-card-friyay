<?php
/*
 * Example controller
 * You may change the `ProductId` reference to match your site
 * ie. change `ProductId` to `TicketId` if you are selling tickets etc
 */
class ExampleController extends FrontendController
{
    public $paypalManager;

    public function init()
    {
        if (IS_DEVELOPMENT || IS_STAGING)
        {
            $paypalEmail = 'dev@methodstudios.co.nz';
            $paypalUrl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
            $returnUrl = 'http://staging.methodstudios.co.nz/mcdowellparkinsons.co.nz/thanks';
            $cancelUrl = 'http://staging.methodstudios.co.nz/mcdowellparkinsons.co.nz/book-tickets';
            $notifyUrl = 'http://staging.methodstudios.co.nz/mcdowellparkinsons.co.nz/process-payment';
        }
        else
        {
            $paypalEmail = '';
            $paypalUrl = 'https://www.paypal.com/cgi-bin/webscr';
            $returnUrl = 'http://www.mcdowellparkinsons.co.nz/thanks';
            $cancelUrl = 'http://www.mcdowellparkinsons.co.nz/book-tickets';
            $notifyUrl = 'http://www.mcdowellparkinsons.co.nz/process-payment';
        }

        $this->paypalManager = new PaypalManager($paypalEmail, $returnUrl, $cancelUrl, $notifyUrl);
    }

    public function actionBuyProducts()
    {
        $model = new Products;

        if (!empty($_POST['Products']))
        {
            $model->attributes = $_POST['Products'];

            if (!empty($model->Quantity))
                $model->TotalAmount = $model->Quantity * $this->ticketPrice;

            if ($model->validate())
            {
                $items = array();
                for ($i = 1; $i <= $model->Quantity; $i++)
                {
                    $items[] = array(
                        'name' => '1x Product',
                        'amount' => '5.00',
                    );
                }

                $model->save(); // Save the "pending" tickets
                $this->paypalManager->sendPayment($items, $model->Reference, $model->Name, '', $model->Email);
            }
        }

        $this->render('buyProducts', array(
            'model' => $model,
        ));
    }

    public function actionThanks()
    {
        $this->render('thanks', array());
    }

    public function actionProcessPayment()
    {
        try
        {
            $data = $this->paypalManager->processPayment();

            if (!empty($data) && is_array($data))
            {
                $this->saveTransaction($data);
            }
        }
        catch (Exception $e)
        {
            // Log the error and email $errors@ so we can see track what happened
            $this->sendErrorEmail('Exception occurred during payment process', $e->getMessage());
            error_log(date('[Y-m-d H:i e] '). "Exception occurred during payment process: $e->getMessage()" . PHP_EOL, 3, PaypalManager::LOG_FILE);

            // Save the transaction as failed
            if (!empty($_SESSION['Reference']))
            {
                $model = new Products;
                $model->Status = Products::STATUS_FAILED;
                $model->save();
            }
        }
    }

    private function saveTransaction($data)
    {
        $model = Products::model()->findByAttributes(array('Reference' => $data['Reference']));

        $transaction = new Transactions;
        $transaction->attributes = $data['Transactions'];
        $transaction->ProductId = $model->getPrimaryKey();
        $transaction->save(false); // Save transaction without validation since at this stage we have received the money from the user

        $model->Status = Products::STATUS_APPROVED;
        $model->save();
    }
}

