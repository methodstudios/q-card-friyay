<?php
/*
 * Example migration
 * You may change the `products` and `ProductId` references to match your site
 * ie. change `products` to `tickets` and `ProductId` to `TicketId` if you are selling tickets etc
 */
class m140930_013846_transaction_table extends CDbMigration
{
	public function up()
	{
            $this->execute("CREATE TABLE `transactions` (
                `TransactionId` int(11) NOT NULL AUTO_INCREMENT,
                `PaypalTxnId` varchar(30) NOT NULL,
                `ProductId` int(11) DEFAULT NULL,
                `ItemName` varchar(50) DEFAULT NULL,
                `TotalAmount` decimal(13,2) NOT NULL DEFAULT '0.00',
                `Currency` varchar(3) DEFAULT NULL,
                `ReceiverEmail` varchar(100) DEFAULT NULL,
                `PayerEmail` varchar(100) DEFAULT NULL,
                `PaymentStatus` varchar(30) DEFAULT NULL,
                `Created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY (`TransactionId`),
                UNIQUE KEY `PaypalTxnId` (`PaypalTxnId`),
                KEY `ProductId` (`ProductId`)
              ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

            $this->execute("ALTER TABLE `transactions`
                ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`ProductId`) REFERENCES `products` (`ProductId`);");
	}

	public function down()
	{
            $this->execute("ALTER TABLE `transactions` DROP FOREIGN KEY `transactions_ibfk_1`;");
            $this->execute("DROP TABLE `transactions`;");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}