<?php
/*
 * Example model
 * You may change the `product` and `ProductId` references to match your site
 * ie. change `product` to `ticket` and `ProductId` to `TicketId` if you are selling tickets etc
 */


/**
 * This is the model class for table "transactions".
 *
 * The followings are the available columns in table 'transactions':
 * @property integer $TransactionId
 * @property string $PaypalTxnId
 * @property integer $ProductId
 * @property string $ItemName
 * @property string $TotalAmount
 * @property string $Currency
 * @property string $ReceiverEmail
 * @property string $PayerEmail
 * @property string $PaymentStatus
 * @property string $Created
 *
 * The followings are the available model relations:
 * @property Products $product
 */
class Transactions extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'transactions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('PaypalTxnId', 'required'),
            array('PaypalTxnId', 'unique'),
            array('ProductId', 'numerical', 'integerOnly'=>true),
            array('PaypalTxnId, PaymentStatus', 'length', 'max'=>30),
            array('ItemName', 'length', 'max'=>50),
            array('TotalAmount', 'numerical', 'integerOnly'=>false, 'min'=>1),
            array('Currency', 'length', 'max'=>3),
            array('ReceiverEmail, PayerEmail', 'length', 'max'=>100),
            array('Created', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('TransactionId, PaypalTxnId, ProductId, ItemName, TotalAmount, Currency, ReceiverEmail, PayerEmail, PaymentStatus, Created', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'product' => array(self::BELONGS_TO, 'Products', 'ProductId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'TransactionId' => 'Transaction',
            'PaypalTxnId' => 'Paypal Txn Id',
            'ProductId' => 'Product',
            'ItemName' => 'Item Name',
            'TotalAmount' => 'Total Amount',
            'Currency' => 'Currency',
            'ReceiverEmail' => 'Receiver Email',
            'PayerEmail' => 'Payer Email',
            'PaymentStatus' => 'Payment Status',
            'Created' => 'Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('TransactionId',$this->TransactionId);
        $criteria->compare('PaypalTxnId',$this->PaypalTxnId,true);
        $criteria->compare('ProductId',$this->ProductId);
        $criteria->compare('ItemName',$this->ItemName,true);
        $criteria->compare('TotalAmount',$this->TotalAmount,true);
        $criteria->compare('Currency',$this->Currency,true);
        $criteria->compare('ReceiverEmail',$this->ReceiverEmail,true);
        $criteria->compare('PayerEmail',$this->PayerEmail,true);
        $criteria->compare('PaymentStatus',$this->PaymentStatus,true);
        $criteria->compare('Created',$this->Created,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Transactions the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    // Check to make sure this paypal txn does not already exist
    public static function validTransaction($paypalTxnId)
    {
        $transaction = self::model()->findByAttributes(array('PaypalTxnId' => $paypalTxnId));

        if ($transaction)
            return false;

        return true;
    }
}
