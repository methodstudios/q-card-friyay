<div class="stores">
  <div class="centered-content">
    <h1 class="title">Where you can use your <span class="q-img"></span> card online</h1>

    <span class="view-all do-show-stores">View all</span>
    <span class="close do-hide-stores">Close</span>

    <div class="slider-wrapper">
      <div class="prev">
        <?php include('www/images/stores/arrow.svg'); ?>
      </div>
      <div class="slider">
        <a href="http://www.thewarehouse.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/1-warehouse.png" alt="Warehouse">
        </a>
        <a href="http://www.eventfinder.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/5-eventfinda.png" alt="Eventfinda">
        </a>
        <a href="http://www.warehousestationery.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/2-warehouse-stat.png" alt="Warehouse Stationery">
        </a>
        <a href="http://www.rebelsport.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/rebel.png" alt="Rebel">
        </a>
        <a href="http://www.briscoes.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/briscoes.png" alt="Briscoes">
        </a>
        <a href="http://www.grabone.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/3-grab-one.png" alt="Grab One">
        </a>
        <a href="http://www.farmers.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/farmers.png" alt="Farmers">
        </a>
        <a href="http://www.numberoneshoes.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/new---number-one-shoes.png" alt="Number one Shoes">
        </a>
        <a href="http://www.houseoftravel.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/hot-online.png" alt="Hot Online">
        </a>
        <a href="https://www.tvsn.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/tvsn.png" alt="Tvsn">
        </a>
        <a href="https://www.heathcotes.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/7-heathcote.png" alt="Heathcote">
        </a>
        <a href="http://www.burnsco.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/12-burnsco.png" alt="Burnsco">
        </a>
        <a href="http://murad.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/18-murad.png" alt="Murad">
        </a>
        <a href="http://smashboxcosmetics.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/new---smashbox.png" alt="Smashbox">
        </a>
        <a href="http://www.jumpflex.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/26-jumpflex.png" alt="Jumpflex">
        </a>
        <a href="http://www.bikebarn.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/13-bike-barn.png" alt="Bike Barn">
        </a>
        <a href="http://www.bigyardgolfballs.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/15-big-yard.png" alt="Big Yard">
        </a>
        <a href="http://www.motomail.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/31-motomail.png" alt="Motomail">
        </a>
        <a href="http://www.chances.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/new---chances.png" alt="Chances">
        </a>
        <a href="http://www.iticket.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/iticket.png" alt="Iticket">
        </a>
        <a href="http://www.bettaelectrical.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/36-beta.png" alt="Betta Electrical">
        </a>
        <a href="http://www.pbtech.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/25-pbtech.png" alt="Pbtech">
        </a>
        <a href="http://www.jacobsdigital.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/27-jacobs.png" alt="Jacobs">
        </a>
        <a href="http://stirlingsports.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/29-stirling.png" alt="Stirling">
        </a>
        <a href="http://www.evolutioncycles.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/9-evo-cycles.png" alt="Evo cycles">
        </a>
        <a href="http://www.kiwiautohomes.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/38-kiwi.png" alt="Kiwi">
        </a>
        <a href="http://www.lightingplus.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/10-lighting-plus.png" alt="Lighting Plus">
        </a>
        <a href="http://www.potblack.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/23-potblack.png" alt="Potblack">
        </a>
        <a href="http://www.homeimprovement.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/34-home.png" alt="Home">
        </a>
        <a href="http://www.toyco.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/32-toyco.png" alt="Toyco">
        </a>
        <a href="http://www.yesshop.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/37-yesshop.png" alt="Yesshop">
        </a>
        <a href="http://www.backdoor.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/16-backdoor.png" alt="Backdoor">
        </a>
        <a href="http://www.paperplus.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/39-paperplus.png" alt="Paperplus">
        </a>
        <a href="http://shop.boardertown.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/28-boardertown.png" alt="Boardertown">
        </a>
        <a href="http://www.pcforce.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/44-pc-force.png" alt="Pc Force">
        </a>
        <a href="http://www.christies.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/55---christies.png" alt="Christies">
        </a>
        <a href="http://www.watchesonline.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/49-watches.png" alt="Watches">
        </a>
        <a href="http://www.playtech.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/30-playtech.png" alt="Playtech">
        </a>
        <a href="http://www.asurestay.com/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/41-asure.png" alt="Asure">
        </a>
        <a href="http://www.equipfitnz.com/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/40-equipfit.png" alt="Equipfit">
        </a>
        <a href="http://www.kilwell.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/20-kilwell.png" alt="Kilwell">
        </a>
        <a href="http://www.babyonline.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/46-baby-online.png" alt="Baby Online">
        </a>
        <a href="http://www.learningstaircase.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/51-learning.png" alt="Learning">
        </a>
        <a href="http://www.parallelimported.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/48-parallel.png" alt="Parallel">
        </a>
        <a href="http://www.computerlounge.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/45-computer-lounge.png" alt="Computer Lounge">
        </a>
        <a href="http://www.gitawaypromotions.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/42-gitaway.png" alt="Gitaway">
        </a>
        <a href="http://www.fitnessandleisure.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/50-fitness.png" alt="Fitness">
        </a>
        <a href="http://www.ryos.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/21-ryos.png" alt="Ryos">
        </a>
        <a href="http://www.albanyextreme.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/47-barrels.png" alt="Barrels">
        </a>
        <a href="http://www.rockshop.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/rockshop.png" alt="Rockshop">
        </a>
        <a href="http://www.animates.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/animates.png" alt="Animates">
        </a>
        <a href="http://www.babyfactory.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/53-baby-factory.png" alt="Baby Factory">
        </a>
        <a href="http://www.tradedepot.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/58---trade-depot.png" alt="Trade Depot">
        </a>
        <a href="http://www.hyper.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/new---hyperride.png" alt="Hyperride">
        </a>
        <a href="http://www.nzgolfmagazine.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/56---nz-golf.png" alt="NZ Golf">
        </a>
        <a href="http://www.lastseason.co.nz/?gclid=CMTxpqrMr8ACFVYHvAodOF0AgQ" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/54-last-season.png" alt="Last Season">
        </a>
        <a href="http://www.mrvintage.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/mr-vintage.png" alt="Mr Vintage">
        </a>
        <a href="https://www.postie.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/6-postie.png" alt="Postie">
        </a>
        <a href="http://www.rchobbies.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/57---rc-hobbies.png" alt="RC Hobbies">
        </a>
        <a href="http://www.outdooraction.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/59---outdoor.png" alt="Outdoor">
        </a>
        <a href="http://travelcafe.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/travelcafe.png" alt="Travelcafe">
        </a>
        <a href="http://www.bellybeyond.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/belly-beyond.png" alt="Belly Beyond">
        </a>
        <a href="http://www.treatme.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/treatme.png" alt="Treatme">
        </a>
        <a href="http://www.tradetested.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/tradetested.png" alt="Tradetested">
        </a>
        <a href="http://www.bbqsandmore.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/bbqs-and-more.png" alt="Bbqs and More">
        </a>
        <a href="http://www.barkersonline.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/barkers.png" alt="Barkers">
        </a>
        <a href="http://www.livingandgiving.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/living-and-giving.png" alt="Living and Giving">
        </a>
        <a href="http://www.officecentre.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/officecentre.png" alt="Officecentre">
        </a>
        <a href="https://www.amazonsurf.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/amazon.png" alt="Amazon">
        </a>
        <a href="http://www.champions.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/champions.png" alt="Champions">
        </a>
        <a href="https://www.mtruapehu.com/winter/season-passes/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/mt-ruapehu.png" alt="Mt Ruapehu">
        </a>
        <a href="http://www.golfwarehouse.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/golf-warehouse.png" alt="Golf Warehouse">
        </a>
        <a href="http://www.2moroit.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/2moro-it.png" alt="Oro-it">
        </a>
        <a href="http://www.nzwinesociety.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/wine-society.png" alt="Wine Society">
        </a>
        <a href="http://www.airtimehoops.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/airtime-hoops.png" alt="Airtime Hoops">
        </a>
        <a href="http://www.nzpetdoctors.co.nz" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/pet-doctors.png" alt="Pet Doctors">
        </a>
        <a href="http://www.walkerandhall.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/8-walker-and-hall.png" alt="Walker and Hall">
        </a>
        <a href="http://www.homeoutlet.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/home-outlet.png" alt="Home Outlet">
        </a>
        <a href="http://www.kandk.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/14-k-k.png" alt="K K">
        </a>
        <a href="http://www.bigmanclothing.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/new---bigman-clothing.png" alt="Bigman Clothing">
        </a>
        <a href="http://www.kiwiplay.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/kiwi-play.png" alt="Kiwi-play">
        </a>
        <a href="http://kings.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/kings.png" alt="Kings">
        </a>
        <a href="https://www.sprintfit.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/sprintfit.jpg" alt="Sprintfit">
        </a>
        <a href="http://www.babies.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/babies.png" alt="Babies">
        </a>
        <a href="http://www.alta.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/alta.png" alt="Alta">
        </a>
        <a href="http://www.toyworld.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/11-toyworld.png" alt="Toyworld">
        </a>
        <a href="http://www.rapalloav.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/rapallo.png" alt="Rapallo">
        </a>
        <a href="http://timberland.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/timberland-online.png" alt="Timberland Online">
        </a>
        <a href="http://empireskate.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/empire-skate.png" alt="Empire Skate">
        </a>
        <a href="http://www.onitsukatiger.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/onitsuka.png" alt="Onitsuka">
        </a>
        <a href="http://www.bedpost.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/bedpost.png" alt="Bedpost">
        </a>
        <a href="https://www.cyclone.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/cyclone.png" alt="Cyclone">
        </a>
        <a href="http://www.nzwatchstore.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/nz-watch-store.png" alt="NZ Watch Store">
        </a>
        <a href="http://ecovaluespas.com/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/eco-spa.png" alt="Eco Spa">
        </a>
        <a href="http://www.fenmore.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/fenmore.png" alt="Fenmore">
        </a>
        <a href="http://homekit.co.nz/index.php?route=common/home" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/home-kit.png" alt="Home Kit">
        </a>
        <a href="http://www.babycity.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/new-babycity.png" alt="Babycity">
        </a>
        <a href="http://www.bigbrandsonline.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/bigbrands.png" alt="Bigbrands">
        </a>
        <a href="http://www.hangarone.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/hangar-one.png" alt="Hangar One">
        </a>
        <a href="http://www.chocolatetraders.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/35-chocolate.png" alt="Chocolate">
        </a>
        <a href="http://www.vineonline.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/vol_qcard.png" alt="Vol Qcard">
        </a>
        <a href="http://www.freemanx.com/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/1-freeman.png" alt="Freeman">
        </a>
        <a href="http://www.theperfumebar.co.nz/" target="_blank" class="slider-item">
          <img src="<?php echo $this->getImagesPath(); ?>stores/the-perfume-bar.jpg" alt="The Perfume Bar">
        </a>
      </div>
      <div class="next">
        <?php include('www/images/stores/arrow.svg'); ?>
      </div>
    </div>
  </div>
</div>
