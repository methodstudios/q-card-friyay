<?php
$competionOn = Entries::competitionActive();
$shoppingOn = Entries::shoppingActive();

$shoppingFinished = false;
if (!$competionOn && !$shoppingOn)
    $shoppingFinished = true;

// Use rounded totals and $days for when comp has ENDED
$roundedTotals = Entries::getRoundedTotals();
$days = Entries::getDaysArray();
?>

<main class="like">

  <div class="centered-content<?php echo $shoppingOn ? ' shop-today' : '' ?> <?php echo $shoppingFinished ? ' shop-finished' : '' ?>">
    <?php
    // If the competition is running and users can add likes
    if ($competionOn)
    {
        echo $this->renderPartial('/competition/_active');
    }
    // If we are on the Entries::SHOPPING_DATE and users can shop to recieve the special
    elseif ($shoppingOn)
    {
        echo $this->renderPartial('/competition/_shopping', array(
          'roundedTotals'=>$roundedTotals,
          'days'=>$days,
        ));
    }
    // If the competition has closed and users are unable to shop today
    else
    {
        echo $this->renderPartial('/competition/_finished');
    }
    ?>
  </div>
</main>




