<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2>Error <?php echo  $type . ':' . $code; ?></h2>

<div class="error">
    <p><?php echo CHtml::encode($message); ?></p>
    <?php if (IS_DEVELOPMENT || IS_STAGING): ?>
        <p>File: <?php echo $file; ?></p>
        <p>Line: <?php echo $line; ?></p>
        <p>Trace: <?php echo nl2br($trace); ?></p>
        <p>Traces: <?php echo '<pre>' . print_r($traces,1) . '</pre>'; ?></p>
    <?php endif; ?>
</div>