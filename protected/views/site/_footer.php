<?php
$competionOn = Entries::competitionActive();
$latestTotal = Entries::getLatestTotal();
$roundedTotals = Entries::getRoundedTotals();

$roundedMax = Entries::FIRST_MAX_ENTRIES;
if ($latestTotal > $roundedMax)
    $roundedMax = Entries::MAX_ENTRIES;
?>

<footer class="footer">
  <div class="centered-content">
    <h1 class="footer-title">Don't have a <span class="q-img"></span> Card? <a href="http://www.qcard.co.nz/index.php/applying-is-easy" class="apply-btn" target="_blank">Apply now</a></h1>
    <div class="social">
      <a href="https://www.facebook.com/qcard" target="_blank" class="fb">Facebook</a>
      <a href="https://twitter.com/MyQCard" target="_blank" class="twitter">Twitter</a>
      <a href="https://instagram.com/myQCard" target="_blank" class="instagram">Instagram</a>
    </div>

    <?php if ($competionOn): ?>
        <p>*The number of no payments and no interest days ('Payment Holiday days') will be determined based on the number of ‘Likes’ on this Q Card promotional Facebook app page from 15 June 2015 to 8pm NZ Time on 16 June 2015. The maximum number of Payment Holiday days that can be reached is 210 days. Each ‘Like’ increases the Payment Holiday days by 0.1 days. The final Payment Holiday days reached at 8pm NZ Time on 16 June 2015 will be rounded to the nearest month to calculate the Payment Holiday term ('Payment Holiday term'). The Payment Holiday term will be announced on 17 June 2015 via this Facebook app page, and will apply to all online purchases made at participating online retailers on Q EFTPOS Plans on 19 June 2015 only. The entity responsible for this promotion is Consumer Finance Limited (Promoter). This promotion is not in any way sponsored, administered by, or associated with, Facebook. You agree to release Facebook from any and all claims and demands arising out of or in connection with your participation into this promotion. The Promoter reserves the right to verify the validity of entries and assumes no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorised access to or alteration of entries. The Promoter assumes no responsibility for any injury or damage to participants or any other person’s computer related to or resulting from participation in or downloading any materials in this promotion.<br /><br />
        ^Offer available to existing Q Cardholders only. No payments and no interest for the Payment Holiday term is available on all online purchases at participating online retailers on 19 June 2015 only on Q EFTPOS Plans. Account Fees may apply. Q Card Standard Interest Rate applies to any outstanding balance at end of Payment Holiday term. Q Card lending criteria, fees, terms and conditions apply.</p>
    <?php else: ?>
        <p>*The number of no payments and no interest days (‘Payment Holiday days’) will be determined based on the number of ‘Likes’ on this app page from 15 June 2015 to 8pm NZ Time on 16 June 2015. The maximum number of Payment Holiday days that can be reached is 200 days. Each ‘Like’ increases the Payment Holiday days by 0.1 days. The final Payment Holiday days reached at 8pm NZ Time on 16 June 2015 will be rounded to the nearest month to calculate the Payment Holiday term (‘Payment Holiday term’). The Payment Holiday term will be announced on 17 June 2015 via this Facebook app page and will apply to all online purchases made at participating online retailers on Q EFTPOS Plans on 19 June 2015 only. The entity responsible for this promotion is Consumer Finance Limited (Promoter). This promotion is not in any way sponsored, administered by, or associated with, Facebook. You agree to release Facebook from any and all claims and demands arising out of or in connection with your participation into this promotion. The Promoter reserves the right to verify the validity of entries and assumes no responsibility for any error, omission, interruption, deletion, defect, delay in operation or transmission, communications line failure, theft or destruction or unauthorised access to or alteration of entries. The Promoter assumes no responsibility for any injury or damage to participants or any other person’s computer related to or resulting from participation in or downloading any materials in this promotion.<br /><br />
        ^This information is provided by way of disclosure under the CCCFA by Consumer Finance Limited. Offer available to existing Q Cardholders only. Offer applies to online retailers that offer Q EFTPOS Plans. <?php echo $roundedTotals['days']; ?> days (<?php echo $roundedTotals['months']; ?> months) no payments and no interest (“Payment Holiday”) applies to online purchases at participating online retailers on Q EFTPOS Plans on 19 June 2015 only. Account Fees may apply. Q Card Standard Interest Rate applies to any outstanding balance at end of Payment Holiday. Q Card lending criteria, fees, terms and conditions apply.</p>
    <?php endif; ?>

    <h1 class="footer-title privacy">Privacy Policy</h1>
    <p>The Q Card Like it Up Privacy Policy describes how Q Card handles personal information collected during the use of applications by its users via the Facebook™ page.</p>

    <ol>
      <li>
        Upon participating in this promotion, Q Card will store the user’s IP address to ensure entries are unique.
      </li>
      <li>
        Your information will not be shared with any third party at any time except the agency administering the promotion as named in Point 3 below.
      </li>
      <li>
        For the duration of the promotion your details will be held at Method Studios Ltd.
      </li>
      <li>
        Your information will not be stored once the promotion has ended and will be deleted in its entirety at the conclusion of the promotion.
      </li>
      <li>
        You will not be contacted directly by Q Card for any purpose other than that described in Point 1.
      </li>
    </ol>

  </div>
</footer>
