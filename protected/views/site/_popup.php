<div class="popup-overlay">
  <div class="popup-wrapper">
    <p>
      Woohoo! You <strong class="strong highlight">Liked it Up</strong> to 200 days quicker than expected. How about we try to reach <strong class="strong">300 days</strong> instead?
    </p>
    <p>
      <strong class="strong">You've got until 8pm TONIGHT, so get liking and tell your friends.</strong>
    </p>

    <a href="#closePopup" class="close-link do-close-popup">close <span class="cross">x</span></a>
  </div>
</div>
