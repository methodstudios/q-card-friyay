<?php
// Disable Yii's version of jQuery
Yii::app()->clientScript->scriptMap=array('jquery.min.js'=>false);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php echo $this->renderPartial('/includes/_head'); ?>
</head>
<body <?php echo (!empty($this->redirectTrue) && IS_DESKTOP ? 'style="display:none;"' : ''); ?>>
  <div id="fb-root"></div>

  <div class="wrapper">
    <?php echo $this->renderPartial('/site/_header'); ?>
    <?php echo $this->renderPartial('/site/_like'); ?>
    <?php echo $this->renderPartial('/site/_stores'); ?>
    <?php echo $this->renderPartial('/site/_footer'); ?>
  </div>

  <?php echo $this->renderPartial('/site/_popup'); ?>

  <?php echo $this->renderPartial('/includes/_js'); ?>

  <script>

    function counter(newTotal){

        $('.counter').empty();

        var number = newTotal;
        var numArray = number.split("");

        for(var i=0; i<numArray.length; i++) {
            numArray[i] = parseInt(numArray[i], 10);
            $(".counter").append('<span class="digit-con"><span class="digit'+i+'">0<br>1<br>2<br>3<br>4<br>5<br>6<br>7<br>8<br>9<br></span></span>');
        }

        var increment = $('.digit-con').outerHeight();
        var speed = 1000;

        for(var i=0; i<numArray.length; i++) {
            $('.digit'+i).animate({top: -(increment * numArray[i])}, speed);
        }

        $(".digit-con:nth-last-child(2)").after("<span class='comma'>.</a>");
    }

    $(document).ready(function(){
        checkEntry();

        var totalDays    = App.total,
            totalDaysStr = totalDays.toString(),
            totalAnimate = totalDaysStr.replace('.','');

        counter(totalAnimate);

    });

    function reloadPage()
    {
        location.reload();
    }

    function storeEntry()
    {
        if (typeof(Storage) !== "undefined")
        {
            localStorage.setItem("submitted", "true");
        }
    }

    function checkEntry()
    {
        if (typeof(Storage) !== "undefined" && localStorage.getItem("submitted") == "true")
        {
            $('.stage1').hide();
            $('.stage2').show();
        }
    }

    function statusChangeCallback(response) {
        <?php if (IS_DESKTOP): ?>
            var redirect = true;
        <?php else: ?>
            var redirect = false;
        <?php endif; ?>

        if (response.status === 'connected' || response.status === 'not_authorized')
        {
            // Logged into FB - redirect to the app
            if (redirect)
            {
                window.location = 'https://www.facebook.com/qcard/app_391362677726955';
            }
            else
            {
                $('body').show();
                $('.fb-like').show();
            }
        }
        else
        {
            // Not logged into FB
            $('body').show();
            $('.fb-like').hide();
            $('.fb-login-button').show();

            /*FB.login(function(response) {
                if (response.authResponse)
                {
                    FB.api('/me', function(response) {
                        if (redirect)
                            window.location = 'https://www.facebook.com/qcard/app_391362677726955';
                        else
                            location.reload();
                    });
                }
                else
                {
                    // User cancelled login or did not fully authorize.
                    alert('You must be logged into facebook to view this page.');
                    window.location = 'https://www.facebook.com/';
                }
            });*/
        }
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId: '391362677726955',
            status: false,
            xfbml: true,
            version: 'v2.3'
        });

        <?php if (!empty($this->redirectTrue)): ?>
            FB.getLoginStatus(statusChangeCallback, true);
        <?php endif; ?>

        FB.Event.subscribe('edge.create', function(response){
            var data = {};
            data['Entries[Name]'] = '';

            $.ajax({
                type: 'POST',
                url: App.path + 'site/sendLike',
                data: data,
                success: function(response) {
                    response = JSON.parse(response);
                    if (response.message === 'success')
                    {
                        storeEntry();
                        $('.stage1').hide();
                        $('.stage2').show();

                        //Update total for counter
                        App.total = response.amount;
                        var totalDaysNew    = App.total,
                            totalDaysStrNew = totalDaysNew.toString(),
                            totalAnimateNew = totalDaysStrNew.replace('.','');

                        counter(totalAnimateNew);
                    }
                    else
                    {
                        var array = response.message.split('<li>');
                        array.shift();
                        for (j = 0, len = array.length; j < len; j++)
                        {
                            var i = array[j];
                            var text = i.replace('</li>', '').replace('</ul></div>', '');
                            $('.error-message').append($('<li>').html(text));
                        }
                        $('.instructions').hide();
                        $('.error-message').removeClass('hidden');
                    }
                }
            });
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));


  </script>

</body>
</html>
