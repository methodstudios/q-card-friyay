<?php
$latestString = Entries::getLatestTotalAsString();
$days = array();
for ($i = 0; $i <= 4; $i++)
    $days[] = substr($latestString, $i, 1);
?>

<p class="lead">shop online friday 19 june to get</p>

      <div class="counter"></div>

      <div class="counter-copy">
        <span>days</span>
        <span class="smaller">and counting...</span>
      </div>

      <p class="lead larger">no&nbsp;payments, no&nbsp;interest no&nbsp;minimum spend</p>

      <!--  ======= Change href to point to app not page ========  -->

      <div class="insentive-wrapper">
        <div class="stage1">
          <span class="btn">
            <img src="<?php echo $this->getImagesPath(); ?>like/like-it-up.gif" alt="Like it up">
            <div class="fb-like do-like" data-href="https://app.msocial.co.nz/q-card-friyay" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
            <div class="fb-login-button" data-max-rows="1" data-size="small" data-show-faces="false" data-auto-logout-link="false" data-onlogin="reloadPage();" style="display:none;"></div>
          </span>

          <div class="error-message hidden">
          </div>

          <ol class="list instructions">
            <li>
              <span class="list-number">1</span>
              <span class="list-copy">Click 'LIKE' before 8pm Tuesday 16&nbsp;June to help bump the no payments and no interest period from 90 up to 200 days.</span>
            </li>
            <li>
              <span class="list-number">2</span>
              <span class="list-copy">Check back before you start shopping on Friday 19 June to see the final magic number.</span>
            </li>
            <li>
              <span class="list-number">3</span>
              <span class="list-copy">Shop online on Friday 19 June at <a class="scroll-store-btn" href="#">accepting stores</a> with the Q deal YOU helped choose.</span>
            </li>
          </ol>
        </div>
        <div class="stage2">
          <p class="liked-copy">
            <strong class="strong">Nice one!</strong> You've helped make the online Q deal for Friday 19 June even better. Make sure you tell your friends to 'Like it Up' too, the more likes, the better it is for everyone.
          </p>
          <p class="liked-copy">
            Don't want to miss out on more Q Card deals and offers? LIKE us on Facebook or <a href="http://www.qcard.co.nz/index.php/q-card-offers?view=promoitem" class="medium" target="_blank">Sign up to Q Deals</a> now.
          </p>
          <a href="http://www.qdeals.co.nz/prospects/join.html?__utma=1.2144358669.1422501133.1434065310.1434079220.128&__utmb=1.1.10.1434079220&__utmc=1&__utmx=-&__utmz=1.1422501133.1.1.utmcsr=(direct)%7cutmccn=(direct)%7cutmcmd=(none)&__utmv=-&__utmk=84452505" class="signup-link" target="_blank">Sign Up Now</a>
        </div>
      </div>