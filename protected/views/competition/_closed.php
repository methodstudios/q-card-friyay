<?php
$shoppingDate = DateTime::createFromFormat('Y-m-d H:i:s', Entries::SHOPPING_DATE);
?>

<p class="lead larger">shop online <?php echo $shoppingDate->format('l d F'); ?> to get</p>

<div class="final-count">
  <span class="final-count-figure"><?php echo $days[0]; ?></span>
  <span class="final-count-figure smaller"><?php echo $days[1]; ?></span>
  <span class="final-count-figure"><?php echo $days[2]; ?></span>
</div>

<div class="final-count-copy">
  <span>days</span>
  <p class="lead">no&nbsp;payments, no&nbsp;interest no&nbsp;minimum spend^</p>
</div>

<p class="liked-copy">
  <strong class="strong">Woohoo, you helped choose the Q Card deal! For one day only, you’ll get an incredible <?php echo $roundedTotals['days']; ?> days no payments and no interest^ on all online purchases with Q Card, and there’s no minimum spend.</strong>
  <br /><br />
  This <?php echo $shoppingDate->format('l d F'); ?> only at all accepting <a class="scroll-store-btn" href="#">online merchants</a>.
</p>
