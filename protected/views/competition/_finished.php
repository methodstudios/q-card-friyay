
<div class="final-count">
  <span class="final-count-figure">2</span>
  <span class="final-count-figure smaller">0</span>
  <span class="final-count-figure">0</span>
</div>

<div class="final-count-copy">
  <span>days</span>
  <p class="lead">no&nbsp;payments, no&nbsp;interest no&nbsp;minimum spend^</p>
</div>

<p class="liked-copy">
  <strong class="strong">Woohoo, great work to everyone who ‘Liked it Up’! The Q Card online extravaganza has now closed and those who shopped online on Friday 19 June enjoyed a massive 200 days no payments and no interest^ on their purchases.</strong>
</p>
