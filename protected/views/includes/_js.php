<?php if (IS_DEVELOPMENT) : ?>
  <script src="<?php echo $this->getJsPath() ?>vendor/jquery-2.1.4.min.js"></script>
  <script src="<?php echo $this->getJsPath() ?>vendor/owl.carousel.min.js"></script>
  <script src="<?php echo $this->getJsPath() ?>vendor/moment.min.js"></script>
  <script src="<?php echo $this->getJsPath() ?>vendor/readable-range.js"></script>
  <script src="<?php echo $this->getJsPath() ?>application.js"></script>

<?php else: ?>
  <script src="<?php echo $this->getJsPath() ?>application.min.js?<?php echo time(); ?>"></script>
<?php endif; ?>