<?php
$competionOn = Entries::competitionActive();
$latestString = Entries::getLatestTotalAsString();
?>

<title>Like it Up</title>
<meta charset="utf-8">
<meta name="description" content="Friyay">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta property="og:title" content="Like It Up" />
<meta property="og:site_name" content="Q Card Like It Up"/>
<meta property="fb:app_id" content="391362677726955" />
<meta property="og:description" content="Q Card Like It Up" />
<meta property="og:url" content="https://app.msocial.co.nz/q-card-friyay/" />
<meta property="og:image" content="http://app.msocial.co.nz/q-card-friyay/www/images/like/like-it-up-fb-lge.png" />
<meta property="og:image:secure_url" content="https://app.msocial.co.nz/q-card-friyay/www/images/like/like-it-up-fb-lge.png" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="600" />

<link rel="icon" href="<?php echo $this->getImagesPath(); ?>layout/favicon.ico" type="image/x-icon"/>

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->

<script>
  window.App = {};
  App.path = "<?php echo $this->getBaseUrl(); ?>";
  App.total = "<?php echo $latestString; ?>";
  App.endDate = "<?php echo Entries::SHOPPING_DATE; ?>";
  App.competitionOn = <?php echo ($competionOn ? 'true' : 'false') ?>;
</script>

<?php if (IS_DEVELOPMENT) : ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>vendor/normalize.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>vendor/owl.carousel.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>application.css" />
<?php else: ?>
  <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>application.min.css?<?php echo time(); ?>" />
<?php endif ?>

<?php
/* TO ADD GOOGLE ANALYTICS SIMPLY ADD THE GA CODE IN BACKEND SETTINGS AREA */
if (!IS_DEVELOPMENT && !IS_STAGING && $code = Yii::app()->webSettings->getValue('GA_CODE')):
?>
    <script type="text/javascript">
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '<?php echo $code; ?>', 'auto');
        ga('require', 'displayfeatures'); // Add demographics by default
        ga('send', 'pageview');
    </script>
<?php
endif;
?>