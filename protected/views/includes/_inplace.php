<?php
/*
 * Include all in place editing dependencies and initialization scripts
 */
?>
<script>
    var baseUrl = '<?php echo $this->getBaseUrl(); ?>';
    var basePath = '<?php echo $this->getBasePath(); ?>';
</script>
<script src="http://cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>
<script src="<?php echo $this->getBasePath() ?>ckeditor/custom.js"></script>
<link type="text/css" rel="stylesheet" href="<?php echo $this->getBasePath() ?>ckeditor/custom.css" />