<?php

class GoogleController extends BackendController
{
    const REF_DIRECT = '(none)';
    const REF_ORGANIC = 'organic';
    const REF_REFERRAL = 'referral';

    public $referralsArr = array(
        self::REF_DIRECT => 'Direct',
        self::REF_ORGANIC => 'Organic',
        self::REF_REFERRAL => 'Referral',
    );

    public $allowedFilters = array(
        'week' => '-1 weeks',
        'month' => '-1 months',
        '3month' => '-3 months',
    );

    public $analytics;
    public $filter;

    public function __construct($id, $module = null)
    {
        $this->analytics = Yii::app()->googleApi->getAnalytics();

        parent::__construct($id, $module);
    }

    public function actionIndex()
    {
        if (!empty($_GET['filter']) && array_key_exists($_GET['filter'], $this->allowedFilters))
            $this->filter = $_GET['filter'];

        $this->render('index');
    }

    public function actionGetData()
    {
        if (!empty($_GET['filter']) && array_key_exists($_GET['filter'], $this->allowedFilters))
        {
            $startDate = date('Y-m-d', strtotime($this->allowedFilters[$_GET['filter']]));
            $endDate = date('Y-m-d', strtotime('-1 days'));
        }
        else
        {
            $startDate = date('Y-m-d', strtotime('-1 months'));
            $endDate = date('Y-m-d', strtotime('-1 days'));
        }

        $data = $this->getAllData($startDate, $endDate);
        echo json_encode($data);
    }

    private function getData($startDate, $endDate, $metrics = 'ga:visits', $dimensions = 'ga:date,ga:year,ga:month,ga:day', $optParams = array())
    {
        $optParams['dimensions'] = $dimensions;

        return $this->analytics->data_ga->get('ga:' . Yii::app()->googleApi->webPropertyId, $startDate, $endDate, $metrics, $optParams);
    }

    private function buildChartData($columns, $rows)
    {
        $data = array();
        $data['cols'] = array();
        $data['rows'] = array();

        foreach ($columns as $column)
        {
            $data['cols'][] = $column; // Needs 'label' and 'type' (string or number)
        }

        foreach ($rows as $row)
        {
            $label = $row['label'];
            $value = $row['value']; // Must be (int) if $column['type'] is number

            $data['rows'][] = array('c' => array(
                array('v' => $label, 'f' => null),
                array('v' => $value, 'f' => null),
            ));
        }

        return $data;

        // Working example of INPUT with test data
        /*$columns = array(
            array('label' => 'Topping', 'type' => 'string'),
            array('label' => 'Slices', 'type' => 'number'),
        );

        $rows = array(
            array('label' => 'Mushrooms', 'value' => 3),
            array('label' => 'Onions', 'value' => 1),
            array('label' => 'Olives', 'value' => 1),
            array('label' => 'Zucchini', 'value' => 1),
            array('label' => 'Pepperoni', 'value' => 2),
        );*/

        // Working example of OUTPUT with test data
        /*$data = array(
            'cols' => array(
                array(
                    'id' => '',
                    'label' => 'Topping',
                    'pattern' => '',
                    'type' => 'string', //string or number
                ),
                array(
                    'id' => '',
                    'label' => 'Slices',
                    'pattern' => '',
                    'type' => 'number', //string or number
                ),
            ),
            'rows' => array(
                array(
                    'c' => array(
                        array('v' => 'Mushrooms', 'f' => null),
                        array('v' => 3, 'f' => null),
                    ),
                ),
                array(
                    'c' => array(
                        array('v' => 'Onions', 'f' => null),
                        array('v' => 1, 'f' => null),
                    ),
                ),
                array(
                    'c' => array(
                        array('v' => 'Olives', 'f' => null),
                        array('v' => 1, 'f' => null),
                    ),
                ),
                array(
                    'c' => array(
                        array('v' => 'Zucchini', 'f' => null),
                        array('v' => 1, 'f' => null),
                    ),
                ),
                array(
                    'c' => array(
                        array('v' => 'Pepperoni', 'f' => null),
                        array('v' => 2, 'f' => null),
                    ),
                ),
            ),
        );*/
    }

    private function getAllData($startDate = null, $endDate = null)
    {
        if (empty($startDate))
            $startDate = date('Y-m-d', strtotime('-1 months'));
        if (empty($endDate))
            $endDate = date('Y-m-d', strtotime('-1 days'));

        try
        {
            #####----------------------------------------- HOSTNAME STARTS -----------------------------------------#####
            $content = $this->getData($startDate, $endDate, 'ga:visits', 'ga:hostname');
            $data['profileName'] = null;

            if (!empty($content['modelData']['profileInfo']['profileName']) && strpos($content['modelData']['profileInfo']['profileName'], '.') !== FALSE)
                $data['profileName'] = $content['modelData']['profileInfo']['profileName'];

            $profileRows = $content['rows'];
            // Reorder array to display the profile name with the most views first - this should be the main account
            usort($profileRows, function($a, $b){
                return $b[1] - $a[1];
            });

            if (!$data['profileName'] && !empty($profileRows[0][0]))
                $data['profileName'] = $profileRows[0][0];
            #####----------------------------------------- HOSTNAME ENDS -----------------------------------------#####

            #####----------------------------------------- PAGE VIEWS START -----------------------------------------#####
            $content = $this->getData($startDate, $endDate);

            $data['totalViews'] = 0;
            if (!empty($content->totalsForAllResults['ga:visits']))
                $data['totalViews'] = $content->totalsForAllResults['ga:visits'];

            if (!empty($content['rows']))
            {
                $columns = array(
                    array('label' => 'Date', 'type' => 'string'),
                    array('label' => 'Page Views', 'type' => 'number'),
                );

                $rows = array();
                foreach ($content['rows'] as $row)
                {
                    $rows[] = array(
                        'label' => date('d-M-y', strtotime($row[0])),
                        'value' => (int)$row[4],
                    );
                }

                $data['chartData'] = $this->buildChartData($columns, $rows);
            }
            #####----------------------------------------- PAGE VIEWS END -----------------------------------------#####

            #####----------------------------------------- SOURCE MEDIUMS START -----------------------------------------#####
            $mediums = $this->getData($startDate, $endDate, 'ga:visits', 'ga:medium');
            $data['sourceMediums'] = array();

            if (!empty($mediums['rows']))
            {
                foreach ($mediums['rows'] as $row)
                {
                    $views = (!empty($row[1]) ? $row[1] : 0);

                    $percentage = null;
                    if ($views > 0 && $data['totalViews'] > 0)
                        $percentage = ($views / $data['totalViews']) * 100;

                    if ($row[0] == self::REF_DIRECT)        $data['sourceMediums'][self::REF_DIRECT] = $percentage;
                    elseif ($row[0] == self::REF_ORGANIC)   $data['sourceMediums'][self::REF_ORGANIC] = $percentage;
                    elseif ($row[0] == self::REF_REFERRAL)  $data['sourceMediums'][self::REF_REFERRAL] = $percentage;
                }
            }
            #####----------------------------------------- SOURCE MEDIUMS END -----------------------------------------#####

            #####----------------------------------------- REFERRALS START -----------------------------------------#####
            $referrals = $this->getData($startDate, $endDate, 'ga:visits', 'ga:sourceMedium');
            $data['referralData'] = array();
            $limit = 25;

            if (!empty($referrals['rows']))
            {
                foreach ($referrals['rows'] as $row)
                {
                    if (strpos($row[0], self::REF_REFERRAL) !== FALSE)
                    {
                        $key = trim(substr($row[0], 0, strpos($row[0], '/ referral')));
                        if (strlen($key) > $limit)
                        {
                            // If the domain is too long - replace middle characters with ...
                            $key = substr($key, 0, 11) . '...' . substr($key, (strlen($key)-11));
                        }

                        $data['referralData'][$key] = $row[1];
                    }
                }

                arsort($data['referralData']);
                $data['referralData'] = array_slice($data['referralData'], 0, 5);
            }
            #####----------------------------------------- REFERRALS END -----------------------------------------#####

            #####----------------------------------------- BOUNCE RATE STARTS -----------------------------------------#####
            $bounceRateData = $this->getData($startDate, $endDate, 'ga:bounceRate', null);
            $data['bounceRate'] = null;

            if (!empty($bounceRateData['rows'][0][0]))
            {
                $data['bounceRate'] = round($bounceRateData['rows'][0][0]);
            }
            #####----------------------------------------- BOUNCE RATE ENDS -----------------------------------------#####

            #####----------------------------------------- NEW/RETURNING USERS START -----------------------------------------#####
            $sessionData = $this->getData($startDate, $endDate, 'ga:percentNewSessions', null);
            $data['sessions'] = array();

            if (!empty($sessionData['rows'][0][0]))
            {
                $data['sessions']['new'] = round($sessionData['rows'][0][0]);
                $data['sessions']['returning'] = 100 - $data['sessions']['new'];
            }
            #####----------------------------------------- NEW/RETURNING USERS END -----------------------------------------#####

            #####----------------------------------------- PAGES/SESSION STARTS -----------------------------------------#####
            $pagesSessionData = $this->getData($startDate, $endDate, 'ga:pageviewsPerSession', null);
            $data['pagesSession'] = 0.0;

            if (!empty($pagesSessionData['rows'][0][0]))
            {
                $data['pagesSession'] = round($pagesSessionData['rows'][0][0], 1);
            }
            #####----------------------------------------- PAGES/SESSION ENDS -----------------------------------------#####

            #####----------------------------------------- AVG. SESSION DURATION STARTS -----------------------------------------#####
            $avgDurationData = $this->getData($startDate, $endDate, 'ga:avgSessionDuration', null);
            $data['avgDuration'] = 0.00;

            if (!empty($avgDurationData['rows'][0][0]))
            {
                $data['avgDuration'] = gmdate("H:i:s", $avgDurationData['rows'][0][0]);
            }
            #####----------------------------------------- AVG. SESSION DURATION ENDS -----------------------------------------#####

            #####----------------------------------------- GEO DATA STARTS -----------------------------------------#####
            $geoData = $this->getData($startDate, $endDate, 'ga:visits', 'ga:city,ga:country');
            $data['geoInfo'] = array();

            if (!empty($geoData['rows']))
            {
                foreach ($geoData['rows'] as $row)
                {
                    if ($row[0] == '(not set)' && $row[1] == '(not set)')
                        continue;

                    if ($row[0] == '(not set)')
                        $row[0] = 'Unknown';

                    $key = $row[0] . ' (' . $row[1] . ')';

                    $data['geoInfo'][$key] = $row[2];
                }

                arsort($data['geoInfo']);
                $data['geoInfo'] = array_slice($data['geoInfo'], 0, 5);
            }
            #####----------------------------------------- GEO DATA ENDS -----------------------------------------#####

            return $data;
        }
        catch(Exception $e)
        {
            // This needs some love as not every exception means that the token was invalidated
            throw $e;
        }
    }
}