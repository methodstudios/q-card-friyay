<?php

class RedactorController extends BackendController
{
    public $filename;
    public $uploadError;

    const IMAGE_UPLOAD_PATH = 'www/uploads/';
    const FILE_UPLOAD_PATH = 'www/file_uploads/';

    public function actionUploadImage()
    {
        $result = array();

        if ($this->uploadItem('image', 'file'))
        {
            $result = array(
                'filelink' => Yii::app()->baseUrl . '/' . self::IMAGE_UPLOAD_PATH . $this->filename,
                'filename' => $this->filename,
            );
        }
        else
        {
            $result = array(
                'error' => $this->uploadError,
            );
        }

        echo stripslashes(json_encode($result));
    }

    public function actionListImages()
    {
        // Scan the root uploads/ files first
        $result = $this->scanFiles();

        // Then add the subfolders and items
        $subFolders = $this->getSubFolders();
        if (!empty($subFolders))
        {
            foreach ($subFolders as $folder)
            {
                $result = array_merge($result, $this->scanFiles($folder));
            }
        }

        echo stripslashes(json_encode($result));
    }

    public function actionUploadFile()
    {
        $result = array();

        if ($this->uploadItem('file', 'file'))
        {
            $result = array(
                'filelink' => Yii::app()->baseUrl . '/' . self::FILE_UPLOAD_PATH . $this->filename,
                'filename' => $this->filename,
            );
        }
        else
        {
            $result = array(
                'error' => $this->uploadError,
            );
        }

        echo stripslashes(json_encode($result));
    }

    ######----------------------------------------------------------------------------------------------------######
    #                                       PRIVATE METHODS START
    ######----------------------------------------------------------------------------------------------------######

    private function getUploadsPath()
    {
        return Yii::app()->baseUrl . '/' . self::IMAGE_UPLOAD_PATH;
    }

    private function uploadItem($type, $attribute, $saveAsName = null)
    {
        if ($type == 'file')
        {
            $allowedFiles = array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'csv');
            $uploadDir = $this->getBaseDir() . self::FILE_UPLOAD_PATH;
            $maxSize = 500; //kbs
        }
        else
        {
            $allowedFiles = array('jpg', 'gif', 'png', 'jpeg');
            $uploadDir = $this->getBaseDir() . self::IMAGE_UPLOAD_PATH;
            $maxSize = 500; //kbs
        }

        if (empty($_FILES[$attribute]))
            return true; // Do nothing

        if (empty($saveAsName))
            $saveAsName = md5(date('YmdHis')); // Generate random filename

        if (!is_dir($uploadDir))
            mkdir($uploadDir);

        $file = $_FILES[$attribute];

        if (is_uploaded_file($file['tmp_name']))
        {
            $ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));

            if (in_array($ext, $allowedFiles))
            {
                //$dimensions = getimagesize($file['tmp_name']);
                $size = ($file['size']/1000); // Size in kbs

                //if (!empty($dimensions) && $dimensions[0] <= 500 && $dimensions[1] <= 500) // Set a max size of 500x500
                if ($size <= $maxSize) // If size is smaller than 500kb
                {
                    $saveAsName = $saveAsName . '.' . $ext;

                    move_uploaded_file($file['tmp_name'], $uploadDir . $saveAsName);

                    $this->filename = $saveAsName;
                    return true;
                }
                else
                {
                    $this->uploadError = 'Sorry your file is too large, it must be smaller than ' . $maxSize . 'kb';
                    return false;
                }
            }
            else
            {
                $this->uploadError = 'Sorry only the following file types are allowed: ' . implode(', ', $allowedFiles);
                return false;
            }
        }
        else
        {
            switch ($file['error'])
            {
                case 0: //no error; possible file attack!
                    $this->uploadError = "There was a problem with your upload.";
                    break;
                case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
                    $this->uploadError = "The file you are trying to upload must be smaller than " . ini_get("upload_max_filesize") . '.';
                    break;
                case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
                    $this->uploadError = "The file you are trying to upload is too big.";
                    break;
                case 3: //uploaded file was only partially uploaded
                    $this->uploadError = "The file you are trying upload was only partially uploaded.";
                    break;
                case 4: //no file was uploaded
                    $this->uploadError = "You must select a file for upload.";
                    break;
                default:
                    $this->uploadError = "There was a problem with your upload.";
                    break;
            }
            return false;
        }
    }

    private function getSubFolders()
    {
        $dir = $this->getBaseDir() . self::IMAGE_UPLOAD_PATH;
        $files = scandir($dir);
        $folders = array();

        foreach ($files as $file)
        {
            if ($file === '.' or $file === '..') continue;

            if (is_dir($dir . '/' . $file))
            {
                $folders[] = $file;
            }
        }

        return $folders;
    }

    private function scanFiles($folder = null)
    {
        $result = array();

        $dir = $this->getBaseDir() . self::IMAGE_UPLOAD_PATH . $folder;
        $files = scandir($dir);

        if (!empty($folder))
            $folder .= '/';

        foreach ($files as $file)
        {
            if ($file === '.' || $file === '..' || $file === '.DS_Store') continue;

            if (is_dir($dir . '/' . $file))
                continue; // Ignore folders for now - at some point we could add multi-folder functionality fairly easily

            if (is_file($dir . '/' . $file))
            {
                $result[] = array(
                    'thumb' => $this->getUploadsPath() . $folder . $file,
                    'image' => $this->getUploadsPath() . $folder . $file,
                    'title' => $file,
                    'folder' => (!empty($folder) ? substr(ucwords($folder),0,strlen($folder)-1) : 'Uploads'),
                );
            }
        }

        return $result;
    }

    ######----------------------------------------------------------------------------------------------------######
    #                                       PRIVATE METHODS END
    ######----------------------------------------------------------------------------------------------------######
}