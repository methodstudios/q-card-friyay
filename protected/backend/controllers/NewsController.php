<?php

class NewsController extends BackendController
{
    public function init()
    {
        $enabled = Yii::app()->webSettings->getValue(Settings::KEY_ENABLE_NEWS);
        if (!$enabled)
            throw new CHttpException(404, 'The page you are looking for does not exist.');

        parent::init();
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new News;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['News']))
        {
            $model->attributes=$_POST['News'];

            if ($model->validate())
                $this->saveItem($model);
        }

        $model->convertDates();

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['News']))
        {
            $this->deleteOldImages($model);
            $model->attributes=$_POST['News'];

            if ($model->validate())
                $this->saveItem($model);
        }

        $model->convertDates();

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    private function saveItem($model)
    {
        $model = $this->checkImages($model);
        if ($model->save())
            $this->redirect(array('view','id'=>$model->getPrimaryKey()));
        else
            throw new Exception('Unable to save item. Attributes: <pre>' . print_r($model->attributes,1) . '</pre><br>Errors: ' . CHtml::errorSummary($model));
    }

    public function actionDelete($id = null)
    {
        if ($id)
        {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        elseif (!empty($_POST['ids']))
        {
            foreach ($_POST['ids'] as $id)
                $this->loadModel($id)->delete();
        }
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria();

        if (isset($_GET['search']))
        {
            $search = trim($_GET['search']);
            $criteria->compare('Title', $search, true, 'OR');
            $criteria->compare('ArticlePreview', $search, true, 'OR');
            $criteria->compare('Article', $search, true, 'OR');
        }

        $dataProvider=new CActiveDataProvider('News', array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder' => 'Date DESC, Created DESC',
            ),
            'pagination'=>array(
                'pageSize'=>$this->pageLimit,
            ),
        ));

        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function loadModel($id)
    {
        $model=News::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS START
    #####---------------------------------------------------------------------------------------------------------------------#####
    private function deleteOldImages(News $model)
    {
        $this->sizes = News::$sizes;
        $this->parentDeleteOldImages($model, 'Image');
    }

    private function checkImages(News $model)
    {
        $this->sizes = News::$sizes;
        return $this->parentCheckImages($model, 'Image');
    }

    public function actionImageCrop()
    {
        $model = new News;
        echo $this->parentImageCrop($model);
    }
    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS END
    #####---------------------------------------------------------------------------------------------------------------------#####
}
