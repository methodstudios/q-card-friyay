<?php

class PagesController extends BackendController
{
    public function init()
    {
        $enabled = Yii::app()->webSettings->getValue(Settings::KEY_ENABLE_PAGES);
        if (!$enabled)
            throw new CHttpException(404, 'The page you are looking for does not exist.');

        parent::init();
    }

    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model=new Pages;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Pages']))
        {
            $model->attributes=$_POST['Pages'];

            if ($model->validate())
                $this->saveItem($model);
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Pages']))
        {
            $this->deleteOldImages($model);
            $model->attributes=$_POST['Pages'];

            if ($model->validate())
                $this->saveItem($model);
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    public function actionDelete($id = null)
    {
        if ($id)
        {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
        elseif (!empty($_POST['ids']))
        {
            foreach ($_POST['ids'] as $id)
                $this->loadModel($id)->delete();
        }
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria();

        if (isset($_GET['search']))
        {
            $search = trim($_GET['search']);
            $criteria->compare('Title', $search, true, 'OR');
            $criteria->compare('Content', $search, true, 'OR');
        }

        $dataProvider=new CActiveDataProvider('Pages', array(
            'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder' => 'Created DESC',
            ),
            'pagination'=>array(
                'pageSize'=>$this->pageLimit,
            ),
        ));

        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    private function saveItem($model)
    {
        $model = $this->checkImages($model);
        if ($model->save())
            $this->redirect(array('view','id'=>$model->getPrimaryKey()));
        else
            throw new Exception('Unable to save item. Attributes: <pre>' . print_r($model->attributes,1) . '</pre><br>Errors: ' . CHtml::errorSummary($model));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Pages the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Pages::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Pages $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='pages-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS START
    #####---------------------------------------------------------------------------------------------------------------------#####
    private function deleteOldImages(Pages $model)
    {
        $this->sizes = Pages::$sizes;
        $this->parentDeleteOldImages($model, 'Image');
    }

    private function checkImages(Pages $model)
    {
        $this->sizes = Pages::$sizes;
        return $this->parentCheckImages($model, 'Image');
    }

    public function actionImageCrop()
    {
        $model = new Pages;
        echo $this->parentImageCrop($model);
    }
    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS END
    #####---------------------------------------------------------------------------------------------------------------------#####
}
