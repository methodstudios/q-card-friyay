<?php

class SiteController extends BackendController
{
    public $logoutError;

    public function actions()
    {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    public function actionIndex()
    {
        $this->redirect($this->getBaseUrl() . 'google');
        //$this->render('index');
    }

    public function actionError()
    {
        if ($error=Yii::app()->errorHandler->error)
        {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        $model=new LoginForm;
        $error = null;

        if (!empty(Yii::app()->session['ForceLogout']))
        {
            $this->logoutError = 'You have been logged out due to your account being used on another device.';
            unset(Yii::app()->session['ForceLogout']);
        }

        if (isset($_POST['LoginForm']))
        {
            try
            {
                LoginAttempts::validateLoginAttempt(self::getIpAddress());

                $model->attributes=$_POST['LoginForm'];

                if ($model->validate() && $model->login())
                {
                    $returnUrl = str_replace('/', '', Yii::app()->user->returnUrl);
                    $baseUrl = str_replace('/', '', Yii::app()->baseUrl);

                    // Little hack to make the return url redirect work on local
                    if (empty($returnUrl) || ((IS_DEVELOPMENT || IS_STAGING) && $returnUrl == $baseUrl))
                        $this->redirect($this->getBaseUrl() . 'index');

                    $this->redirect(Yii::app()->user->returnUrl);
                }
                else
                {
                    $this->addFailedAttempt($model);
                }
            }
            catch (Exception $e)
            {
                $error = $e->getMessage();
            }
        }

        $this->render('login',array(
            'model'=>$model,
            'error'=>$error,
        ));
    }

    private function addFailedAttempt(LoginForm $model)
    {
        if (!empty($model->Username))
        {
            $attempt = new LoginAttempts;
            $attempt->IpAddress = self::getIpAddress();
            $attempt->Email = $model->Username;
            if (!$attempt->save())
                throw new Exception('Unable to save login attempt.');
        }
    }

    public function actionLogout()
    {
        LoginForm::logout();
        $this->redirect($this->getBaseUrl() . 'login');
    }

    public function actionForgotPassword()
    {
        $this->layout = 'login';
        $model = new ForgotPasswordForm();

        if (!empty($_POST['ForgotPasswordForm']))
        {
            $model->attributes = $_POST['ForgotPasswordForm'];

            if ($model->validate())
            {
                $user = Users::model()->findByAttributes(array('Username' => $model->Username, 'Status' => Users::STATUS_ENABLED));

                if (!empty($user))
                {
                    $user->PasswordToken = $user->getPasswordToken();
                    $user->PasswordTokenExpiry = $user->getPasswordTokenExpiry();

                    if ($user->save())
                        $this->sendForgotPasswordEmail();
                }
                else
                {
                    Yii::app()->user->setFlash('error', 'Incorrect email address supplied please try again.');
                }
            }
            else
            {
                echo $model->errorSummary();
                die;
            }
        }

        $this->render('forgotPassword', array('model' => $model));
    }

    private function sendForgotPasswordEmail(Users $user)
    {
        $link = 'http://' . $_SERVER['HTTP_HOST'] . $this->getBaseUrl() . 'resetPassword?token=' . $user->PasswordToken;
        $message = '
        Please browse to the following link to reset your password: ' . $link . '<br><br>' .
        'Note, this link will expire in 24 hours and will have to be resent.';

        if ($this->sendEmail($user->Username, 'Admin - Password Reset', $message))
            Yii::app()->user->setFlash('success', 'An email with instructions has been sent to your email address.');
        else
            Yii::app()->user->setFlash('error', 'There was an error sending the email, please try again.');
    }

    public function actionResetPassword($token = null)
    {
        $this->layout = 'login';
        $displayForm = false;
        $model = new Users();

        try
        {
            if (!empty($token))
            {
                $user = Users::model()->findByAttributes(array(
                    'PasswordToken' => $token,
                    'Status' => Users::STATUS_ENABLED,
                ));
            }

            if (!empty($user))
            {
                if (strtotime($user->PasswordTokenExpiry) < time())
                    throw new Exception('Sorry this password token has expired.');
                else
                    $displayForm = true;
            }
            else
            {
                throw new Exception('The password token supplied is invalid.');
            }

            if (!empty($_POST['Users']))
            {
                $model = $user;
                $model->scenario = 'reset';
                $model->attributes = $_POST['Users'];

                if ($model->validate())
                {
                    // Reset the password expiry fields
                    $model->PasswordToken = null;
                    $model->PasswordTokenExpiry = null;
                    $model->Password = Users::passwordHash($model->Password);
                    $model->ConfirmPassword = Users::passwordHash($model->ConfirmPassword);

                    if ($model->save())
                    {
                        Yii::app()->user->setFlash('success', 'Password successfully updated! <a href="' . $this->getBaseUrl() . 'login">Click here</a> to login.');

                        // Empty these so they dont display in the form fields
                        $model->Password = null;
                        $model->ConfirmPassword = null;
                    }
                }
            }
        }
        catch (Exception $e)
        {
            $this->logoutError = $e->getMessage();
        }

        $this->render('resetPassword', array(
            'displayForm' => $displayForm,
            'model' => $model,
        ));
    }

    public function actionSettings()
    {
        if (!$this->isAdmin())
            $this->redirect('index');

        $errors = '';

        if (!empty($_POST['Settings']))
        {
            if (!empty($_FILES['Settings']['name']['Logo']))
            {
                $setting = Settings::model()->findByAttributes(array('Key' => 'BACKEND_LOGO'));
                $setting->Logo = CUploadedFile::getInstance($setting,'Logo');
                if ($setting->validate())
                {
                    $fileName = $this->slugify($_SERVER['SERVER_NAME']) . '-logo.' . $setting->Logo->getExtensionName();
                    $setting->Logo->saveAs($this->getBaseDir() . 'www/backend/img/' . $fileName);
                    $setting->Value = $fileName;
                    $setting->save();
                }
                else
                {
                    $errorsArr = $setting->getErrors();
                    foreach ($errorsArr as $error)
                        $errors .= $error[0] . '<br />';
                }
            }

            foreach ($_POST['Settings'] as $key => $value)
            {
                $setting = Settings::model()->findByAttributes(array('Key' => $key));
                if (empty($setting))
                    continue;

                $setting->Value = $value;
                if ($setting->validate())
                {
                    $setting->save();
                }
                else
                {
                    $errorsArr = $setting->getErrors();
                    foreach ($errorsArr as $error)
                        $errors .= $error[0] . '<br />';
                }
            }

            if (!empty($errors))
                Yii::app()->user->setFlash('error', $errors);
            else
                Yii::app()->user->setFlash('success', 'Settings successfully saved.');
        }

        $this->render('settings', array(
            'settings' => Settings::model()->findAll(),
        ));
    }

    private function runMigrationTool($key = null)
    {
        if ($this->isAdmin() || $key == Yii::app()->params['migrationKey'])
        {
            $commandPath = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'commands';
            $runner = new CConsoleCommandRunner();
            $runner->addCommands($commandPath);
            $commandPath = Yii::getFrameworkPath() . DIRECTORY_SEPARATOR . 'cli' . DIRECTORY_SEPARATOR . 'commands';
            $runner->addCommands($commandPath);
            $args = array('yiic', 'migrate', '--interactive=0');

            ob_start();
            $runner->run($args);
            return htmlentities(ob_get_clean(), null, Yii::app()->charset);
        }
    }

    public function actionMigrate()
    {
        $result = $this->runMigrationTool();
        $this->render('migrate', array('result' => $result));
    }

    public function actionProcessMigrations($key)
    {
        if ($key == Yii::app()->params['migrationKey'])
        {
            $result = $this->runMigrationTool($key);
            echo nl2br($result);
        }
        else
        {
            throw new CHttpException(404, 'The page you are looking for does not exist.');
        }
    }

}