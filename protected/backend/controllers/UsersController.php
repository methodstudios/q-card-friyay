<?php

class UsersController extends BackendController
{
        // Only allow superadmins to access this page
        public function accessRules()
        {
            return array(
                array('allow', // allow admin user to perform 'admin' and 'delete' actions
                        'expression'=>'Yii::app()->controller->isAdmin()',
                ),
                array('allow',  // allow users to update themselves
                    'actions'=>array('update'),
                    'users'=>array('*'),
                ),
                array('deny',  // deny all unauthenticated users
                    'users'=>array('*'),
                ),
            );
        }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $model=new Users;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Users']))
            {
                $model->attributes=$_POST['Users'];
                if ($model->validate())
                {
                    if($model->save())
                    {
                        $this->redirect(array('index'));
                    }
                }
            }

            $this->render('create',array(
                    'model'=>$model,
            ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
            // If user is not admin - only allow them to edit their own account
            if (!$this->isAdmin() && $id != Yii::app()->user->UserId)
            {
                throw new CHttpException(403,'You are not authorized to perform this action.');
            }

            $model=$this->loadModel($id);

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Users']))
            {
                if (!empty($_POST['Users']['Password']))
                {
                    $model->scenario = 'insert';
                }
                else
                {
                    unset($_POST['Users']['Password']);
                    unset($_POST['Users']['ConfirmPassword']);
                }

                $model->attributes=$_POST['Users'];

                if ($model->validate())
                {
                    if($model->save())
                    {
                        $this->redirect(array('index'));
                    }
                }
            }

            $this->render('update',array(
                    'model'=>$model,
            ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id = null)
	{
            if ($id)
            {
                $this->loadModel($id)->delete();

                // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                if(!isset($_GET['ajax']))
                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
            elseif (!empty($_POST['ids']))
            {
                foreach ($_POST['ids'] as $id)
                {
                    $this->loadModel($id)->delete();
                }
            }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            $criteria = new CDbCriteria();

            if (isset($_GET['search']))
            {
                $search = $_GET['search'];
                $criteria->compare('Username', $search, true, 'OR');
                //$criteria->compare('attribute2', $search, true, 'OR');
            }

            $dataProvider=new CActiveDataProvider('Users', array(
                'criteria'=>$criteria,
                'sort'=>array(
                    'defaultOrder' => 'Created DESC',
                ),
            ));

            $this->render('index',array(
                'dataProvider'=>$dataProvider,
            ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
            $model=Users::model()->findByPk($id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
            if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
            {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
	}
}
