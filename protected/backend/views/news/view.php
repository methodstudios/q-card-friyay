<?php
/* @var $this NewsController */
/* @var $model News */

$this->menu=array(
	array('label'=>'All News Items', 'url'=>array('index')),
	array('label'=>'Create News Item', 'url'=>array('create')),
        array('label'=>'View News Item', 'url'=>array('view', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
	array('label'=>'Update News Item', 'url'=>array('update', 'id'=>$model->getPrimaryKey())),
	array('label'=>'Delete News Item', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1><?php echo $model->Title; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Title',
		'Slug',
		array(
                    'label' => 'Date',
                    'type' => 'raw',
                    'value' => date('d-M-Y', strtotime($model->Date)),
                ),
                array(
                    'label' => 'Image',
                    'type' => 'raw',
                    'value' => '<img src="' . News::getUploadsPath() . $model->Image . '" alt="" />',
                ),
		array(
                    'label' => 'Article Preview',
                    'value' => $model->ArticlePreview . '...',
                ),
		array(
                    'label' => 'Article',
                    'type' => 'raw',
                    'value' => $model->Article,
                ),
		'Status',
		array(
                    'label' => 'Modified',
                    'type' => 'raw',
                    'value' => (!empty($model->Modified) ? date('d-M-Y', strtotime($model->Modified)) : ''),
                ),
		array(
                    'label' => 'Created',
                    'type' => 'raw',
                    'value' => date('d-M-Y', strtotime($model->Created)),
                ),
	),
)); ?>
