<?php
/* @var $this NewsController */
/* @var $model News */

$this->menu=array(
	array('label'=>'All News Items', 'url'=>array('index')),
	array('label'=>'Create News Item', 'url'=>array('create'), 'itemOptions' => array('class' => 'active')),
);
?>

<h1>Create News Item</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>