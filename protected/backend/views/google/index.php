    <div class="googleLoader"><img src="<?php echo $this->getImagesPath(); ?>ajax-loader.gif" alt="loading..." /></div>

    <div class="googleContainer">
        <div class="left">
            <h1 style="margin-top:8px;">Website Statistics <span style="text-transform:lowercase;"><br><span class="profileName"><!-- DYNAMICALLY LOADED --></span></span></h1>
        </div>

        <?php
        if (empty($_GET['filter']))
            $selected = 'month';
        else
            $selected = $_GET['filter'];
        ?>

        <div class="right graphControls">
            <div class="graphBtn <?php echo ($selected == 'week' ? 'selected' : ''); ?>"><a href="<?php echo $this->getBaseUrl(); ?>google/index?filter=week">WEEK</a></div>
            <div class="graphBtn <?php echo ($selected == 'month' ? 'selected' : ''); ?>"><a href="<?php echo $this->getBaseUrl(); ?>google/index?filter=month">MONTH</a></div>
            <div class="graphBtn <?php echo ($selected == '3month' ? 'selected' : ''); ?>"><a href="<?php echo $this->getBaseUrl(); ?>google/index?filter=3month">3 MONTHS</a></div>
        </div>

        <div class="clear"></div>

        <hr />

        <div id="chart-div"></div>

        <div class="analyticsStats">

            <div class="statsPanel">
                <p>
                    <span class="totalViews"><!-- DYNAMICALLY LOADED --></span><br>
                    <span class="subTitle">Total Visits</span>
                </p>
            </div>
            <div class="statsPanel grey">
                <p>
                    <span class="pagesSession"><!-- DYNAMICALLY LOADED --></span><br>
                    <span class="subTitle">Pages/Session <span class="info-icon" id="pageSessionToolTip">i</span></span>
                </p>
            </div>
            <div class="statsPanel grey">
                <p>
                    <span class="avgDuration"><!-- DYNAMICALLY LOADED --></span><br>
                    <span class="subTitle">Avg. Session Duration <span class="info-icon" id="avgSessionToolTip">i</span></span>
                </p>
            </div>

        </div>

        <div class="clear"></div>
    </div>

</div><!-- This tag closes the .mainContentPanel div so we can have a gap between divs -->

    <div class="contentPanel" style="background-color:inherit; padding:0;">

        <div class="dashboardPanelLeft">
            <div class="googleLoader"><img src="<?php echo $this->getImagesPath(); ?>ajax-loader.gif" alt="loading..." /></div>
            <div class="googleContainer">
                <div class="activeUsers">
                    <h1>Bounce Rate <span class="info-icon" id="bounceToolTip">i</span></h1>
                    <div class="bounceRateWrapper">
                        <!-- DYNAMICALLY LOADED -->
                    </div>
                </div>

                <div class="activeInfo left" style="display:none;">
                    <div class="row">
                        <p class="greyCopy">New Visitors</p>
                        <div class="left clear blueCopy"><span class="sessionsNew"><!-- DYNAMICALLY LOADED --></span>%</div>
                        <div class="left activeGraph">
                            <div class="activeGraphInner sessionsNewGraphInner"></div>
                        </div>
                    </div>

                    <div class="row">
                        <p class="greyCopy">Returning Visitors</p>
                        <div class="left clear blueCopy"><span class="sessionsReturning"><!-- DYNAMICALLY LOADED --></span>%</div>
                        <div class="left activeGraph">
                            <div class="activeGraphInner sessionsReturningGraphInner"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="dashboardPanelRight">
            <div class="googleLoader"><img src="<?php echo $this->getImagesPath(); ?>ajax-loader.gif" alt="loading..." /></div>
            <div class="googleContainer">
                <div class="trafficSource">
                    <h1>Traffic Source</h1>

                    <div class="trafficGraphs">
                        <!-- DYNAMICALLY LOADED -->
                    </div>
                </div>

                <div class="trafficReferrals left">
                    <h1>Top Referrals</h1>

                    <ol class="trafficReferralsInner" style="display:none;">
                        <!-- DYNAMICALLY LOADED -->
                    </ol>
                </div>

                <div class="topLocations left">
                    <h1>Top Locations</h1>

                    <ol class="topLocationsInner" style="display:none;">
                        <!-- DYNAMICALLY LOADED -->
                    </ol>
                </div>
            </div>
        </div>

    </div>

<script>
$(document).ready(function(){
    getDashboardData();
});

function getDashboardData()
{
    $('.googleContainer').hide();
    $('.googleLoader').fadeIn();

    $.ajax({
        url: '<?php echo $this->getBaseUrl(); ?>google/getData',
        method: 'get',
        data: '<?php echo (!empty($this->filter) ? 'filter=' . $this->filter : ''); ?>',
        success: function(jsonData){
            var data = JSON.parse(jsonData);
            displayDashboard(data);
        }
    });
}

function displayDashboard(data)
{
    $('.profileName').html(data.profileName);
    $('.totalViews').html(Math.round(parseFloat(data.totalViews)));
    $('.pagesSession').html(parseFloat(data.pagesSession).toFixed(2));
    $('.avgDuration').html(data.avgDuration);

    if (data.bounceRate)
    {
        $('.bounceRateWrapper').html('<div class="circlifulItem bounceRate" data-fontsize="30" data-text="' + data.bounceRate + '%" data-dimension="110" data-width="15" data-percent="' + data.bounceRate + '" data-fgcolor="<?php echo Yii::app()->webSettings->getValue('MAIN_ADMIN_HEX'); ?>" data-bgcolor="#eee"></div>');
    }

    if (data.sessions)
    {
        $('.activeInfo').show();
        $('.sessionsNew').html(data.sessions.new);
        $('.sessionsNewGraphInner').css('width', data.sessions.new + '%');
        $('.sessionsReturning').html(data.sessions.returning);
        $('.sessionsReturningGraphInner').css('width', data.sessions.returning + '%');
    }

    if (data.sourceMediums)
    {
        var html = '';
        $.each(data.sourceMediums, function(type, value){
            if (value)
            {
                var title = '';
                if (type === '<?php echo GoogleController::REF_DIRECT; ?>')
                    title = '<?php echo $this->referralsArr[GoogleController::REF_DIRECT]; ?>';
                else if (type === '<?php echo GoogleController::REF_ORGANIC; ?>')
                    title = '<?php echo $this->referralsArr[GoogleController::REF_ORGANIC]; ?>';
                else if (type === '<?php echo GoogleController::REF_REFERRAL; ?>')
                    title = '<?php echo $this->referralsArr[GoogleController::REF_REFERRAL]; ?>';

                html += '\
                <div class="row">\
                    <div class="circlifulItem" data-dimension="35" data-width="5" data-percent="' + value + '" data-fgcolor="<?php echo Yii::app()->webSettings->getValue('MAIN_ADMIN_HEX'); ?>" data-bgcolor="#eee"></div>\
                    <span class="blueCopy trafficValue">' + Math.round(parseFloat(value)) + '%</span>\
                    <span class="greyCopy trafficTitle">' + title + '</span>\
                </div>';
            }
        });
        $('.trafficGraphs').html(html);
    }

    if (data.referralData)
    {
        var html = '';
        $.each(data.referralData, function(name, amount){
            html += '<li><span class="greyCopy">' + name + ' (' + amount + ')</span></li>';
        });
        $('.trafficReferralsInner').html(html);
        $('.trafficReferralsInner').show();
    }

    if (data.geoInfo)
    {
        var html = '';
        $.each(data.geoInfo, function(location, amount){
            html += '<li><span class="greyCopy">' + location + ' (' + amount + ')</span></li>';
        });
        $('.topLocationsInner').html(html);
        $('.topLocationsInner').show();
    }

    if (!$('html').hasClass('lte-ie8'))
    {
        $('.circlifulItem').circliful();
    }

    $('.googleLoader').hide();
    $('.googleContainer').fadeIn(250);

    drawChart('line', data.chartData, '', 'No. of Views', '', '400');
}
</script>

