<div class="left">
    <h1>Login</h1>

    <?php if (!empty($error)): ?>
        <p><?php echo $error; ?></p>
        <p><a href="<?php echo $this->getBaseUrl() ?>">Back</a></p>
    <?php else: ?>

        <p>Please fill out the following form with your login credentials:</p>

        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                //'enableClientValidation'=>true,
                'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                ),
        )); ?>

                <div class="row">
                        <?php echo $form->labelEx($model,'Username'); ?>
                        <?php echo $form->textField($model,'Username'); ?>
                        <?php echo $form->error($model,'Username'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'Password'); ?>
                        <?php echo $form->passwordField($model,'Password'); ?>
                        <?php echo $form->error($model,'Password'); ?>
                </div>

                <div class="row rememberMe">
                        <?php echo $form->checkBox($model,'RememberMe'); ?>
                        <?php echo $form->label($model,'RememberMe'); ?>
                </div>

                <div class="row rowFixedBottom">
                    <div class="forgotPassLink">
                        <a href="<?php echo $this->getBaseUrl(); ?>forgotPassword">Forgot your password?</a>
                    </div>

                    <?php echo CHtml::submitButton('Login'); ?>
                </div>

        <?php $this->endWidget(); ?>
        </div><!-- form -->

    <?php endif; ?>

    <div class="clear"></div>

</div>
