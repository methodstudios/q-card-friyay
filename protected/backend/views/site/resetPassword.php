<h1>Reset Password</h1>

<?php
if (!$displayForm)
{
    echo '<p><a href="' . $this->getBaseUrl() . 'login">Back to login</a></p>';
}
else
{
?>

    <div class="left">

        <p>Please enter your new password below:</p>

        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'reset-pass-form',
                'enableClientValidation'=>false,
                'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                ),
        )); ?>

                <?php
                $error = $form->error($model,'Password');
                if (!empty($error))
                {
                    echo '<div class="flash flash-error">' . $error . '</div>';
                }
                ?>

                <div class="row">
                        <?php echo $form->labelEx($model,'Password'); ?>
                        <?php echo $form->passwordField($model,'Password',array('value' => '')); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'ConfirmPassword'); ?>
                        <?php echo $form->passwordField($model,'ConfirmPassword',array('value' => '')); ?>
                </div>

                <div class="row">
                    <div class="forgotPassLink">
                        <a href="<?php echo $this->getBaseUrl(); ?>login">Back to login</a>
                    </div>

                    <?php echo CHtml::submitButton('Submit'); ?>
                </div>

        <?php $this->endWidget(); ?>
        </div><!-- form -->

        <div class="clear"></div>

    </div>

<?php
}
?>