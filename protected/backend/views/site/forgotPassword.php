<div class="left">
    <h1>Forgot Password</h1>

    <p>Please enter your email address below, and we will send you instructions on resetting your password:</p>

    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'forgot-form',
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                    'validateOnSubmit'=>true,
            ),
    )); ?>

            <div class="row">
                    <?php echo $form->labelEx($model,'Username'); ?>
                    <?php echo $form->textField($model,'Username'); ?>
                    <?php echo $form->error($model,'Username'); ?>
            </div>

            <div class="row">
                <div class="forgotPassLink">
                    <a href="<?php echo $this->getBaseUrl(); ?>login">Back to login</a>
                </div>

                <?php echo CHtml::submitButton('Submit'); ?>
            </div>

    <?php $this->endWidget(); ?>
    </div><!-- form -->

    <div class="clear"></div>

</div>