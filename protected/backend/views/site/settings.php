<h1>Site Settings</h1>

<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="flash flash-' . $key . '">' . $message . "</div>\n";
    }
?>

<div class="form settings-container">
    <form action="" method="post" enctype="multipart/form-data">

        <div class="row">
            <label>Admin Logo</label>
            <input type="file" name="Settings[Logo]" size="60" />
        </div>

        <?php foreach ($settings as $setting): ?>

            <?php if (empty($setting->Visible)) continue; ?>

            <div class="row">
                <label><?php echo $setting->Name; ?></label>
                <?php
                if ($setting->Type == Settings::TYPE_CHECKBOX)
                {
                    echo '
                    <select name="Settings[' . $setting->Key . ']">
                        <option value="1" ' . ((!empty($setting->Value) || !empty($_POST['Settings'][$setting->Key])) ? 'selected' : '') . '>Yes</option>
                        <option value="0" ' . ((empty($setting->Value) && empty($_POST['Settings'][$setting->Key])) ? 'selected' : '') . '>No</option>
                    </select>';
                }
                else
                {
                    echo '<input type="text" name="Settings[' . $setting->Key . ']" value="' . (isset($_POST['Settings'][$setting->Key]) ? $_POST['Settings'][$setting->Key] : $setting->Value) . '" size="60" />';
                }
                ?>
            </div>

        <?php endforeach; ?>

        <div class="row">
            <input type="submit" value="Save All" />
        </div>
    </form>
</div><!-- form -->