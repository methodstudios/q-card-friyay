<?php echo $this->getRedactorScript('#People_Content'); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'people-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php echo $this->getStatusToggle($model, $form); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Name'); ?>
		<?php echo $form->textField($model,'Name',array('size'=>30,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Title'); ?>
		<?php echo $form->textField($model,'Title',array('size'=>30,'maxlength'=>80)); ?>
	</div>

        <div class="row">
                <?php echo $form->labelEx($model,'Image'); ?>
                <?php echo $form->hiddenField($model, 'Image'); ?>
                <button type="button" class="show-all-lightboxes" data-id="0"><?php echo ($model->isNewRecord ? 'Add Images' : 'Change All Images'); ?></button>
        </div>

        <?php
        // Display list of images if updating record
        echo $this->getImagesList($model, 'Image');
        ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Content'); ?>
                <div class="left textarea-wrap">
                    <?php echo $form->textArea($model,'Content',array('rows'=>15, 'cols'=>60)); ?>
                </div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
// Initialise the cropper
$attribute = 'Image'; // Change attribute if required
echo $this->renderPartial('/includes/_cropInitialize', array(
    'attribute'=>$attribute,
    'model'=>$model,
));
?>