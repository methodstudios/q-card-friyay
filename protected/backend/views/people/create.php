<?php
/* @var $this PeopleController */
/* @var $model People */

$this->menu=array(
	array('label'=>'All People', 'url'=>array('index')),
	array('label'=>'Create Person', 'url'=>array('create'), 'itemOptions' => array('class' => 'active')),
);
?>

<h1>Create Person</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>