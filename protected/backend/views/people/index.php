<?php
/* @var $this PeopleController */
/* @var $dataProvider CActiveDataProvider */
$this->showSwitchTiles=true;
$this->showSearchForm=true;

$this->menu=array(
	array('label'=>'All People', 'url'=>array('index'), 'itemOptions' => array('class' => 'active')),
	array('label'=>'Create Person', 'url'=>array('create')),
);
?>

<h1>All People</h1>

<div class="clear"></div>

<div class="view-tiles" style="display:none;">
    <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'_view',
            'afterAjaxUpdate'=>'initiateIsotopeDelayed',
            'pager'=>array(
                'header'=>'',
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
            ),
    )); ?>
</div>

<div class="view-list people-list">

    <?php echo $this->getBulkActions(); ?>

    <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $dataProvider,
            //'filter' => $model,
            'columns' => array(
                array(
                    'type' => 'raw',
                    'value' => 'BackendController::getListCheckbox($data->getPrimaryKey())',
                ),
                array(
                    'name' => 'Image',
                    'type'=>'raw',
                    'value' => 'CHtml::image("' . People::getUploadsPath() . '" . $data->Image)',
                ),
                'Name',
                'Title',
                array(
                    'name' => 'Created',
                    'value' => 'date("d-M-Y", strtotime($data->Created))',
                ),
                array(
                    'name' => '',
                    'type' => 'raw',
                    'htmlOptions' => array('class' => 'actionLinks'),
                    'value' => '
                        CHtml::link("View", array("view","id"=>$data->getPrimaryKey())) . "&nbsp;&nbsp;|&nbsp;&nbsp;" .
                        CHtml::link("Edit", array("update","id"=>$data->getPrimaryKey())) . "&nbsp;&nbsp;|&nbsp;&nbsp;" .
                        CHtml::link("Delete", array("delete","id"=>$data->getPrimaryKey()), array("onclick" => "deleteItem(\"" . $data->getPrimaryKey() . "\", \"" . BackendController::getControllerUrl() . "\"); return false;"))',
                ),
            ),
            'pager'=>array(
                'header'=>'',
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
            ),
    )); ?>
</div>