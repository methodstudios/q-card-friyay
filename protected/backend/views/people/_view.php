<?php
/* @var $this PeopleController */
/* @var $data People */
?>

<div class="view" id="item-<?php echo $data->getPrimaryKey(); ?>" style="text-align: center;">

        <?php if (!empty($data->Image)): ?>
            <p style="text-align:center"><img src="<?php echo People::getUploadsPath() . $data->Image; ?>" alt="" /></p>
        <?php endif; ?>

        <p><b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('Title')); ?>:</b>
	<?php echo CHtml::encode($data->Title); ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('Created')); ?>:</b>
	<?php echo date("d-M-Y", strtotime($data->Created)); ?>
	</p>

        <p>
            <a href="<?php echo $this->createUrl('view', array('id' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-eye-open"></span> View
                </button>
            </a>
            <a href="<?php echo $this->createUrl('update', array('id' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> Edit
                </button>
            </a>
            <a href="#" onclick="deleteItem('<?php echo $data->getPrimaryKey(); ?>', '<?php echo $this->getControllerUrl() ?>'); return false;">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </a>
        </p>



</div>