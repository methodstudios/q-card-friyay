<?php
/* @var $this PeopleController */
/* @var $model People */

$this->menu=array(
	array('label'=>'All People', 'url'=>array('index')),
	array('label'=>'Create Person', 'url'=>array('create')),
	array('label'=>'View Person', 'url'=>array('view', 'id'=>$model->getPrimaryKey())),
        array('label'=>'Update Person', 'url'=>array('update', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
        array('label'=>'Delete Person', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>Update <?php echo $model->Name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>