<?php
/* @var $this PeopleController */
/* @var $model People */
$this->menu=array(
	array('label'=>'All People', 'url'=>array('index')),
	array('label'=>'Create Person', 'url'=>array('create')),
	array('label'=>'View Person', 'url'=>array('view', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
        array('label'=>'Update Person', 'url'=>array('update', 'id'=>$model->getPrimaryKey())),
        array('label'=>'Delete Person', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View <?php echo $model->Name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Name',
		'Title',
                array(
                    'label' => 'Image',
                    'type' => 'raw',
                    'value' => '<img src="' . People::getUploadsPath() . $model->Image . '" alt="" />',
                ),
                array(
                    'label' => 'Content',
                    'type' => 'raw',
                    'value' => nl2br($model->Content),
                ),
		'Status',
		array(
                    'label' => 'Modified',
                    'type' => 'raw',
                    'value' => (!empty($model->Modified) ? date('d-M-Y', strtotime($model->Modified)) : ''),
                ),
		array(
                    'label' => 'Created',
                    'type' => 'raw',
                    'value' => date('d-M-Y', strtotime($model->Created)),
                ),
	),
)); ?>
