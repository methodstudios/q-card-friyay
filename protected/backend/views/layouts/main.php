<?php $this->renderPartial('/includes/_head'); ?>

<body>

    <?php if ($this->isLogged()): ?>
        <div class="settingsOverlay" style="display:none;"></div>
        <div class="settingsWrap" style="display:none;">
            <div class="left clear" style="width:100%; height:60px;"></div>

            <h3>Settings</h3>
            <ul>
                <li><a href="<?php echo $this->getBaseUrl() . 'users/update?id=' . Yii::app()->user->UserId; ?>">Edit Account</a></li>
                <li><a href="<?php echo $this->getBaseUrl() . 'users'; ?>">Users</a></li>
                <?php if ($this->isAdmin()): ?>
                    <li><a href="<?php echo $this->getBaseUrl() . 'site/settings'; ?>">Site Settings</a></li>
                    <li><a href="<?php echo $this->getBaseUrl() . 'site/migrate'; ?>">Run Migrations</a></li>
                <?php endif; ?>
                <li><a href="<?php echo $this->getBaseUrl(); ?>logout">Logout</a></li>
            </ul>

        </div>
    <?php endif; ?>

    <div class="mainHeader">

        <?php if ($this->isLogged()): ?>
            <div class="left">
                <h2>Welcome back <?php echo Yii::app()->user->FirstName; ?></h2>
            </div>

            <div class="right">
                <div class="headerDate left"><?php echo date('l, d F'); ?></div>
                <div class="headerTime left"><?php echo date('h:i a'); ?></div>
                <div class="settingsBtn left"><a href="#" onclick="displaySettings();"></a></div>
            </div>

        <?php else: ?>
            <div class="left">
                <h2>Login</h2>
            </div>
        <?php endif; ?>

    </div>

    <div class="container" id="page">
        <div id="mainMenu">
            <div class="mainLogo">
                <img src="<?php echo $this->getImagesPath() . Yii::app()->webSettings->getValue('BACKEND_LOGO'); ?>" alt="Logo" />
            </div>

            <div class="mainMenuContainer">
                <?php $this->widget('zii.widgets.CMenu',array(
                    'items' => array(
                        array('label' => 'Dashboard', 'url' => array('/index'), 'visible' => $this->isLogged(), 'itemOptions' => $this->getActiveItem('site', 'index')),
                        //array('label' => 'Users', 'url' => array('/users'), 'visible' => $this->isAdmin(), 'itemOptions' => $this->getActiveItem('users')),
                        array('label' => 'News', 'url' => array('/news'), 'visible' => $this->isEnabled(Settings::KEY_ENABLE_NEWS), 'itemOptions' => $this->getActiveItem('news')),
                        array('label' => 'Pages', 'url' => array('/pages'), 'visible' => $this->isEnabled(Settings::KEY_ENABLE_PAGES), 'itemOptions' => $this->getActiveItem('pages')),
                        array('label' => 'People', 'url' => array('/people'), 'visible' => $this->isEnabled(Settings::KEY_ENABLE_PEOPLE), 'itemOptions' => $this->getActiveItem('people')),
                    ),
                )); ?>
            </div>

            <div class="menuFooter"><a href="http://method.kiwi" target="_blank">Grid CMS 3.0 by Method Studios</a></div>
        </div>

        <?php if (!empty($this->menu)): ?>
            <div class="subMenu">
                <h2><?php echo ucwords(strtolower(Yii::app()->controller->id)); ?></h2>
                <?php $this->widget('zii.widgets.CMenu',array('items' => $this->menu)); ?>
            </div>

            <style type="text/css">
                .contentPanel { margin-left:435px; } /* Add extra margin for the sub-menu since its absolute */
            </style>
        <?php endif; ?>

        <?php
        $homeItem = $this->getActiveItem('site', 'index');
        $isDashboard = (!empty($homeItem) ? true : false);
        ?>

        <div class="rightContent <?php echo ($isDashboard ? 'dashboardWrap' : ''); ?>">
            <div class="mainContentPanel contentPanel">
                <?php
                if ($this->showSearchForm)
                {
                    echo $this->getSearchForm();
                }

                if ($this->showSwitchTiles)
                {
                    echo $this->getSwitchTiles();
                }

                echo $content;
                ?>
                <div class="clear"></div>
            </div>

            <div class="footerSpacer"></div>
        </div>

    </div><!-- page -->

    <script type="text/javascript">
        <?php
        echo 'var statusEnabled = "' . BaseModel::STATUS_ENABLED . '";';
        echo "\n";
        echo 'var statusDisabled = "' . BaseModel::STATUS_DISABLED . '";';
        ?>
    </script>
    <script src="<?php echo $this->getJsPath() ?>backend.js"></script>
</body>
</html>
