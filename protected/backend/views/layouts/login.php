<?php $this->renderPartial('/includes/_head'); ?>

<body>

    <div class="loginContainer" id="page">
        <div class="middle">
            <div class="inner">

                <?php
                foreach (Yii::app()->user->getFlashes() as $key => $message)
                    echo '<div class="flash flash-' . $key . '">' . $message . "</div>\n";
                ?>

                <?php
                if (!empty($this->logoutError))
                    echo '<div class="flash flash-error">' . $this->logoutError . '</div>';
                ?>

                <div class="loginLogoContainer">
                    <div class="mainLogoOuter">
                        <div class="mainLogoMiddle">
                            <div class="mainLogoInner">
                                <img src="<?php echo $this->getImagesPath(); ?>grid_logo.png" alt="Logo" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="loginContentContainer">
                    <?php echo $content; ?>
                </div>

            </div>
        </div>
    </div><!-- page -->

    <script type="text/javascript">
        <?php
        echo 'var statusEnabled = "' . BaseModel::STATUS_ENABLED . '";';
        echo "\n";
        echo 'var statusDisabled = "' . BaseModel::STATUS_DISABLED . '";';
        ?>
    </script>
    <script src="<?php echo $this->getJsPath() ?>backend.js"></script>
</body>
</html>
