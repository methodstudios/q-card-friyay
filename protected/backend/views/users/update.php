<?php
/* @var $this UsersController */
/* @var $model Users */

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index'), 'visible' => $this->isAdmin()),
	array('label'=>'Create User', 'url'=>array('create'), 'visible' => $this->isAdmin()),
    	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
        array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?'), 'visible' => $this->isAdmin()),
);
?>

<h1>Update User</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>