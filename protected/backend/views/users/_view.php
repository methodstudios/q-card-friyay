<?php
/* @var $this UsersController */
/* @var $data Users */
?>

<div class="view">

        <p><b><?php echo CHtml::encode($data->getAttributeLabel('Username')); ?>:</b>
	<?php echo CHtml::encode($data->Username); ?>
	</p>

        <p><b><?php echo CHtml::encode($data->getAttributeLabel('FirstName')); ?>:</b>
	<?php echo CHtml::encode($data->FirstName); ?>
	</p>

        <p><b><?php echo CHtml::encode($data->getAttributeLabel('LastName')); ?>:</b>
	<?php echo CHtml::encode($data->LastName); ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('AdminLevel')); ?>:</b>
	<?php echo CHtml::encode($data->AdminLevel); ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('LastLogin')); ?>:</b>
	<?php echo (!empty($data->LastLogin) ? date('d-M-Y', strtotime($data->LastLogin)) : ''); ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('Created')); ?>:</b>
	<?php echo date('d-M-Y', strtotime($data->Created)); ?>
	</p>

        <p>
            <!--<a href="<?php echo $this->createUrl('view', array('id' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-eye-open"></span> View
                </button>
            </a>-->
            <a href="<?php echo $this->createUrl('update', array('id' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> Edit
                </button>
            </a>
            <a href="#" onclick="deleteItem('<?php echo $data->getPrimaryKey(); ?>', '<?php echo $this->getControllerUrl() ?>'); return false;">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </a>
        </p>

</div>