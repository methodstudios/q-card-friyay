<?php
/* @var $this UsersController */
/* @var $model Users */

$this->menu=array(
        array('label'=>'List Users', 'url'=>array('index')),
        array('label'=>'Create User', 'url'=>array('create'), 'itemOptions' => array('class' => 'active')),
);
?>

<h1>Create User</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>