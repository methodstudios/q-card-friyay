<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */
$this->showSwitchTiles=true;
$this->showSearchForm=true;

$this->menu=array(
    array('label'=>'All Users', 'url'=>array('index'), 'itemOptions' => array('class' => 'active')),
    array('label'=>'Create User', 'url'=>array('create')),
);
?>

<h1>Browse All Users</h1>

<div class="clear"></div>

<div class="view-tiles">
    <?php $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'afterAjaxUpdate'=>'initiateIsotope',
            'itemView'=>'_view',
            'pager'=>array(
                'header'=>'',
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
            ),
    )); ?>
</div>

<div class="view-list" style="display:none;">

    <?php echo $this->getBulkActions(); ?>

    <?php $this->widget('zii.widgets.grid.CGridView', array(
            'dataProvider' => $dataProvider,
            //'filter' => $model,
            'columns' => array(
                array(
                    'type' => 'raw',
                    'value' => 'BackendController::getListCheckbox($data->getPrimaryKey())',
                ),
                'Username',
                'FirstName',
                'LastName',
                'AdminLevel',
                'Status',
                'LastLogin',
                array(
                    'name' => 'Created',
                    'type' => 'raw',
                    'value' => 'date("d-M-Y", strtotime($data->Created))',
                ),
                array(
                    'name' => '',
                    'type' => 'raw',
                    'htmlOptions' => array('class' => 'actionLinks'),
                    'value' => '
                        //CHtml::link("View", array("view","id"=>$data->getPrimaryKey())) . "&nbsp;&nbsp;|&nbsp;&nbsp;" .
                        CHtml::link("Edit", array("update","id"=>$data->getPrimaryKey())) . "&nbsp;&nbsp;|&nbsp;&nbsp;" .
                        CHtml::link("Delete", array("delete","id"=>$data->getPrimaryKey()), array("onclick" => "deleteItem(\"" . $data->getPrimaryKey() . "\", \"" . BackendController::getControllerUrl() . "\"); return false;"))',
                ),
            ),
            'pager'=>array(
                'header'=>'',
                'prevPageLabel'=>'<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel'=>'<span class="glyphicon glyphicon-chevron-right"></span>',
            ),
    )); ?>
</div>