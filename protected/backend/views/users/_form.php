<?php
$adminOptions = array();
$adminOptions[Users::ADMIN_LVL_OTHER] = Users::ADMIN_LVL_OTHER;
if (Yii::app()->user->AdminLevel == Users::ADMIN_LVL_ADMIN || Yii::app()->user->AdminLevel == Users::ADMIN_LVL_SUPER)
{
    $adminOptions[Users::ADMIN_LVL_ADMIN] = Users::ADMIN_LVL_ADMIN;
}
if (Yii::app()->user->AdminLevel == Users::ADMIN_LVL_SUPER)
{
    $adminOptions[Users::ADMIN_LVL_SUPER] = Users::ADMIN_LVL_SUPER;
}
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php echo $this->getStatusToggle($model, $form); ?>

        <?php
        // EXAMPLE USAGE OF DATEPICKER
        /*$this->widget('zii.widgets.jui.CJuiDatePicker',array(
                    'attribute'=>'FromDate',
                    'model'=>$model,
                    // additional javascript options for the date picker plugin
                    'options'=>array(
                        'dateFormat'=>'dd-mm-yy',
                    ),
                    'htmlOptions'=>array(
                        //'style'=>'height:20px;'
                    ),
        ));*/ ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Username'); ?>
		<?php echo $form->textField($model,'Username',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<?php if ($model->isNewRecord || $model->UserId == Yii::app()->user->UserId): ?>
        <div class="row">
		<?php echo $form->labelEx($model,'Password'); ?>
		<?php echo $form->passwordField($model,'Password',array('size'=>60,'maxlength'=>255,'value'=>'')); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'ConfirmPassword'); ?>
		<?php echo $form->passwordField($model,'ConfirmPassword',array('size'=>60,'maxlength'=>255,'value'=>'')); ?>
	</div>
        <?php endif; ?>

	<div class="row">
		<?php echo $form->labelEx($model,'FirstName'); ?>
		<?php echo $form->textField($model,'FirstName',array('size'=>60,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'LastName'); ?>
		<?php echo $form->textField($model,'LastName',array('size'=>60,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AdminLevel'); ?>
		<?php echo $form->dropDownList($model,'AdminLevel',$adminOptions); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->