<?php
/* @var $this NewsController */
/* @var $model News */

$this->menu=array(
	array('label'=>'All Pages', 'url'=>array('index')),
	array('label'=>'Create Page', 'url'=>array('create')),
        array('label'=>'View Page', 'url'=>array('view', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
	array('label'=>'Update Page', 'url'=>array('update', 'id'=>$model->getPrimaryKey())),
	array('label'=>'Delete Page', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1><?php echo $model->Title; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Title',
		'Slug',
                'LeadCopy',
                array(
                    'label' => 'Image',
                    'type' => 'raw',
                    'value' => '<img src="' . Pages::getUploadsPath() . $model->Image . '" alt="" />',
                ),
		array(
                    'label' => 'Content',
                    'type' => 'raw',
                    'value' => $model->Content,
                ),
                'Status',
		array(
                    'label' => 'Modified',
                    'type' => 'raw',
                    'value' => (!empty($model->Modified) ? date('d-M-Y', strtotime($model->Modified)) : ''),
                ),
		array(
                    'label' => 'Created',
                    'type' => 'raw',
                    'value' => date('d-M-Y', strtotime($model->Created)),
                ),
	),
)); ?>
