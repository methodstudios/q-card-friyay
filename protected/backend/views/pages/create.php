<?php
/* @var $this NewsController */
/* @var $model News */

$this->menu=array(
	array('label'=>'All Pages', 'url'=>array('index')),
	array('label'=>'Create Page', 'url'=>array('create'), 'itemOptions' => array('class' => 'active')),
);
?>

<h1>Create Page</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>