<?php echo $this->getRedactorScript('#Pages_Content'); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php echo $this->getStatusToggle($model, $form); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Title'); ?>
		<?php echo $form->textField($model,'Title',array('size'=>60,'maxlength'=>100)); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'Slug'); ?>
		<?php echo $form->textField($model,'Slug',array('size'=>60,'maxlength'=>100)); ?>
	</div>

        <!-- IMAGE CROPPER STARTS -->
        <div class="row">
            <?php echo $form->labelEx($model,'Image'); ?>
            <?php echo $form->hiddenField($model, 'Image'); ?>
            <button type="button" class="show-all-lightboxes" data-id="0"><?php echo ($model->isNewRecord ? 'Add Image(s)' : 'Change All Images'); ?></button>
        </div>

        <?php
        // Display list of images if updating record
        echo $this->getImagesList($model, 'Image');
        ?>
        <!-- IMAGE CROPPER ENDS -->

	<div class="row">
		<?php echo $form->labelEx($model,'LeadCopy'); ?>
                <div class="left textarea-wrap">
                    <span class="small-label">Maximum 150 characters.</span>
                    <?php echo $form->textArea($model,'LeadCopy',array('rows'=>3, 'cols'=>60,'maxlength'=>150)); ?>
                </div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Content'); ?>
                <div class="left textarea-wrap">
                    <span class="small-label">Note: Do not copy text from Word - use Notepad or similar to copy text from.</span>
                    <?php echo $form->textArea($model,'Content',array('rows'=>20, 'cols'=>120)); ?>
                </div>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    <?php if ($model->isNewRecord): ?>
    $('#Pages_Title').keyup(function(){
        addSlug(this, '#Pages_Slug');
    });

    $('#Pages_Title').blur(function(){
        addSlug(this '#Pages_Slug');
    });
    <?php endif; ?>
</script>

<?php
// Initialise the cropper
$attribute = 'Image'; // Change attribute if required
echo $this->renderPartial('/includes/_cropInitialize', array(
    'attribute'=>$attribute,
    'model'=>$model,
));
?>