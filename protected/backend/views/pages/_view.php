<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="view" id="item-<?php echo $data->getPrimaryKey(); ?>">

        <?php if (!empty($data->Image)): ?>
            <p style="text-align:center"><img src="<?php echo Pages::getUploadsPath() . $data->Image; ?>" alt="" /></p>
        <?php endif; ?>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('Title')); ?>:</b>
	<?php echo $data->Title; ?>
	</p>

        <p><b>Url:</b>
        <?php echo $data->Slug; ?>
	</p>

	<p><b><?php echo CHtml::encode($data->getAttributeLabel('LeadCopy')); ?>:</b>
	<?php echo $data->LeadCopy; ?>
	</p>

        <p>
            <a href="<?php echo $this->createUrl('view', array('id' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-eye-open"></span> View
                </button>
            </a>
            <a href="<?php echo $this->createUrl('update', array('id' => $data->getPrimaryKey())) ?>">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-pencil"></span> Edit
                </button>
            </a>
            <a href="#" onclick="deleteItem('<?php echo $data->getPrimaryKey(); ?>', '<?php echo $this->getControllerUrl() ?>'); return false;">
                <button type="button" class="btn btn-default btn-xs">
                    <span class="glyphicon glyphicon-trash"></span> Delete
                </button>
            </a>
        </p>



</div>