<?php
/* @var $this NewsController */
/* @var $model News */

$this->menu=array(
	array('label'=>'All Pages', 'url'=>array('index')),
	array('label'=>'Create Page', 'url'=>array('create')),
        array('label'=>'View Page', 'url'=>array('view', 'id'=>$model->getPrimaryKey())),
	array('label'=>'Update Page', 'url'=>array('update', 'id'=>$model->getPrimaryKey()), 'itemOptions' => array('class' => 'active')),
	array('label'=>'Delete Page', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->getPrimaryKey()),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1><?php echo $model->Title; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>