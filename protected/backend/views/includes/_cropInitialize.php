<?php
$class = get_class($model);
?>
<script type="text/javascript">
var filename = '<?php echo (!empty($model->$attribute) ? str_replace('.jpg', '', $model->$attribute) : $this->generateString(20)); ?>';
var imageName = '<?php echo (!empty($model->$attribute) ? $model->$attribute : ''); ?>';
var hiddenImageFieldId = '#<?php echo $class; ?>_<?php echo $attribute; ?>';
</script>
<script src="<?php echo $this->getBasePath(); ?>imagecrop/initiateCropper.js"></script>

<?php
    foreach ($class::$sizes as $key => $size)
    {
        echo '<script type="text/javascript">
                var uploadPath = "' . $class::getUploadsPath($size['width'], $size['height']) . '";
              </script>';

        echo $this->renderPartial('/includes/_cropLightbox', array(
            'model' => $model,
            'id' => $key,
            'size' => $size,
            'lastRecord' => (count($class::$sizes) == $key+1),
        ));
    }
?>