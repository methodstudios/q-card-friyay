<?php
// Disable Yii's version of jQuery
Yii::app()->clientScript->scriptMap=array('jquery.min.js'=>false);
?>
<!DOCTYPE html>
<!--[if lte IE 7]> <html lang="en-us" class="no-js lte-ie7 oldie"> <![endif]-->
<!--[if lte IE 8]> <html lang="en-us" class="no-js lte-ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo $this->getBootstrapPath(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getBootstrapPath(); ?>css/bootstrap-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>main.php" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>jquery.circliful.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getCssPath(); ?>jquery.qtip.min.css" />

    <script type="text/javascript">
        var mainColor = '<?php echo Yii::app()->webSettings->getValue('MAIN_ADMIN_HEX'); ?>';
    </script>
    <script src="<?php echo $this->getJsPath() ?>jquery-1.11.0.min.js"></script>
    <script src="<?php echo $this->getBootstrapPath() ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $this->getBootstrapPath() ?>js/bootstrap-switch.min.js"></script>
    <script src="<?php echo $this->getJsPath(); ?>isotope.pkgd.min.js"></script>
    <script src="<?php echo $this->getJsPath(); ?>jquery.circliful.min.js"></script>
    <script src="<?php echo $this->getJsPath(); ?>jquery.qtip.min.js"></script>
    <script src="<?php echo $this->getJsPath(); ?>modernizr-2.6.1.min.js"></script>
    <script src="<?php echo $this->getJsPath(); ?>jquery.lightbox_me.js"></script>

    <!-- Redactor WYSIWYG -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getBasePath(); ?>redactor/redactor.css" />
    <script type="text/javascript">
        var pages = new Object();
        var news = new Object();
        <?php
        foreach (Pages::model()->findAll() as $page)
            echo 'pages["' . $page->Slug . '"] = "' . $page->Title . '"' . "\n";

        foreach (News::model()->findAll() as $news)
            echo 'news["' . $news->Slug . '"] = "' . $news->Title . '"' . "\n";
        ?>

    </script>
    <script src="<?php echo $this->getBasePath(); ?>redactor/plugins/internalpagelinks.js"></script>
    <script src="<?php echo $this->getBasePath(); ?>redactor/plugins/internalnewslinks.js"></script>
    <script src="<?php echo $this->getBasePath(); ?>redactor/redactor.js"></script>

    <!-- jQuery Image Cropper -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->getBasePath(); ?>imagecrop/crop.css" />
    <script src="<?php echo $this->getBasePath(); ?>imagecrop/crop.js"></script>

    <!-- Google JS API (graphs) -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="<?php echo $this->getJsPath(); ?>charts.js"></script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>