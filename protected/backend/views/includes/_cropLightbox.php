<?php
$styles = 'width:%spx; height:%spx;';
$cropWidth = $size['width']+80;
$cropHeight = $size['height']+80;

// If this is not the first record - show the back button
if ($id == 0):
?>
<style type="text/css">
    .backButton<?php echo $id; ?> { display:none; }
</style>
<?php endif; ?>

<div class="lightbox" id="lightbox-<?php echo $id; ?>" style="display:none;">

    <h2><?php echo ($model->isNewRecord ? 'Add Image' : 'Change Image'); ?> <span>(<?php echo $size['width'] . 'x' . $size['height'] ?>)</span></h2>
    <?php
    if (!empty($size['msg']))
        echo '<p class="flash flash-notice">' . $size['msg'] . '</p>';
    else
        echo '<hr />';
    ?>

    <div class="left clear">
        <div class="left imageCrop<?php echo $id; ?>" style="min-height:<?php echo $cropHeight+50; ?>px; min-width:<?php echo $cropWidth; ?>px;">
            <input type="file" class="uploadfile" id="uploadfile<?php echo $id; ?>"/>
            <div class="newupload newupload<?php echo $id; ?>">Click here to upload image</div>
            <div class="default<?php echo $id; ?>">
                <div class="cropMain" style="<?php echo sprintf($styles, $cropWidth, $cropHeight); ?>"></div>
                <div class="cropSlider"></div>
                <div class="cropButtonWrap">
                    <span style="font-size:11px;" class="cropInstructions">&nbsp;Click button when this image is ready to save</span>
                    <div class="clear"></div>
                    <button class="backButton<?php echo $id; ?>" type="button" onclick="pressBackButton('<?php echo $id; ?>');">Back</button>
                    <button class="cropButton<?php echo $id; ?>" type="button" onclick="pressCropButton(this, '<?php echo $id; ?>');"><?php echo ($lastRecord ? 'Finish' : 'Next'); ?></button>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    var ajaxUrl = '<?php echo BackendController::getControllerUrl(); ?>/imageCrop';

    initiateImageCropper('<?php echo $id; ?>', imageName, uploadPath, ajaxUrl, hiddenImageFieldId, <?php echo $cropWidth-80; ?>, <?php echo $cropHeight-80; ?>, filename);
</script>
