<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$backend=dirname(dirname(__FILE__));
$frontend=dirname($backend);
Yii::setPathOfAlias('backend', $backend);

return array(
        'basePath' => $frontend,

        'controllerPath' => $backend . '/controllers',
        'viewPath' => $backend . '/views',
        'runtimePath' => $backend . '/runtime',

	'name'=>'Administration',
        'timeZone' => 'Pacific/Auckland',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
                'application.models.*',
                'application.components.*',
                'application.helpers.*',
                'backend.models.*',
                'backend.components.*',
                'ext.YiiMailer.YiiMailer',
                'ext.ImageCrop.ImageCrop',
                'ext.YiiImage.Image',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		/*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'Enter Your Password Here',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		*/
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,
			'rules'=>array(
                            // Be sure to add new rules BEFORE the final _c rules as below
                            //'backend/news'=>'news',
                            //'backend/news/<_c>'=>'news/<_c>',
                            //'backend/news/<_c>/<_a>'=>'news/<_c>/<_a>',

                            'backend/uploadImage'=>'redactor/uploadImage',
                            'backend/listImages'=>'redactor/listImages',
                            'backend/uploadFile'=>'redactor/uploadFile',
                            'backend/index'=>'site/index',
                            'backend/login'=>'site/login',
                            'backend/logout'=>'site/logout',
                            'backend/forgotPassword'=>'site/forgotPassword',
                            'backend/resetPassword'=>'site/resetPassword',
                            'backend/processMigrations/<key:.*?>'=>'site/processMigrations',

                            'backend'=>'site/index',
                            'backend/<_c>'=>'<_c>',
                            'backend/<_c>/<_a>'=>'<_c>/<_a>',
			),
		),
                'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
                'googleApi' => array(
                    'class' => 'ext.GoogleApi.GoogleApi',
                    /*
                     * To find this ID you need to open your browser, access an Analytics report for this domain and extract the number from the URL.
                     * If you read the URL carefully, it will contain a string like a123w456p7890 – you need the number after p:
                     * https://www.google.com/analytics/web/?hl=en#report/visitors-overview/a32206541w75090385p77549402/ would mean the ID is 77549402
                     *
                     * NOTE: You also need to add the web services email address into the allowed emails list in your analytics account under:
                     * Admin->Property->User Management add the following email - "682491475404-f8de5clm9i7ak8brcbhp58g17mnn017a@developer.gserviceaccount.com"
                    */
                    'webPropertyId' => '3247583',
                ),
                'webSettings' => array(
                    'class'=>'WebSettings',
                ),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
            'hashSalt' => 'qwh3RH32t2dn7khne',
            'adminEmail'=>'josh@methodstudios.co.nz',
            'migrationKey'=>'6d9346c034c2afef6005af',
	),
);