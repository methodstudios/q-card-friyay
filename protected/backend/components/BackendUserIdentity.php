<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class BackendUserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            $user = Users::model()->findByAttributes(array(
                'Username' => $this->username, //Email
                'Password' => $this->password,
                'Status' => Users::STATUS_ENABLED,
            ));

            if (empty($user))
            {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
                //$this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else
            {
                $this->errorCode = self::ERROR_NONE;
                $this->setState('UserId', $user->UserId);
                $this->setState('AdminLevel', $user->AdminLevel);
                $this->setState('FirstName', $user->FirstName);
                $this->setState('LastName', $user->LastName);
            }

            return !$this->errorCode;
	}
}