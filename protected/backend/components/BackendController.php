<?php
/**
 * Custom frontend controller class - these functions will be globally available across the frontend by using $this->function();
 * All frontend controller classes for this application should extend from this base class.
 */
class BackendController extends Controller
{
    public $layout='//layouts/main';
    public $menu=array();
    public $breadcrumbs=array();
    public $baseFolder='www/backend';
    public $homeUrl='backend';
    public $showSearchForm=false;
    public $showSwitchTiles=false;
    public $sizes = array();
    public $pageLimit = 25;

    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to login and logout
                'actions'=>array('login','logout','forgotPassword', 'resetPassword', 'processMigrations', 'error'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated users to access all admin pages
                'users'=>array('@'),
            ),
            array('deny',  // deny all unauthenticated users
                'users'=>array('*'),
            ),
        );
        /*return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );*/
    }

    public function beforeAction($action)
    {
        $this->filterIps();
        $this->checkSession();

        return parent::beforeAction($action);
    }

    /*
     * If there are IP addresses stored in settings, then only allow those IPs to access the backend
    */
    private function filterIps()
    {
        if (IS_DEVELOPMENT)
            return true;

        $ipWhitelist = Yii::app()->webSettings->getValue('IP_WHITELIST');
        $ipAddress = self::getIpAddress();

        if (!empty($ipWhitelist))
            $ipFilter = explode(',', $ipWhitelist);

        if (!empty($ipFilter) && !in_array($ipAddress, $ipFilter))
            throw new CHttpException(404,'The specified page cannot be found. Ip address: ' . $ipAddress);
    }

    public static function getIpAddress()
    {
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            return $_SERVER['REMOTE_ADDR'];
    }

    /*
     * Check the session token matches the DB otherwise log the user out
     */
    private function checkSession()
    {
        $user = Users::model()->findByPk($this->getUserId());
        if (!empty($user) && Yii::app()->session['SessionToken'] != $user->SessionToken)
        {
            LoginForm::logout();
            Yii::app()->session->open();
            Yii::app()->session['ForceLogout'] = 1;
        }
    }

    public function getUserId()
    {
        if (!empty(Yii::app()->user->UserId))
            return Yii::app()->user->UserId;
        elseif (!empty(Yii::app()->session['UserId']))
            return Yii::app()->session['UserId'];

        return null;
    }

    public function getBasePath()
    {
        return Yii::app()->baseUrl . '/' . $this->baseFolder . '/';
    }

    public function getImagesPath()
    {
         return $this->getBasePath() . 'img/';
    }

    public function getCssPath()
    {
         return $this->getBasePath() . 'css/';
    }

    public function getJsPath()
    {
         return $this->getBasePath() . 'js/';
    }

    public function getBootstrapPath()
    {
        return $this->getBasePath() . 'bootstrap-3.1.1/';
    }

    public static function getBaseUrl()
    {
        return Yii::app()->baseUrl . '/backend/';
    }

    public static function getControllerUrl()
    {
        return self::getBaseUrl() . Yii::app()->controller->id;
    }

    public function getBaseDir()
    {
        return dirname(Yii::app()->request->scriptFile) . '/';
    }

    public function getUploadsDir()
    {
        return $this->getBaseDir() . 'www/uploads/';
    }

    public function getFrontendBasePath()
    {
        return Yii::app()->baseUrl . '/www/';
    }

    public function isEnabled($setting)
    {
        $enabled = Yii::app()->webSettings->getValue($setting);
        if (!empty($enabled))
            return true;

        return false;
    }

    public function isLogged()
    {
        if (!Yii::app()->user->isGuest)
        {
            return true;
        }

        return false;
    }

    public function isAdmin()
    {
        if (!Yii::app()->user->isGuest && Yii::app()->user->AdminLevel == Users::ADMIN_LVL_SUPER)
        {
            return true;
        }

        return false;
    }

    public function getActiveItem($controller, $method = null)
    {
        // If the controller and method match - then its active
        if (!empty($method) && strtolower($controller) == strtolower(Yii::app()->controller->id) && strtolower($method) == strtolower(Yii::app()->controller->action->id))
        {
           return array('class' => 'active');
        }
        // If only the controller matches and method is null - then its active
        elseif (empty($method) && strtolower($controller) == strtolower(Yii::app()->controller->id))
        {
            return array('class' => 'active');
        }
        // If its the google controller index - then force home to be active
        elseif (strtolower($controller) == 'site' && $method == 'index' && strtolower(Yii::app()->controller->id) == 'google')
        {
            return array('class' => 'active');
        }

        return array();
    }

    public function slugify($string)
    {
        $string = html_entity_decode($string);
        $string = str_replace('&', 'and', $string);
        $string = str_replace(array('/'), '-', $string);

        return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), $string));
    }

    public function getSwitchTiles($active = 'list')
    {
        return '
            <div class="view-icons right">
                <a href="#" id="view-btn-tiles" controller="' . strtolower(Yii::app()->controller->id) . '" onclick="switchView(this, \'tiles\'); return false;" title="View Tiles" ' . ($active == 'tiles' ? 'class="active"' : '') . '>
                    <span class="glyphicon glyphicon-th"></span>
                </a>
                <a href="#" id="view-btn-list" controller="' . strtolower(Yii::app()->controller->id) . '" onclick="switchView(this, \'list\'); return false;" title="View List"' . ($active == 'list' ? 'class="active"' : '') . '>
                    <span class="glyphicon glyphicon-th-list"></span>
                </a>
            </div>
        ';
    }

    public function getSearchForm()
    {
        return '
            <form action="" method="get" class="search-form">
                <input type="search" placeholder="Search" name="search" value="' . (isset($_GET['search']) ? CHtml::encode($_GET['search']) : '') . '" />
                <input type="image" src="' . $this->getImagesPath() . 'search-btn.png" />
            </form>
        ';
    }

    public function getBulkActions($url = null)
    {
        return '
            <div class="clear"></div>
            <div class="bulkActions">
                <p>Bulk Actions</p>
                <a href="#" class="actionsBtn"><span class="glyphicon glyphicon-chevron-down"></span></a>
                <div class="actionsList" style="display:none;">
                    <ul>
                        <li><a href="#" onclick="bulkDelete(\'' . ($url ? $url : $this->getControllerUrl()) . '\'); return false;">Delete Checked Items</a></li>
                    </ul>
                </div>
            </div>
        ';
    }

    public static function getListCheckbox($primaryKey = null)
    {
        return '
            <div class="list-checkbox" onclick="checkCheckbox(this);" primarykey="' . $primaryKey . '"></div>
        ';
    }

    public function getRedactorScript($selector)
    {
        $plugins = array();
        if (Yii::app()->webSettings->getValue(Settings::KEY_ENABLE_PAGES))
            $plugins[] = "'internalpagelinks'";
        if (Yii::app()->webSettings->getValue(Settings::KEY_ENABLE_NEWS))
            $plugins[] = "'internalnewslinks'";

        return "
            <script type=\"text/javascript\">
            $(function()
            {
                var buttons = ['html', '|', 'bold', 'italic', 'deleted', '|',
                    'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
                    'image', 'video', 'file', 'table', 'link', '|',
                    'fontcolor', 'backcolor', '|', 'alignment', '|', 'horizontalrule'];

                $('" . $selector . "').redactor({
                    buttons: buttons,
                    imageUpload: '" . $this->getBaseUrl() . "uploadImage/',
                    imageUploadErrorCallback: function(json)
                    {
                        alert(json.error);
                    },
                    imageGetJson: '" . $this->getBaseUrl() . "listImages/',
                    fileUpload: '" . $this->getBaseUrl() . "uploadFile/',
                    fileUploadErrorCallback: function(json)
                    {
                        alert(json.error);
                    },
                    minHeight: 400,
                    pastePlainText: true,
                    plugins: [" . implode(',', $plugins) . "]
                });
            });

            $(document).ready(function(){

                $('form').submit(function(e){

                    var div = document.createElement('div');
                    div.innerHTML = $('" . $selector . "').redactor('get');
                    var elements = div.childNodes;
                    $(elements).find('a').attr('class', 'underlined');

                    $('" . $selector . "').redactor('set', div.innerHTML);

                });

            });

            </script>
        ";
    }

    public function getStatusToggle($model, CActiveForm $form = null)
    {
        $html = '<div class="status-switch">';
            if ($form)
                $html .= $form->labelEx($model,'Status');

            $class = get_class($model);
            $checked = (!empty($model->Status) && $model->Status == $class::STATUS_ENABLED ? 'checked="checked"' : '');

            $html .= '<input type="hidden" name="' . $class . '[Status]" />';
            $html .= '<input type="checkbox" value="' . $class::STATUS_ENABLED . '" name="" ' . $checked . ' />';
        $html .= '</div>';

        return $html;
    }

    // Send a basic email - don't need anything fancy from the backend
    public function sendEmail($to, $subject, $message, $from = null, $replyTo = null)
    {
        if (empty($from))
            $from = Yii::app()->params['adminEmail'];

        $mail = new YiiMailer();
        $mail->setTo($to);
        $mail->setSubject($subject);
        $mail->setBody($message);
        $mail->setFrom($from);
        if (!empty($replyTo)) $mail->setReplyTo($replyTo);

        // Use Gmail SMTP for dev emails so they work in all cases
        if (IS_DEVELOPMENT)
        {
            $mail->IsSMTP();
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure= 'ssl';
            $mail->Username = "methodstudios@gmail.com";
            $mail->Password = "Method66";
            // Disable BCC on local
            $mail->MethodBcc = '';
            $mail->setBcc(array());
        }

        if ($mail->send())
            return true;

        return false;
    }

    public function generateString($length = 10)
    {
        $string = sha1(uniqid(mt_rand(), true));

        return substr($string, 0, $length);
    }

    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS START
    #####---------------------------------------------------------------------------------------------------------------------#####
    protected function parentDeleteOldImages($model, $fieldName)
    {
        $modelName = get_class($model);
        // If a new image has been uploaded
        if (!empty($model->$fieldName) && !empty($_POST[$modelName][$fieldName]) && $model->$fieldName != $_POST[$modelName][$fieldName])
        {
            // We need to unlink all old images so we dont have dupes
            foreach ($this->sizes as $size)
            {
                $imagesDir = $modelName::getUploadsDir($size['width'], $size['height']);

                if (is_dir($imagesDir) && file_exists($imagesDir . $model->$fieldName))
                    unlink($imagesDir . $model->$fieldName);
            }
        }

        return true;
    }

    protected function hasDashes($string)
    {
        if (strpos($string, '-') !== FALSE)
            return true;

        return false;
    }

    protected function parentCheckImages($model, $fieldName, $slugFieldName = 'Title')
    {
        $modelName = get_class($model);
        // If the filename has no dashes - we should rename it using a slug etc
        if (!empty($model->$fieldName) && !$this->hasDashes($model->$fieldName))
        {
            $filename = $this->parentGenerateFilename($model, 'jpg', $slugFieldName);
            foreach ($this->sizes as $size)
            {
                $modelName = get_class($model);
                $imagesDir = $modelName::getUploadsDir($size['width'], $size['height']);
                rename($imagesDir . $model->$fieldName, $imagesDir . $filename);
            }
            $model->$fieldName = $filename;
        }

        return $model;
    }

    protected function parentGenerateFilename($model, $extension = 'jpg', $fieldName = 'Title')
    {
        if (!empty($model->$fieldName))
            return $this->slugify($model->$fieldName) . '-' . $this->generateString() . '.' . $extension;
    }

    protected function parentCheckDirectories($imagesDir, $folder)
    {
        if (!is_dir($this->getBaseDir() . 'www/uploads'))
            mkdir($this->getBaseDir() . 'www/uploads');

        if (!is_dir($this->getBaseDir() . 'www/uploads/' . $folder))
            mkdir($this->getBaseDir() . 'www/uploads/' . $folder);

        if (!is_dir($imagesDir))
            mkdir($imagesDir);

        return true;
    }

    public function parentImageCrop($model)
    {
        $modelName = get_class($model);
        if (!empty($_POST['image']) && !empty($_POST['filename']) && !empty($_POST['width']) && !empty($_POST['height']))
        {
            $imagesDir = $modelName::getUploadsDir($_POST['width'], $_POST['height']);
            $subFolder = strtolower($modelName);

            $this->parentCheckDirectories($imagesDir, $subFolder);

            $imageCrop = new ImageCrop($imagesDir, $_POST['filename']);
            $result = $imageCrop->resizeImage($_POST['image']);

            $this->cleanupOldImages($result['filename'], $imagesDir);

            return json_encode($result);
        }
    }

    /*
     * This function deletes old cropped images that were croped but never saved and renamed for a record
     */
    private function cleanupOldImages($filename, $imagesDir)
    {
        if ($handle = opendir($imagesDir))
        {
            while (false !== ($file = readdir($handle)))
            {
                if ($file === '.' || $file === '..' || $file === $filename || $this->hasDashes($file))
                    continue;

                unlink($imagesDir . $file);
            }
            closedir($handle);
        }
    }

    /*
     * This function will list all images for a model so they can be edited individually using the image cropper
     */
    public function getImagesList($model, $attribute)
    {
        $html = '';
        $class = get_class($model);

        if (!$model->isNewRecord && !empty($model->$attribute))
        {
            $html .= '<div class="row">';
            foreach ($class::$sizes as $key => $size)
            {
                $uploadsDir = $class::getUploadsDir($size['width'], $size['height']);

                if (file_exists($uploadsDir . $model->$attribute))
                {
                    $html .= '
                    <div class="left update-thumb-tile show-lightbox" data-id="' . $key . '" data-change="1">
                        <div class="update-thumb-title">' . $size['width'] . ' x ' . $size['height'] . (!empty($size['msg']) ? '<br>' . $size['msg'] : '') . '</div>
                        <div class="update-thumb update-thumb-' . $key . '">
                          <img src="' . $class::getUploadsPath($size['width'], $size['height']) . $model->$attribute . '?' . time() . '" alt="" />
                        </div>
                        <div class="update-thumb-link">
                            <a href="#" class="">(change)</a>
                        </div>
                    </div>
                    ';
                }
            }
            $html .= '</div>';
        }

        return $html;
    }
    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS END
    #####---------------------------------------------------------------------------------------------------------------------#####
}