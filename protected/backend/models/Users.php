<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $UserId
 * @property string $Username
 * @property string $Password
 * @property string $FirstName
 * @property string $LastName
 * @property string $AdminLevel
 * @property string $Status
 * @property string $PasswordToken
 * @property string $PasswordTokenExpiry
 * @property string $LastLogin
 * @property string $SessionToken
 * @property integer $ModifiedBy
 * @property string $Created
 */
class Users extends BaseModel
{
    const STATUS_ENABLED = 'Enabled';
    const STATUS_DISABLED = 'Disabled';

    const ADMIN_LVL_SUPER = 'Super';
    const ADMIN_LVL_ADMIN = 'Admin';
    const ADMIN_LVL_OTHER = 'Other';

    public $ConfirmPassword;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Username, FirstName, LastName', 'required'),
            array('Password, ConfirmPassword', 'required', 'on'=>'insert,reset'),
            array('Password', 'compare', 'compareAttribute'=>'ConfirmPassword', 'on'=>'insert,reset'),
            array('Username', 'length', 'max'=>100),
            array('FirstName, LastName', 'length', 'max'=>30),
            array('Username', 'email'),
            array('Username', 'unique', 'on' => 'insert'),
            array('Password', 'length', 'max'=>255),
            array('SessionToken', 'length', 'max'=>12),
            array('Password', 'length', 'min'=>8),
            array('PasswordToken', 'length', 'max'=>60),
            array('AdminLevel', 'length', 'max'=>5),
            array('ModifiedBy', 'numerical', 'integerOnly'=>true),
            array('LastLogin', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('UserId, Username, Password, AdminLevel, Status, LastLogin, Created', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'UserId' => 'User',
            'Username' => 'Email',
            'Password' => 'Password',
            'FirstName' => 'First Name',
            'LastName' => 'Last Name',
            'AdminLevel' => 'Admin Level',
            'PasswordToken' => 'Password Token',
            'PasswordTokenExpiry' => 'Password Token Expiry',
            'LastLogin' => 'Last Login',
            'SessionToken' => 'Session Token',
            'ModifiedBy' => 'Modified By',
            'Created' => 'Created',
        );
    }

    public function beforeSave()
    {
        parent::beforeSave();

        if (isset($this->Password) && strlen($this->Password) < 60) // If its more than 60 its already been hashed)
        {
            $this->Password = self::passwordHash($this->Password);
        }

        return true;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('UserId',$this->UserId);
        $criteria->compare('Username',$this->Username,true);
        $criteria->compare('Password',$this->Password,true);
        $criteria->compare('FirstName',$this->FirstName,true);
        $criteria->compare('LastName',$this->LastName,true);
        $criteria->compare('AdminLevel',$this->AdminLevel,true);
        $criteria->compare('PasswordToken',$this->PasswordToken,true);
        $criteria->compare('PasswordTokenExpiry',$this->PasswordTokenExpiry,true);
        $criteria->compare('LastLogin',$this->LastLogin,true);
        $criteria->compare('Created',$this->Created,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function passwordHash($password)
    {
        return hash('sha256', Yii::app()->params['hashSalt'] . $password);
    }

    public function getPasswordToken()
    {
        $token = sha1(uniqid(mt_rand(), true));

        return substr($token, 0, 60);
    }

    public function getPasswordTokenExpiry()
    {
        return date('Y-m-d H:i:s', strtotime('+1 day', time()));
    }
}
