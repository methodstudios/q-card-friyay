<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $Username;
	public $Password;
	public $RememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
            return array(
                array('Username, Password', 'required'),
                array('Username', 'email'),
                array('RememberMe', 'boolean'),
                // password needs to be authenticated
                array('Password', 'authenticate'),
            );
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
            return array(
                'RememberMe'=>'Keep me logged in',
                'Username'=>'Email',
            );
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
            if (!$this->hasErrors())
            {
                $this->_identity=new BackendUserIdentity($this->Username, Users::passwordHash($this->Password));
                if (!$this->_identity->authenticate())
                {
                    $this->addError('Password','Incorrect username or password.');
                }
            }
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
            if($this->_identity===null)
            {
                $this->_identity=new BackendUserIdentity($this->Username, Users::passwordHash($this->Password));
                $this->_identity->authenticate();
            }

            if ($this->_identity->errorCode===BackendUserIdentity::ERROR_NONE)
            {
                $duration=$this->RememberMe ? 3600*24*30 : 86400; // 30 days or 24 hours
                Yii::app()->user->login($this->_identity,$duration);

                $sessionToken = generateString(12);

                $user = Users::model()->findByAttributes(array('Username' => $this->Username));
                $user->LastLogin = date('Y-m-d H:i:s', time());
                $user->SessionToken = $sessionToken;
                $user->save();

                // Add these variables to session so we can check if user is logged in on the frontend
                Yii::app()->session['LoggedIn'] = true;
                Yii::app()->session['UserId'] = $user->getPrimaryKey();

                // Add session token to ensure user is only logged into one place at once
                Yii::app()->session['SessionToken'] = $sessionToken;

                return true;
            }

            return false;
	}

        public static function logout()
        {
            Yii::app()->user->logout();
            unset(Yii::app()->session['LoggedIn']);
            unset(Yii::app()->session['UserId']);
        }
}
