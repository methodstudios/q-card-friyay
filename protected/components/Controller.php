<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

        public function init()
        {
            register_shutdown_function(array($this, 'onShutdownHandler'));
            earlyFatalErrorHandler(true); // Unregister early hanlder
        }

        public static function sendErrorEmail($subject, $message)
        {
            $message .= self::getGlobals();
            $message .= '<br><br>Mail key: ' . Yii::app()->params['mailKey'] . ' - Use this key to set up mail rules in your inbox.';

            $mail = new YiiMailer();
            $mail->setTo('errors@methodstudios.co.nz');
            $mail->setSubject($subject);
            $mail->setBody($message);
            $mail->setFrom(Yii::app()->params['adminEmail']);

            // Use Gmail SMTP for dev emails so they work in all cases (some emails do not get sent to @method emails for some reason)
            $mail->IsSMTP();
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure= 'ssl';
            $mail->Username = "methodstudios@gmail.com";
            $mail->Password = "method66";
            // Disable BCC on local
            $mail->MethodBcc = '';
            $mail->setBcc(array());

            if ($mail->send())
            {
                return true;
            }

            return false;
        }

        private static function getGlobals()
        {
            $globs = '';

            $allowed = array('_GET', '_POST', '_SERVER', '_SESSION', '_COOKIE');
            foreach ($GLOBALS as $key => $value)
            {
                if (in_array($key, $allowed) && !empty($value))
                {
                    $globs .= '$' . $key . ':<br>';
                    $globs .= '<pre>' . print_r($value,1) . '</pre>';
                    $globs .= '<br>';
                }
            }

            return $globs;
        }

        public function onShutdownHandler()
        {
            // 1. error_get_last() returns NULL if error handled via set_error_handler
            // 2. error_get_last() returns error even if error_reporting level less then error
            $error = error_get_last();

            // Fatal errors
            $errorsToHandle = E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING;

            if (!is_null($error) && ($error['type'] & $errorsToHandle))
            {
                // It's better to set errorAction = null to use system view "error.php" instead of
                // run another controller/action (less possibility of additional errors)
                Yii::app()->errorHandler->errorAction = null;

                $message = 'FATAL ERROR: ' . $error['message'];
                if (!empty($error['file'])) $message .= ' (' . $error['file'] . ' :' . $error['line']. ')';

                // Force log & flush as logs were already processed as the error is fatal
                Yii::log($message, CLogger::LEVEL_ERROR, 'php');
                Yii::getLogger()->flush(true);

                // Pass the error to Yii error handler (standard or a custom handler you may be using)
                Yii::app()->handleError($error['type'], 'Fatal error: ' . $error['message'], $error['file'], $error['line']);
            }
        }
}