<?php
/**
 * Custom frontend controller class - these functions will be globally available across the frontend by using $this->function();
 * All frontend controller classes for this application should extend from this base class.
 */
class FrontendController extends Controller
{
    public $layout='//layouts/main';
    public $menu=array();
    public $breadcrumbs=array();
    public $sourceFolder='src';
    public $baseFolder='www';
    public $pageClass='';
    public $pageDescription='';

    // Do not filter the following $_GET and $_POST fields
    protected $dontFilter = array('InPlaceValue');

    public function init()
    {
        $this->filterAllData();
    }

    public function filterAllData()
    {
        if (!empty($_POST))
            $_POST = $this->recursivelyFilterData($_POST);
        if (!empty($_GET))
            $_GET = $this->recursivelyFilterData($_GET);
    }

    /*
     * recursivelyFilterData
     * This function purifies all input using HtmlPurifier
     * It then filters the HTML using htmlspecial chars to prevent layout breakages with HTML
     */
    protected function recursivelyFilterData($data)
    {
        $filter = new CHtmlPurifier;
        $clean = $filter->purify($data);

        foreach ($clean as $key => $value)
        {
            if (is_array($value))
                $clean[$key] = $this->recursivelyFilterData($value);
            else
                $clean[$key] = $this->filterItem($key, $value);
        }

        return $clean;
    }

    private function filterItem($key, $value)
    {
        if (in_array($key, $this->dontFilter))
            return $value;
        else
            return htmlspecialchars($value);
    }

    public function actionAdmin()
    {
        // This method is to redirect to admin so we can use /admin or /backend links
        $this->redirect('backend');
    }

    public function actionError()
    {
        if ($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function getSourcePath()
    {
        return Yii::app()->baseUrl . '/' . $this->sourceFolder . '/';
    }

    public function getBasePath()
    {
        return Yii::app()->baseUrl . '/' . $this->baseFolder . '/';
    }

    public function getBaseDir()
    {
        return dirname(Yii::app()->request->scriptFile) . '/';
    }

    public function getImagesPath()
    {
         return $this->getBasePath() . 'images/';
    }

    public function getCssPath()
    {
        if (IS_DEVELOPMENT)
        {
            return $this->getSourcePath() . 'css/';
        }
        else
        {
            return $this->getBasePath() . 'stylesheets/';
        }
    }

    public function getJsPath()
    {
        if (IS_DEVELOPMENT)
        {
            return $this->getSourcePath() . 'js/';
        }
        else
        {
            return $this->getBasePath() . 'javascripts/';
        }
    }

    public function getFontsPath()
    {
        return $this->getBasePath() . 'fonts/';
    }

    public function getBaseUrl()
    {
        return Yii::app()->baseUrl . '/';;
    }

    public function slugify($string)
    {
        $string = html_entity_decode($string);
        $string = str_replace('&', 'and', $string);
        $string = str_replace(array('/'), '-', $string);

        return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), $string));
    }

    public function obfuscateEmail($email, $className = null)
    {
        $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
        $key = str_shuffle($character_set);
        $cipher_text = '';
        $id = 'e'.rand(1,999999999);
        for ($i=0;$i<strlen($email);$i+=1)
            $cipher_text.= $key[strpos($character_set,$email[$i])];
        $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";';
        $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
        $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\" class=\\"'. $className .'\\">"+d+"</a>"';
        $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")";
        $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';

        return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;
    }

    // Send a basic email - nothing fancy
    public function sendEmail($to, $subject, $message, $from = null, $replyTo = null, $cc = null)
    {
        if (empty($from))
            $from = Yii::app()->params['adminEmail'];

        $mail = new YiiMailer();
        $mail->setTo($to);
        $mail->setSubject($subject);
        $mail->setBody($message);
        $mail->setFrom($from);
        if (!empty($replyTo)) $mail->setReplyTo($replyTo);
        if (!empty($cc)) $mail->setCc($cc);

        // Use Gmail SMTP for dev emails so they work in all cases
        if (IS_DEVELOPMENT)
        {
            $mail->IsSMTP();
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure= 'ssl';
            $mail->Username = "methodstudios@gmail.com";
            $mail->Password = "method66";
            // Disable BCC on local
            $mail->MethodBcc = '';
            $mail->setBcc(array());
        }

        if ($mail->send())
        {
            return true;
        }

        return false;
    }
}
