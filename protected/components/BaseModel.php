<?php
class BaseModel extends CActiveRecord
{
    const STATUS_ENABLED = 'Enabled';
    const STATUS_DISABLED = 'Disabled';
    const STATUS_DELETED = 'Deleted';

    public $modelName;
    public $fieldName = 'Status';

    public function checkInArray($attribute, $params)
    {
        if (!array_key_exists($this->$attribute, $params['array']))
            $this->addError($attribute, 'Your selected ' . $this->getAttributeLabel($attribute) . ' is not supported.');
    }

    public function defaultScope()
    {
        $this->modelName = $this->getTableAlias(false,false);
        $condition = $this->checkStatus();

        return array(
            //'alias' => $this->modelName,
            'condition' => $condition,
        );
    }

    private function checkStatus()
    {
        if (!$this->hasAttribute($this->fieldName))
            return '';

        // These variables cause errors on cron jobs
        if (empty(Yii::app()->session) && empty(Yii::app()->user))
            return '';

        // By default - only show enabled items (for frontend)
        $condition = "`$this->modelName`.`$this->fieldName` != '" . self::STATUS_DISABLED . "' AND `$this->modelName`.`$this->fieldName` != '" . self::STATUS_DELETED . "'";

        // If user is in the backend show enabled and disabled items but NOT deleted
        if (!Yii::app()->user->isGuest && !empty(Yii::app()->user->UserId))
        {
            $condition = "`$this->modelName`.`$this->fieldName` != '" . self::STATUS_DELETED . "'";
        }

        /*
        Bypass defaultScope().
        At the start of code sections in your controller where you
        do not want to apply defaultScope, do this:
        Yii::app()->user->setState('skipDefaultScope', True);.

        REMEMBER to unset(Yii::app()->user->skipDefaultScope) at
        the end of these sections, AND before any 'return'
        statement in these sections, AND in
        siteController/actionError so that it is unset after
        unexpected exceptions.
        */
        if(isset(Yii::app()->user->skipDefaultScope))
        {
            if(Yii::app()->user->skipDefaultScope === TRUE)
            {   //Return all records
                $condition = "";
            }
        }

        return $condition;
    }

    public function cloneModel()
    {
        $primaryKeyAttribute = $this->tableSchema->primaryKey;
        $model = clone $this;
        unset($model->$primaryKeyAttribute);
        $model->isNewRecord = true;

        return $model;
    }

    public function beforeSave()
    {
        if ($this->hasAttribute('Modified') && !$this->isNewRecord)
            $this->Modified = new CDbExpression('NOW()');

        if ($this->hasAttribute('ModifiedBy') && !$this->isNewRecord)
        {
            if (!empty(Yii::app()->user->UserId))
                $this->ModifiedBy = Yii::app()->user->UserId;
            elseif (!empty(Yii::app()->session['UserId']))
                $this->ModifiedBy = Yii::app()->session['UserId'];
        }

        return parent::beforeSave();
    }

    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS START
    #####---------------------------------------------------------------------------------------------------------------------#####

    public static function getUploadsDir($width = null, $height = null)
    {
        $uploadsDir = dirname(Yii::app()->request->scriptFile) . self::getUploadDirectory();
        $uploadsDir .= self::getUploadFolder($width, $height);

        return $uploadsDir;
    }

    public static function getUploadsPath($width = null, $height = null)
    {
        $uploadsPath = Yii::app()->baseUrl . self::getUploadDirectory();
        $uploadsPath .= self::getUploadFolder($width, $height);

        return $uploadsPath;
    }

    private static function getUploadDirectory()
    {
        return '/www/uploads/' . strtolower(get_called_class()) . '/';
    }

    private static function getUploadFolder($width, $height)
    {
        $className = get_called_class();

        if (!empty($width) && !empty($height))
            $folder = self::getFolderFromSize($width, $height);
        elseif (!empty($className::$sizes))
            $folder = self::getFolderFromSize($className::$sizes[0]['width'], $className::$sizes[0]['height']);

        return $folder . '/';
    }

    private static function getFolderFromSize($width, $height)
    {
        return $width . 'x' . $height;
    }

    #####---------------------------------------------------------------------------------------------------------------------#####
    #                                                IMAGE CROPPER METHODS END
    #####---------------------------------------------------------------------------------------------------------------------#####
}