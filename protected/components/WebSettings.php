<?php
class WebSettings extends CApplicationComponent
{
    function getValue($key)
    {
        try
        {
            $model = Settings::model()->findByAttributes(array('Key' => $key));

            if (!empty($model))
            {
                return $model->Value;
            }
        }
        catch(CDbException $e)
        {
            // If there is no DB then just return false
            return false;
        }

        return false;
    }
}