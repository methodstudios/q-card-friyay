<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class FrontendUserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            $staff = Staff::model()->findByAttributes(array(
                'Email' => $this->username, //Email
                'Password' => self::passwordHash($this->password),
                'Status' => Staff::STATUS_ENABLED,
            ));

            if (empty($staff))
            {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
                //$this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else
            {
                $this->errorCode = self::ERROR_NONE;
                $this->setState('StaffId', $staff->StaffId);
                $this->setState('Email', $staff->Email);
                $this->setState('FirstName', $staff->FirstName);
                $this->setState('LastName', $staff->LastName);
                $this->setState('Company', $staff->Company);
            }

            return !$this->errorCode;
	}

        public static function passwordHash($password)
        {
            //Hack: Need the frontend salt for Staff passwords so make sure we load the frontend config
            $config = include(dirname(dirname(__FILE__)) . '/config/main.php');

            return hash('sha256', $config['params']['hashSalt'] . $password);
        }

        public static function getPasswordToken()
        {
            $token = sha1(uniqid(mt_rand(), true));

            return substr($token, 0, 60);
        }

        public static function getPasswordTokenExpiry()
        {
            return date('Y-m-d H:i:s', strtotime('+1 day', time()));
        }
}