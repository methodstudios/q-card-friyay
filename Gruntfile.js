module.exports = function(grunt) {
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    src: 'src/',
    assets: 'www/',

    coffee: {
      application: {
        files: {
          '<%= src %>js/application.js': ['<%= src %>coffee/*.coffee']
        }
      }
    },

    sass: {
      dynamic: {
        files: [{
          expand: true,
          cwd: '<%= src %>sass',
          src: ['*.sass'],
          dest: '<%= src %>css',
          ext: '.css'
        }]
      }
    },

    coffeelint: {
      options: {
        configFile: 'coffeelint.json'
      },
      files: '<%= src %>coffee/*.coffee'
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 versions', 'ie 8', 'ie 9', 'ie 10'],
        map: true
      },
      dynamic: {
        expand: true,
        flatten: true,
        src: '<%= src %>css/*.css',
        dest: '<%= src %>css'
      },
    },

    csslint: {
      lax: {
        options: {
          csslintrc: '.csslintrc'
        },
        src: '<%= src %>css/*.css'
      }
    },

    concurrent: {
      dev: {
        compile: ['coffee', 'sass'],
        lint: ['coffeelint', 'csslint']
      }
    },

    watch: {
      livereload: {
        files: ['src/**/*', '!src/sass/*', 'protected/views/**/*'],
        options: { livereload: true }
      },
      coffee: {
        files: '<%= src %>coffee/**/*',
        tasks: 'newer:coffee'
      },
      sass: {
        files: '<%= src %>sass/**/*',
        tasks: 'sass'
      },
      coffeelint: {
        files: '<%= src %>coffee/**/*',
        tasks: 'newer:coffeelint'
      },
      autoprefixer: {
        files: '<%= src %>css/**/*',
        tasks: 'newer:autoprefixer'
      },
      csslint: {
        files: '<%= src %>css/**/*',
        tasks: 'newer:csslint'
      }
    },

    clean: {
      prod: ['<%= assets %>javascripts/*.js', '<%= assets %>stylesheets/*.css']
    },

    uglify: {
      options: {
        compress: {
          drop_console: true
        }
      },
      application: {
        files: {
          '<%= assets %>javascripts/application.min.js': ['<%= src %>js/vendor/*.js', '<%= src %>js/application.js']
        }
      }
    },

    cssmin: {
      application: {
        files: {
          '<%= assets %>stylesheets/application.min.css': ['<%= src %>css/vendor/*.css', '<%= src %>css/application.css']
        }
      }
    },

    notify: {
      prod: {
        options: {
          message: 'Production tasks completed successfully!'
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-coffeelint');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-notify');

  grunt.registerTask('dev', ['concurrent:dev', 'coffee', 'sass', 'watch']);
  grunt.registerTask('prod', ['clean:prod', 'newer:uglify', 'newer:cssmin', 'autoprefixer', 'notify:prod']);
};

