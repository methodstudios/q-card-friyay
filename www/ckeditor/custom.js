var removeTimer;
var alertHeight;

$(document).ready(function(){
    
    $('body').on('click', '.inline-close', function(){
        clearTimeout(removeTimer);
        var parent = $(this).parent();
        
        parent.animate({height: '0px'}, 250);
        setTimeout(function(){
            parent.remove();
        }, 250);                                            
    });
    
    // The "instanceCreated" event is fired for every editor instance created.
    CKEDITOR.on('instanceCreated', function (event){
        var editor = event.editor,
            element = editor.element;

            editor.on('configLoaded', function(){
                editor.config.extraPlugins = 'inlinesave';
                if (element.is('h1','h2','h3'))
                {
                    editor.config.toolbar = [
                        ['Inlinesave'],
                        ['Undo', 'Redo'],
                        ['Cut', 'Copy', 'Paste']
                    ];
                }                
                else if (!element.getAttribute('data-option') || element.getAttribute('data-option') === 'simple')
                {          
                    editor.config.toolbar = [
                        ['Inlinesave'],
                        ['Bold', 'Italic', 'Strike', '-', 'Link', 'Unlink'],
                        ['NumberedList', 'BulletedList'],
                        ['Undo', 'Redo'],
                        ['Cut', 'Copy', 'Paste']
                    ];
                }
                else
                {
                    editor.config.toolbar = [
                        ['Inlinesave'],
                        ['Bold', 'Italic', 'Strike', '-', 'Link', 'Unlink'],
                        ['NumberedList', 'BulletedList'],
                        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                        ['TextColor', 'Font', 'FontSize', 'Format', 'Styles'],
                        ['SpecialChar'],
                        ['Undo', 'Redo'],
                        ['Cut', 'Copy', 'Paste']
                    ];
                }
            });
    });

    CKEDITOR.plugins.add('inlinesave',
    {
        init: function(editor)
        {
            editor.addCommand('inlinesave', {
                exec : function(editor)
                {
                    addData();
                    function addData() {
                        clearTimeout(removeTimer);
                        $('.inline-edit-bar').remove(); // Make sure there are no messages remaining before a new request

                        var containerId = editor.container.getId();

                        var data = {};
                        data.Id = $('#' + containerId).data('id');
                        data.Field = $('#' + containerId).data('field');
                        data.Type = $('#' + containerId).data('type');

                        if (data.Id && data.Type && data.Field)
                        {
                            var html = '';
                            data.InPlaceValue = editor.getData();

                            $.ajax({
                                url: baseUrl + 'api',
                                method: 'post',
                                data: data,
                                success: function(response){
                                    if (response === '1')
                                    {
                                        html = buildInlineMessage('<p>Item saved successfully.</p>', 'success');       
                                        showInlineMessage(html, true);
                                        $('[contenteditable=true]').blur();
                                    }
                                    else
                                    {
                                        html = buildInlineMessage(response, 'error');      
                                        showInlineMessage(html, false);
                                    }
                                },
                                error: function(error){
                                    html = buildInlineMessage('<p>' + error.responseText + '</p>', 'error');
                                    showInlineMessage(html, false);
                                }
                            });
                        }
                    }
                }
            });
            editor.ui.addButton('Inlinesave',
            {
                label: 'Save',
                command: 'inlinesave',
                icon: basePath + 'ckeditor/images/inlinesave.png'
            });
        }
    });   
    
    /*
     * Add edit link to all image containers which are "editable"
     */
    $.each($('[data-image-editable=true]'), function(){
        var classes = 'inline-edit-image-link inline-edit-' + $(this).data('position');
        var link = '<a href="' + $(this).data('url') + '" target="_blank" class="' + classes + '">EDIT</a>';
        
        if (!$(this).css('position') || $(this).css('position') === '')
            $(this).css('position', 'relative');
        $(this).append(link);
    });
});

/*
 * This function can be used to enable the ckeditor for dynamically added elements
 * Simple give the element the class of dynamic-content and call this function after the element is added
 * IMPORTANT: When calling this function call it like so:
    if (typeof reEnableEditors == 'function')
        reEnableEditors()
 */
function reEnableEditors()
{
    $.each($('.dynamic-content'), function(key, value){
        var name;
        for(name in CKEDITOR.instances) {
            var instance = CKEDITOR.instances[name];
            if(this && this == instance.element.$) {
                return;
            }
        }
        CKEDITOR.inline(this);        
    });
}
    
function showInlineMessage(html, success)
{
    $('body').prepend(html); 
    $(".inline-edit-bar").height('0px').show().animateAuto("height", 250); 
    
    // Only auto hide the response if its a success msg
    if (success)
    {
        removeTimer = setTimeout(function(){
            $('.inline-edit-bar').animate({height: '0px'}, 250);
            setTimeout(function(){
                $('.inline-edit-bar').remove();
            }, 250);                                            
        }, 5000);  
    }      
}

function buildInlineMessage(message, type)
{
    return '\
    <div class="inline-edit-bar inline-flash inline-flash-' + type + '">\
        ' + message + '\
        <span class="inline-close">&times;</span>\
    </div>';
}

/*
 * This function gets the "auto" height of an element then animates it to that height
 * ie. Dynamic height/width animation
 */
jQuery.fn.animateAuto = function(prop, speed, callback){
    var elem, height, width;
    return this.each(function(i, el){
        el = jQuery(el), elem = el.clone().css({"height":"auto","width":"auto"}).appendTo("body");
        height = elem.css("height"),
        width = elem.css("width"),
        elem.remove();

        if(prop === "height")
            el.animate({"height":height}, speed, callback);
        else if(prop === "width")
            el.animate({"width":width}, speed, callback);  
        else if(prop === "both")
            el.animate({"width":width,"height":height}, speed, callback);
    });  
} 