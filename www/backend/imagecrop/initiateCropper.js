function initiateImageCropper(uniqueId, imageName, uploadPath, ajaxUrl, hiddenFieldId, folderWidth, folderHeight, filename)
{
    var largeCanvasClass = 'default' + uniqueId;
    var largeContainerClass = 'imageCrop' + uniqueId;

    // you may change the "one" variable to anything
    var one = new CROP();
    one.init('.' + largeCanvasClass);

    if (imageName && imageName !== '')
    {
        // Do not load old images for now since they should be changing them anyway - plus this is not working correctly yet
        //one.loadImg(uploadPath + imageName);
    }

    //$('body').off("click", ".cropButton", function(){
        // unbind any old event handlers
    //});
    $('body').on("click", ".cropButton" + uniqueId, function() {
        processImageUpload(one, folderWidth, folderHeight);
    });   
        
    function processImageUpload(one, folderWidth, folderHeight)
    {
        var width = folderWidth;
        var height = folderHeight;

        $('.' + largeCanvasClass).after('<canvas width="'+width+'" height="'+height+'" id="canvas' + uniqueId + '"/>');        
        
        var ctx = document.getElementById('canvas' + uniqueId).getContext('2d'),
                img = new Image,
                w = coordinates(one).w,
            h = coordinates(one).h,
            x = coordinates(one).x,
            y = coordinates(one).y;

        img.src = coordinates(one).image;

        img.onload = function() 
        {
            // Round images - fix for firefox
            w = Math.round(w);
            h = Math.round(h);
            width = Math.round(width);
            height = Math.round(height);
           
            try 
            {
                // draw cropped image
                ctx.drawImage(img, x, y, w, h, 0, 0, width, height); 
            }
            catch(err) 
            {
                // If the image did not crop it is probably too thin or too short so we check that below and resize accordingly
                var widthDivider = (w / width);
                var heightDivider = (h / height);
                
                if (Math.round(w / widthDivider) < width || Math.round(h / widthDivider) < height)
                {
                    // draw image as is
                    width = (w / widthDivider);
                    height = (h / widthDivider);
                    ctx.drawImage(img, 0, 0, width, height);
                }
                else if (Math.round(h / heightDivider) < height || Math.round(w / heightDivider) < width)
                {
                    // draw image as is
                    width = (w / heightDivider);
                    height = (h / heightDivider);
                    ctx.drawImage(img, 0, 0, width, height);
                } 
            }                   

            $.ajax({
                    type: "post",
                    dataType: "json",
                    url: ajaxUrl,
                    data: { image: document.getElementById('canvas' + uniqueId).toDataURL(), filename: filename, width: folderWidth, height: folderHeight }
            })
            .success(function(data) {
                $(hiddenFieldId).val(data.filename);
        
                if ($('.update-thumb-' + uniqueId))
                {
                    d = new Date();
                    $('.update-thumb-' + uniqueId).find('img').attr('src', uploadPath + filename + '.jpg?' + d.getTime());
                }
            });                    
        }        
    }

    $('body').on("click", ".newupload" + uniqueId, function() {
        $('#uploadfile' + uniqueId).click();
    });

    $('body').change("#uploadfile" + uniqueId, function() {

        loadImageFile();

        // resets input file
        if (document.getElementById("uploadfile" + uniqueId))
        {
            $('#uploadfile' + uniqueId).wrap('<form>').closest('form').get(0).reset();
            $('#uploadfile' + uniqueId).unwrap();
        }
     });


    //  get input type=file IMG through base64 and send it to the cropper
    // --------------------------------------------------------------------------
    var oFReader = new FileReader(), rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
    function loadImageFile() {
        if (!document.getElementById("uploadfile" + uniqueId) || document.getElementById("uploadfile" + uniqueId).files.length === 0) 
            return;
        
        var oFile = document.getElementById("uploadfile" + uniqueId).files[0];
        if (!rFilter.test(oFile.type)) 
            return;
        
        oFReader.readAsDataURL(oFile);            
    }

    oFReader.onload = function (oFREvent) {

        var html = '\
            <div class="' + largeCanvasClass + '">\
                <div class="cropMain" style="width: ' + (folderWidth+80) + 'px; height: ' + (folderHeight+80) + 'px;"></div>\
                <div class="cropSlider"></div>\
                <div class="cropButtonWrap">\
                    <span style="font-size:11px;" class="cropInstructions">&nbsp;Click button when this image is ready to save</span>\
                    <div class="clear"></div>';
        
        if ($('.' + largeCanvasClass + ' .backButton' + uniqueId).hasClass('back-change-back'))
            html += '<button class="backButton' + uniqueId + ' back-change-back" type="button" onclick="pressBackButton(' + uniqueId + ');" style="display:none;">Back</button>';
        else
            html += '<button class="backButton' + uniqueId + '" type="button" onclick="pressBackButton(' + uniqueId + ');">Back</button>';
        
        if ($('.' + largeCanvasClass + ' .cropButton' + uniqueId).hasClass('next-change-back'))
            html += '<button class="cropButton' + uniqueId + ' next-change-back" type="button" onclick="pressCropButton(this, ' + uniqueId + ');">' + $('.cropButton' + uniqueId).html() + '</button>';
        else
            html += '<button class="cropButton' + uniqueId + '" type="button" onclick="pressCropButton(this, ' + uniqueId + ');">' + $('.cropButton' + uniqueId).html() + '</button>';

        html += '\
            </div>';

        $('.' + largeCanvasClass).html(html);

        one = new CROP();
        one.init('.' + largeCanvasClass);
        one.loadImg(oFREvent.target.result);
    };
}

function pressCropButton(selector, id)
{
    if ($(selector).html() === 'Finish')
    {
        $('#lightbox-' + id).trigger('close');
    }
    else
    {
        var floatId = parseFloat(id)+1;
        $('#lightbox-' + id).trigger('close');
        $('#lightbox-' + floatId).lightbox_me();
    }
}

function pressBackButton(id)
{
    var floatId = parseFloat(id)-1;
    $('#lightbox-' + id).trigger('close');
    $('#lightbox-' + floatId).lightbox_me();
}

$(document).ready(function(){
    // Show underline and text color change on "change" link when hovering over tiles
    $('.update-thumb-tile').hover(function(){
        $(this).find('.update-thumb-link a').css('text-decoration', 'underline');
        $(this).find('.update-thumb-link a').css('color', '#2a6496');
    }, function(){
        $(this).find('.update-thumb-link a').css('text-decoration', 'none');
        $(this).find('.update-thumb-link a').css('color', '#428bca');
    });

    $('.show-all-lightboxes').click(function(){
        var id = $(this).attr('data-id');

        $.each($('.lightbox .cropButtonWrap'), function(key, value){
            // If the next/finish button had its text changed, reset it
            if ($(this).find('.next-change-back'))
            {
                $(this).find('.next-change-back').html('Next');
                $(this).find('.next-change-back').removeClass('next-change-back');
            }

            // If the back button was previously hidden then reset it
            if ($(this).find('back-change-back'))
            {
                $(this).find('.back-change-back').show();
                $(this).find('.back-change-back').removeClass('back-change-back');
            }
        });

        $('#lightbox-' + id).lightbox_me();
    });

    $('.show-lightbox').click(function(){
        var id = $(this).attr('data-id');

        // If the next/finish button had its text changed, reset it
        if ($('.cropButton' + id).hasClass('next-change-back'))
        {
            $('.cropButton' + id).html('Next');
            $('.cropButton' + id).removeClass('next-change-back');
        }

        // If the back button was previously hidden then reset it
        if ($('.backButton' + id).hasClass('back-change-back'))
        {
            $('.backButton' + id).show();
            $('.backButton' + id).removeClass('back-change-back');
        }

        // If the user clicks the "change" link, hide the "back" button and change "next" button to say "finish"
        if ($(this).attr('data-change'))
        {
            if (id != '0')
            {
                $('.backButton' + id).hide();
                $('.backButton' + id).addClass('back-change-back');
            }

            if ($('.cropButton' + id).html() == 'Next')
            {
                $('.cropButton' + id).html('Finish');
                $('.cropButton' + id).addClass('next-change-back');
            }
        }

        $('#lightbox-' + id).lightbox_me();
    });    
});