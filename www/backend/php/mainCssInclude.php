<?php
// change the following paths if necessary
$yii=dirname(__FILE__).'/../../../framework/yii.php';

require_once($yii);

function getMainConfig()
{
    $configMain = require_once(dirname( __FILE__ ) . '/../../../protected/backend/config/main.php');

    $configLocal = array();
    if (IS_DEVELOPMENT && file_exists(dirname(__FILE__) . '/../../../protected/backend/config/main.local.php'))
    {
        $configLocal = require_once(dirname( __FILE__ ) . '/../../../protected/backend/config/main.local.php');
    }

    // If $configLocal exists it will overwrite the main config settings
    return array_replace_recursive($configMain, $configLocal);
}

function getDbConfig()
{
    // If the user has set a database.local.php file then include that (overwrite the main db config)
    if (file_exists(dirname(__FILE__) . '/../../../protected/backend/config/database.local.php'))
    {
        $dbConfig = require_once(dirname( __FILE__ ) . '/../../../protected/backend/config/database.local.php');
    }
    // If backend config exists then use it otherwise use the frontend DB
    elseif (file_exists(dirname(__FILE__) . '/../../../protected/backend/config/database.php'))
    {
        $dbConfig = require_once(dirname( __FILE__ ) . '/../../../protected/backend/config/database.php');
    }
    else
    {
        if (file_exists(dirname(__FILE__) . '/../../../protected/config/database.local.php'))
        {
            $dbConfig = require_once(dirname( __FILE__ ) . '/../../../protected/config/database.local.php');
        }
        else
        {
            $dbConfig = require_once(dirname( __FILE__ ) . '/../../../protected/config/database.php');
        }
    }

    return $dbConfig;
}

// Set configurations based on environment
if (strpos($_SERVER['SERVER_NAME'], 'localhost') !== FALSE || strpos($_SERVER['SERVER_NAME'], '.local') !== FALSE)
{
    // Enable debug mode for development environment
    defined('YII_DEBUG') or define('YII_DEBUG',true);

    // Specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

    define ('IS_DEVELOPMENT', true);
}
else
{
    define ('IS_DEVELOPMENT', false);
}

// Define separate STAGING constant incase we need to prevent live API calls etc
if (strpos($_SERVER['SERVER_NAME'], 'dev.') !== FALSE || strpos($_SERVER['SERVER_NAME'], 'service.') !== FALSE || strpos($_SERVER['SERVER_NAME'], 'staging.') !== FALSE)
{
    define('IS_STAGING', true);
}
else
{
    define('IS_STAGING', false);
}

$configMain = getMainConfig();
$dbConfig = getDbConfig();

// Run application
$config = CMap::mergeArray($configMain, $dbConfig);
$path = dirname(__FILE__) . '/../../../protected/components/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
$webSettings = Yii::createComponent('WebSettings');

$webSettings->getValue('ABC');