// Load the Visualization API and the piechart package.
google.load('visualization', '1', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.setOnLoadCallback(drawChart);

function drawChart(type, jsonData, title, vAxisLabel, hAxisLabel, height, width)
{
    if (type == 'pie')
    {
        drawPieChart(jsonData, title, vAxisLabel, hAxisLabel, height, width);
    }

    if (type == 'bar')
    {
        drawBarChart(jsonData, title, vAxisLabel, hAxisLabel, height, width);
    }

    if (type == 'line')
    {
        drawLineChart(jsonData, title, vAxisLabel, hAxisLabel, height, width);
    }
}

function drawPieChart(jsonData, title, vAxisLabel, hAxisLabel, height, width)
{
    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    var options = {
        width: width,
        height: height,
        title: title,
        vAxis: {title: vAxisLabel,  titleTextStyle: {color: 'black'}},
        hAxis: {title: hAxisLabel,  titleTextStyle: {color: 'black'}},
        colors: [mainColor]
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('chart-div'));
    chart.draw(data, options);
}

function drawBarChart(jsonData, title, vAxisLabel, hAxisLabel, height, width)
{
    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    var options = {
        width: width,
        height: height,
        title: title,
        vAxis: {title: vAxisLabel,  titleTextStyle: {color: 'black'}},
        hAxis: {title: hAxisLabel,  titleTextStyle: {color: 'black'}},
        colors: [mainColor]
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.BarChart(document.getElementById('chart-div'));
    chart.draw(data, options);
}

function drawLineChart(jsonData, title, vAxisLabel, hAxisLabel, height, width)
{
    // Create our data table out of JSON data loaded from server.
    var data = new google.visualization.DataTable(jsonData);

    var options = {
        width: width,
        height: height,
        title: title,
        vAxis: {title: vAxisLabel,  titleTextStyle: {color: 'black'}},
        hAxis: {title: hAxisLabel,  titleTextStyle: {color: 'black'}},
        legend: 'none',
        pointSize: 5,
        colors: [mainColor]
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.LineChart(document.getElementById('chart-div'));
    chart.draw(data, options);
}