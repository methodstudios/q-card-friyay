// ENABLE ISOTOPE FOR VIEW PAGES
function initiateIsotope() // For use with ajax generated content
{
    var $container = $('.list-view .items');
    // init
    $container.isotope({
      // options
      itemSelector: '.view',
      layoutMode: 'fitRows'
    });    
}

// This function is to support the isotope library when using pagination and images in the same page
function initiateIsotopeDelayed()
{
    var noOfImages = $(".view-tiles .items .view img").length;
    var noLoaded = 0;

    if (noOfImages > 0)
    {
        $('.view-tiles').css('visibility', 'hidden');

        $(".view-tiles .items .view img").on('load', function(){

            noLoaded++;
            if (noOfImages === noLoaded)
            {
                $('.view-tiles').css('visibility', 'visible');
                initiateIsotope();
            }

        });
    }
    else
    {
        initiateIsotope();
    }
}

/* 
 * Use the window.load function where required to wait for images to finish loading 
*/
$(window).load(function(){
    initiateIsotope();
    
    /* 
     * Dynamically set the top property of the menu container 
     * This is so if the screen height is too small is shows a scroll bar for the menu only 
     * NB: If you change this function you will need to change the similar function below - search for var headerHeight
    */
    var headerHeight = parseFloat($('.mainHeader').outerHeight());
    var logoHeight = parseFloat($('.mainLogo').outerHeight());
    $('.mainMenuContainer').css('top', (headerHeight + logoHeight) + 'px');    
});
//------------------------------------------//

// Function to switch between grid/list views on crud index pages
function switchView(selector, type)
{
    $('.view-icons .active').removeClass('active');
    
    if (type === 'tiles')
    {
        $('.view-list').hide();
        $('.view-tiles').fadeIn(500);
    }
    else
    {
        $('.view-tiles').hide();
        $('.view-list').fadeIn(500);
    }
    
    if (Modernizr.localstorage) 
    {
        // Save the selection into localstorage so we remember for next time
        localStorage['view-' + $(selector).attr('controller')] = type;
    }    
    
    $(selector).addClass('active');
    
    initiateIsotope();
}

// Checks localstorage for previous selections and loads them if applicable
function defaultView()
{
    if (Modernizr.localstorage && localStorage['view-' + $('#view-btn-tiles').attr('controller')]) 
    {
        var selection = localStorage['view-' + $('#view-btn-tiles').attr('controller')];
        switchView($('#view-btn-' + selection), selection);
    }      
}

// Shows/hides the settings menu on the left
function displaySettings()
{
    var $settingsWrap = $('.settingsWrap');
    var $settingsOverlay = $('.settingsOverlay');
    
    $settingsWrap.toggle();
    $settingsOverlay.toggle();
}

// Switch out tick icon and store value as ticked
function checkCheckbox(selector)
{
    if ($(selector).hasClass('checked'))
    {
        $(selector).removeClass('checked');
        $(selector).closest('tr').css('color', 'inherit');
        $(selector).closest('tr').find('a').css('color', '');
        $(selector).closest('tr').css('background-color', '');
    }
    else
    {
        $(selector).addClass('checked');
        $(selector).closest('tr').css('color', '#fff');
        $(selector).closest('tr').find('a').css('color', '#fff');
        $(selector).closest('tr').css('background-color', mainColor);
    }
}

// Delete an individual item when the delete icon is clicked
function deleteItem(id, url)
{
    var check = confirm('Are you sure you want to delete this item?');

    if (check)
    {
        $.post(url + '/delete?id=' + id, function(data){
            location.reload();
        });
    }
}

// Delete all checked items at once
function bulkDelete(url)
{
    var check = confirm('Are you sure you want to delete all of the selected items?');

    if (check)
    {
        var data = new Object();
        data.ids = new Array();
        
        var cnt = 0;
        $.each($('.list-checkbox.checked'), function(index,selector){
            data.ids[cnt] = $(selector).attr('primarykey');
            cnt++;
        });
        
        $.ajax({
            url: url + '/delete',
            type: 'post',
            data: data,
            success: function(){
                location.reload();
            }
        });
    }    
}

// Set qtip options for tooltops
function setTooltip(selector, text)
{
    $(selector).qtip({
        content: {
            text: text
        },
        position: {
            my: 'middle left',
            at: 'middle right'
        },
        style: {
            def: false, //Kill plugin styles
            classes: 'tooltipObj' //add my own
        }
    });    
}

function hideHeaderFixMenu()
{
    var headerHeight = 60;
    var defaultPaddingTop = 60;

    var headerHeight = parseFloat($('.mainHeader').outerHeight());
    var logoHeight = parseFloat($('.mainLogo').outerHeight());  
    var defaultMenu = headerHeight + logoHeight;        

    // Once the header has been scrolled past
    if (parseFloat($(document).scrollTop()) >= 60)
    {
        if ($('#mainMenu').css('padding-top') == defaultPaddingTop + 'px')
        {
            $('#mainMenu').stop().animate({'padding-top': (defaultPaddingTop-headerHeight) + 'px'}, 50);
            $('.subMenu').stop().animate({'padding-top': (defaultPaddingTop-headerHeight) + 'px'}, 50);
            $('.mainMenuContainer').stop().animate({top: (defaultMenu-headerHeight) + 'px'}, 50);
        }
    }
    else
    { 
        if ($('#mainMenu').css('padding-top') != defaultPaddingTop + 'px')
        {            
            $('#mainMenu').stop().animate({'padding-top': defaultPaddingTop + 'px'}, 50);
            $('.subMenu').stop().animate({'padding-top': defaultPaddingTop + 'px'}, 50);
            $('.mainMenuContainer').stop().animate({top: defaultMenu + 'px'}, 50);      
        }
    }    
}

function setHiddenStatus()
{
    if ($('.status-switch input[type="checkbox"]').is(':checked'))
        $('.status-switch input[type="hidden"]').val(statusEnabled);
    else
        $('.status-switch input[type="hidden"]').val(statusDisabled);
}

function switchHiddenStatus(selector)
{
    if ($(selector).hasClass('bootstrap-switch-on'))
        $(selector).parent().find('input[type="hidden"]').val(statusEnabled);
    else
        $(selector).parent().find('input[type="hidden"]').val(statusDisabled);
}

function convertToSlug(text)
{
    return text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}

function addSlug(selector, targetId)
{
    var slug = convertToSlug($(selector).val());
    $(targetId).val(slug);
}

// Display the Bulk Actions drop downs on click of the arrow
$('.actionsBtn').click(function(e){
    e.stopPropagation();
    $(this).parent().find('.actionsList').toggle(); 
    return false;
});

// Hide the Bulk Actions dropdown when clicking away from it
$('body').click(function(){
    $('.actionsList').hide();
});

$('.settingsOverlay').click(function(){
    displaySettings();
});

$(document).ready(function(){
    if (!$('html').hasClass('lte-ie8'))
    {
        $('.circlifulItem').circliful();
    }    
    
    defaultView();
    
    setTooltip('#bounceToolTip', 'Bounce rate is the percentage of single-page visits (i.e. visits in which the person left your site from the entrance page without interacting with the page).');
    setTooltip('#pageSessionToolTip', 'Pages/Session (Average Page Depth) is the average number of pages viewed during a session. Repeated views of a single page are counted.');
    setTooltip('#avgSessionToolTip', 'The average length of a Session.');
    
    $(document).scroll(function() {
        hideHeaderFixMenu();
    }); 
    
    // Enable the status toggle switches
    $('.status-switch input[type="checkbox"]').bootstrapSwitch({size: 'small'});
    setHiddenStatus();
    $('.bootstrap-switch').click(function(){
        switchHiddenStatus(this);
    });        
});