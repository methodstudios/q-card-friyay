<?php
    session_start();
    header("Content-type: text/css; charset: UTF-8");
    $mainColor = $_SESSION['MAIN_ADMIN_HEX'];
    $mainColorRgb = $_SESSION['MAIN_ADMIN_RGB'];
?>
@font-face {
    font-family: 'proxima_novalight';
    src: url('../fonts/proximanova-light_2-webfont.eot');
    src: url('../fonts/proximanova-light_2-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/proximanova-light_2-webfont.woff') format('woff'),
         url('../fonts/proximanova-light_2-webfont.ttf') format('truetype'),
         url('../fonts/proximanova-light_2-webfont.svg#proxima_novalight') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'signikalight';
    src: url('../fonts/signika-light-webfont.eot');
    src: url('../fonts/signika-light-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/signika-light-webfont.woff') format('woff'),
         url('../fonts/signika-light-webfont.ttf') format('truetype'),
         url('../fonts/signika-light-webfont.svg#signikalight') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'signika_negativelight';
    src: url('../fonts/signikanegative-light-webfont.eot');
    src: url('../fonts/signikanegative-light-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/signikanegative-light-webfont.woff') format('woff'),
         url('../fonts/signikanegative-light-webfont.ttf') format('truetype'),
         url('../fonts/signikanegative-light-webfont.svg#signika_negativelight') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'signika_negativeregular';
    src: url('../fonts/signikanegative-regular-webfont.eot');
    src: url('../fonts/signikanegative-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/signikanegative-regular-webfont.woff') format('woff'),
         url('../fonts/signikanegative-regular-webfont.ttf') format('truetype'),
         url('../fonts/signikanegative-regular-webfont.svg#signika_negativeregular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'source_sans_prolight';
    src: url('../fonts/sourcesanspro-light-webfont.eot');
    src: url('../fonts/sourcesanspro-light-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/sourcesanspro-light-webfont.woff') format('woff'),
         url('../fonts/sourcesanspro-light-webfont.ttf') format('truetype'),
         url('../fonts/sourcesanspro-light-webfont.svg#source_sans_prolight') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'source_sans_proregular';
    src: url('../fonts/sourcesanspro-regular-webfont.eot');
    src: url('../fonts/sourcesanspro-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/sourcesanspro-regular-webfont.woff') format('woff'),
         url('../fonts/sourcesanspro-regular-webfont.ttf') format('truetype'),
         url('../fonts/sourcesanspro-regular-webfont.svg#source_sans_proregular') format('svg');
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'opensans_light';
    src: url('../fonts/OpenSans-Light-webfont.eot');
    src: url('../fonts/OpenSans-Light-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/OpenSans-Light-webfont.woff') format('woff'),
         url('../fonts/OpenSans-Light-webfont.ttf') format('truetype'),
         url('../fonts/OpenSans-Light-webfont.svg#opensans_light') format('svg');
    font-weight: normal;
    font-style: normal;
}

body {
    margin: 0;
    padding: 0;
    color: #4b4b4b;
    font: normal 14px opensans_light,Arial,Helvetica,sans-serif;
    background: #eee;
}

.left { float:left; }
.right { float:right; }
.clear { clear:both; }

#page {
    width:100%;
    height:100%;
    padding:0;
    position:absolute;
        top:0;
        left:0;
}

.form {
    float:left;
    width:100%;
}

.form form {
    float:left;
    width:100%;
}

.row {
    float:left;
    clear:both;
    margin:5px;
    width:100%;
}

.row label {
    float:left;
    min-width:165px;
    margin-top:5px;
    margin-right:5px;
}

.row .small-label {
    float:left;
    clear:both;
    font-size:11px;
    margin-bottom:5px;
}

.row input, .row select, .row textarea {
    float:left;
    margin:0 5px 0 0;
    padding:5px;
    border:1px solid #D1D1D1;
    border-radius:5px;
}

.row input[type=file] {
    border:none;
    padding-left:0;
}

.row input[type=submit] {
    padding:5px 15px;
    background-color:<?php echo $mainColor; ?>;
    color:#fff;
    text-transform:uppercase;
    border:none;
}

.nav-tabs > li > a {
    padding:15px 25px;
}

.list-view .items .view {
    padding:10px;
    margin:10px;
    border:1px solid #eee;
    width:350px;
}

.list-view .pager {
    clear:both;
}

.nav-pills {
    float:left;
}

.textarea-wrap {
    max-width:1000px;
}

.textarea-wrap textarea, .textarea-wrap div {
    clear:both;
}

.ui-datepicker {
    z-index:600 !important;
}

.flash {
    padding: 8px 35px 8px 14px;
    margin-bottom: 18px;
    color: #c09853;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    background-color: #fcf8e3;
    border: 1px solid #fbeed5;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

.flash-success {
    color: #468847;
    background-color: #dff0d8;
    border-color: #d6e9c6;
}

.flash-error {
    color: #b94a48;
    background-color: #f2dede;
    border-color: #eed3d7;
}

.flash-notice {
    color: #3a87ad;
    background-color: #d9edf7;
    border-color: #bce8f1;
}

.actionLinks {
    white-space:nowrap;
    text-align:right;
}

.footerSpacer {
    clear:both;
    width:100%;
    height:50px;
}

.info-icon {
    display:inline-block;
    background-color:<?php echo $mainColor; ?>;
    height:16px;
    width:16px;
    border-radius:16px;
    font-style:italic;
    color:#fff;
    text-align:center;
    font-size:13px;
    line-height:16px;
    text-transform:lowercase;
    cursor:pointer;
    padding-right:2px;
}

.tooltipObj {
    width:200px;
    min-height:50px;
    background-color:#fff;
    border:1px solid #eee;
    border-radius:10px;
}

.tooltipObj .qtip-content {
    font-family:signika_negativelight,Arial,sans-serif;
    font-size:14px;
    text-align:center;
    padding:8px 19px;
    line-height:1.2;
    color:#b3b3b3;
}

.tooltipObj .qtip-tip {
    border-color: <?php echo $mainColor; ?>;
}

.lightbox {
    background-color:#fff;
    border-radius:5px;
    padding:20px;
}

.lightbox canvas {
    display:none;
}

.cropButtonWrap {
    width:100%;
    text-align:center;
    margin:40px 0 20px 0;
}
/********************************************************************************
                                 HEADER STARTS
********************************************************************************/

.mainHeader {
    height:60px;
    width:100%;
    background-color:#fff;
    float:left;
    padding-left:15px;
    color:#b3b3b3;
    font-family:signika_negativelight,Arial,sans-serif;
    font-size:16px;
    z-index:701;
    position:relative;
}

.mainHeader h2 {
    margin:0;
    line-height:60px;
    font-size:28px;
    font-weight:normal;
    color:<?php echo $mainColor; ?>;
    font-family:source_sans_prolight, Arial, sans-serif;
}

.mainHeader .headerDate, .mainHeader .headerTime {
    line-height:60px;
    margin-right:30px;
    padding-left:30px;
    background-repeat:no-repeat;
    background-position:0 20px;
}

.mainHeader .headerDate {
    background-image:url(../img/calendar-icon.png);
}

.mainHeader .headerTime {
    background-image:url(../img/clock-icon.png);
}

.mainHeader .settingsBtn a {
    display:block;
    height:60px;
    width:60px;
    background-color:<?php echo $mainColor; ?>;
    background-image:url(../img/settings.png);
    background-repeat:no-repeat;
    background-position:center center;
    outline:none;
}

.settingsWrap {
    width:310px;
    height:100%;
    background-color:<?php echo $mainColor; ?>;
    position:fixed;
        right:0;
        top:0;
    z-index:620;
    color:#fff;
}

.settingsWrap h3 {
    font-family:source_sans_prolight,Arial,sans-serif;
    font-size:23px;
    font-weight:normal;
    margin:30px 0 0 30px;
    clear:both;
    float:left;
}

.settingsWrap ul {
    margin:10px 0 20px 0;
    padding:0;
    list-style-type:none;
    clear:both;
    float:left;
    width:100%;
}

.settingsWrap ul li {
    float:left;
    clear:both;
    width:100%;
}

.settingsWrap ul li a {
    display:block;
    height:40px;
    line-height:40px;
    font-family:signika_negativelight,Arial,sans-serif;
    font-size:15px;
    width:100%;
    text-indent:10px;
    color:#fff;
    padding-left:30px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.settingsWrap ul li a:hover {
    text-decoration:none;
    color:#fff;
    background-color:rgba(255,255,255,0.5);
}

.settingsOverlay {
    width:100%;
    height:100%;
    position:fixed;
    z-index:600;
    background-color:rgba(255,255,255,0.8);
}

/********************************************************************************
                                  HEADER ENDS
********************************************************************************/

/********************************************************************************
                                MAIN MENU STARTS
********************************************************************************/

#mainMenu {
    width:210px;
    background-color:#fff;
    position:fixed;
        left:0;
        top:0;
    padding-top:60px;
    height:100%;
}

#mainMenu .mainLogo {
    width:100%;
    background-color:<?php echo $mainColor; ?>;
    text-align:center;
    padding:25px 0;
}

#mainMenu .mainLogo img {
    max-width:85%;
    max-height:80px;
}

#mainMenu .mainMenuContainer {
    width:210px;
    position:fixed;
    /*top: This is generated dynamically in backend.js */
    bottom:40px;
    overflow:auto;
}

#mainMenu ul {
    list-style-type:none;
    padding:0;
    margin:0;
}

#mainMenu ul li {
    float:left;
    clear:both;
    width:100%;
    border-bottom:1px solid #f0f0f0;
}

#mainMenu ul li a {
    display:block;
    height:60px;
    width:100%;
    line-height:60px;
    padding-left:30px;
    color:#777777;
    font-family:signika_negativelight,Arial,sans-serif;
    font-size:15px;
}

#mainMenu ul li a:hover {
    text-decoration:none;
    color:<?php echo $mainColor; ?>;
}

#mainMenu ul li.active a {
    color:<?php echo $mainColor; ?>;
}

#mainMenu .menuFooter {
    position:absolute;
        left:10px;
        bottom:10px;
    background-image:url(../img/grid_logo_small.png);
    background-repeat:no-repeat;
    background-position:6px 1px;
}

#mainMenu .menuFooter a {
    display:block;
    color:#b3b3b3;
    font-size:10px;
    text-indent:35px;
    line-height:25px;
    outline:none;
}

#mainMenu .menuFooter a:link { text-decoration:none; }
#mainMenu .menuFooter a:visited { text-decoration:none; }
#mainMenu .menuFooter a:hover { text-decoration:none; }
#mainMenu .menuFooter a:active { text-decoration:none; }

.subMenu {
    width:210px;
    height:100%;
    position:fixed;
        left:210px;
        top:0;
    padding-top:60px;
    z-index:220;
    background-color:#ddd;
    background-color:<?php echo $mainColorRgb; ?>;
}

.subMenu h2 {
    color:<?php echo $mainColor; ?>;
    font-size:23px;
    margin:75px 0 30px 0;
    text-indent:30px;
}

.subMenu ul {
    margin:0;
    padding:0;
    list-style-type:none;
}

.subMenu ul li {
    float:left;
    clear:both;
    width:100%;
}

.subMenu ul li a {
    display:block;
    height:40px;
    line-height:40px;
    font-family:signika_negativelight,Arial,sans-serif;
    font-size:15px;
    width:100%;
    color:#777777;
    padding-left:30px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.subMenu ul li a:hover, .subMenu ul li.active a {
    text-decoration:none;
    color:#fff;
    background-color:<?php echo $mainColor; ?>;
}

/********************************************************************************
                                MAIN MENU ENDS
********************************************************************************/

/********************************************************************************
                                RIGHT CONTENT STARTS
********************************************************************************/

.rightContent {
    width:auto;
    overflow:hidden;
}

.contentPanel {
    margin:15px 15px 15px 225px;
    background-color:#fff;
    border-radius:5px;
    padding:30px;
}

.mainContentPanel {
    margin-top:75px; /* The first panel has to account for the header */
}

.rightContent .search-form {
    float:right;
    margin:0;
}

.rightContent .search-form input {
    float:left;
}

.rightContent .search-form input[type=search] {
    background-color:#f1f1f1;
    border:none;
    padding:10px 0 10px 14px;
    font-family:signika_negativelight,Arial,sans-serif;
    color:#4b4b4b;
    width:180px;
    line-height:14px;
    font-size:14px;
    height:37px;
}

.rightContent .view-icons a {
    display:block;
    height:37px;
    width:34px;
    text-align:center;
    background-color:#f1f1f1;
    float:left;
    margin-right:6px;
    outline:none;
}

.rightContent .view-icons a:hover, .rightContent .view-icons a.active {
    background-color:<?php echo $mainColor; ?>;
}

.rightContent .view-icons a .glyphicon {
    line-height:34px;
    color:#8a8a8a;
    width:34px;
    height:37px;
}

.rightContent .view-icons a .glyphicon:hover, .rightContent .view-icons a.active .glyphicon {
    color:#fff;
}

.rightContent h1 {
    margin-top:0;
    font-family:source_sans_proregular,Arial,sans-serif;
    text-transform:uppercase;
    font-size:22px;
    color:#4b4b4b;
    width:100%;
}

.rightContent .pager .yiiPager .last,
.rightContent .pager .yiiPager .first {
    display:none;
}

.rightContent .view-list, .rightContent .view-tiles {
    position:relative;
}

.rightContent .view-list .grid-view, .rightContent .view-tiles .list-view {
    margin-top:15px;
    border-top:1px solid #dbdbdb;
    padding:15px 0;
}

.rightContent .view-list .grid-view, .rightContent .view-tiles .list-view .view a:hover {
    text-decoration:none;
}

.rightContent .view-list .grid-view table.items {
    border:0;
}

.rightContent .view-list .grid-view table.items th {
    background:none;
    text-align:left;
    color:#4b4b4b;
    font-size:14px;
    font-family:signika_negativelight,Arial,sans-serif;
}

.rightContent .view-list .grid-view table.items td {
    border:0;
}

.rightContent .view-list .grid-view table.items tr.even {
    background:none;
}

.rightContent .view-list .grid-view table.items tr.odd {
    background:#f6f7f7;
}

.rightContent .view-list .grid-view table.items tbody tr:hover {
    background:none;
}

.rightContent .view-list .grid-view table.items th, .rightContent .view-list .grid-view table.items td {
    font-size:14px;
}

.rightContent .view-list .grid-view .actionLinks a {
    color:#000;
    font-family:signika_negativelight,Arial,sans-serif;
    font-weight:bold;
    font-size:13px;
}

.rightContent .view-list .grid-view a:hover {
    color:<?php echo $mainColor; ?>;
    text-decoration:none;
}

.rightContent .view-list .grid-view table.items th a {
    color:#4b4b4b;
    font-size:14px;
}

.rightContent .view-list .grid-view table.items th a:hover,
.rightContent .view-list .grid-view table.items th a.asc,
.rightContent .view-list .grid-view table.items th a.desc {
    color:<?php echo $mainColor; ?>;
}

.rightContent .grid-view table.items th, .rightContent .grid-view table.items td {
    padding:8px 4px;
}

.rightContent .list-checkbox {
    width:15px;
    height:15px;
    background-color:#ddd;
    cursor:pointer;
}

.rightContent .list-checkbox.checked {
    background-image:url(../img/checkbox-tick.png);
    background-repeat:no-repeat;
}

.rightContent .bulkActions {
    float:left;
    margin:20px 0 10px 0;
    position:relative;
}

.rightContent .bulkActions p {
    float:left;
    line-height:26px;
    margin-right:10px;
    font-family:signika_negativeregular,Arial,sans-serif;
}

.rightContent .bulkActions .actionsBtn {
    display:block;
    width:30px;
    height:26px;
    float:right;
    text-align:center;
    background-color:#F1F1F1;
    outline:none;
}

.rightContent .bulkActions .actionsBtn .glyphicon {
    line-height:24px;
    color:#8A8A8A;
}

.rightContent .bulkActions .actionsList {
    position:absolute;
        right:0;
        top:26px;
    background-color:#F1F1F1;
    min-width:200px;
    z-index:610;
}

.rightContent .bulkActions .actionsList ul {
    margin:0;
    padding:0;
    list-style-type:none;
}

.rightContent .bulkActions .actionsList ul li {
    float:left;
    clear:both;
    width:100%;
}

.rightContent .bulkActions .actionsList ul li a {
    width:100%;
    height:40px;
    line-height:40px;
    color:#4B4B4B;
    padding:0 15px;
    float:left;
}

.rightContent .bulkActions .actionsList ul li a:hover {
    background-color:#4491E8;
    text-decoration:none;
    color:#fff;
}

.rightContent table.detail-view tr.odd {
    background-color:#eee;
}

.rightContent table.detail-view th, .rightContent table.detail-view td {
    font-size:14px;
}

.rightContent .list-view .pager,
.rightContent .grid-view .pager {
    position:absolute;
        left:0;
        bottom:-65px;
    margin:0;
    text-align:center;
    width:100%;
    font-size:13px;
}

.rightContent .list-view .items .view img {
    max-width:80%;
    margin-bottom:10px;
    max-height:150px;
}

.rightContent .grid-view .items img {
    max-width:75px;
    max-height:60px;
}

.pager ul.yiiPager li {
    display:inline-block;
    margin:0 1px;
}

.pager ul.yiiPager a:link, .pager ul.yiiPager a:visited {
    display:inline-block;
    width:22px;
    height:22px;
    background:none;
    color:#4b4b4b;
    border:none;
    border-radius:0;
    font-family:source_sans_proregular,Arial,sans-serif;
    font-weight:bold;
    font-size:13px;
    line-height:22px;
    text-align:center;
    padding:0;
}

.pager ul.yiiPager a:hover, .pager ul.yiiPager a:active, .pager ul.yiiPager .selected a {
    background-color:<?php echo $mainColor; ?>;
    color:#fff !important;
}

.pager ul.yiiPager a .glyphicon {
    line-height:19px;
    top:2px;
}

.pager ul.yiiPager .next a,
.pager ul.yiiPager .previous a {
    float:none;
    background:none;
    color:<?php echo $mainColor; ?>;
    font-weight:normal;
}

.btn-default {
    background-color:#fff;
    border:1px solid <?php echo $mainColor; ?>;
    color:<?php echo $mainColor; ?>;
    padding:4px 8px;
    line-height:13px;
    font-size:13px;
}

.btn-default:hover {
    background-color:<?php echo $mainColor; ?>;
    border:1px solid <?php echo $mainColor; ?>;
    color:#fff;
}

.settings-container {
    float:left;
    min-width:400px;
}

.settings-container .row label {
    min-width:400px;
    width:auto;
    margin-right:10px;
}

.errorSummary {
    padding: 8px 35px 8px 14px;
    margin-bottom: 18px;
    color: #c09853;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    background-color: #fcf8e3;
    border: 1px solid #fbeed5;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    line-height:20px;
    color: #b94a48;
    background-color: #f2dede;
    border-color: #eed3d7;
}

label.error {
    color:#D84747;
}

input.error, select.error, textarea.error {
    border:1px solid #D84747;
}

.bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-primary, .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-primary {
    background:none repeat scroll 0 0 <?php echo $mainColor; ?>;
}

.status-switch {
    float:left;
    margin:0 0 5px 5px;
}

.status-switch label {
    width:165px;
}

.form .status-switch .bootstrap-switch {
    margin:0 0 0 5px;
}

.redactor_toolbar li .re-internalpagelinks, .redactor_toolbar li .re-internalnewslinks {
    font-family:arial, sans-serif;
    font-size:14px;
}

.re-internalpagelinks:before {
  content: "P";
}

.re-internalnewslinks:before {
  content: "N";
}

@media all and (max-width: 1400px) {
    .rightContent .redactor_box {
        min-width:700px;
    }
}

@media all and (max-width: 1200px) {
    .rightContent .redactor_box {
        min-width:500px;
    }
}

/********************************************************************************
                                RIGHT CONTENT ENDS
********************************************************************************/

/********************************************************************************
                                DASHBOARD STARTS
********************************************************************************/

.dashboardWrap h1 {
    font-family:signika_negativeregular,Arial,sans-serif;
    font-size:14px;
    color:#9e9e9e;
    white-space:nowrap;
}

.dashboardWrap .graphControls .graphBtn a {
    display:block;
    font-size:12px;
    color:#9e9e9e;
    padding:8px 16px;
    border-radius:5px;
    font-family:signika_negativeregular,Arial,sans-serif;
}

.dashboardWrap .graphControls .graphBtn {
    float:left;
    margin-left:15px;
}

.dashboardWrap .graphControls .graphBtn a:hover, .dashboardWrap .graphControls .selected a {
    color:#fff;
    background-color:<?php echo $mainColor; ?>;
    text-decoration:none;
}

.dashboardWrap .analyticsStats {
    float:left;
    clear:both;
}

.dashboardWrap .analyticsStats .statsPanel {
    float:left;
    min-width:200px;
    border-right:1px solid #eeeeee;
    text-align:center;
    font-size:37px;
    font-family:source_sans_prolight,Arial,sans-serif;
    color:<?php echo $mainColor; ?>;
}

.dashboardWrap .analyticsStats .statsPanel p {
    padding:20px;
    margin:0;
}

.dashboardWrap .analyticsStats .statsPanel.grey {
    color:#b3b3b3;
}

.dashboardWrap .analyticsStats .statsPanel .subTitle {
    font-size:14px;
    font-family:signika_negativelight,Arial,sans-serif;
}

.dashboardWrap .dashboardPanelLeft, .dashboardWrap .dashboardPanelRight {
    border-radius:5px;
    background-color:#fff;
    padding:30px;
    min-height:290px;
}

.dashboardWrap .dashboardPanelLeft {
    float:left;
    margin-right:1%;
    width:40%;
}

.dashboardWrap .dashboardPanelRight {
    float:right;
    width:59%;
}

.dashboardWrap .greyCopy {
    color:#b3b3b3;
    font-size:14px;
    font-family:signika_negativelight,Arial,sans-serif;
}

.dashboardWrap .blueCopy {
    font-family:source_sans_prolight,Arial,sans-serif;
    font-size:27px;
    color:<?php echo $mainColor; ?>;
}

.dashboardWrap .trafficSource {
    float:left;
    padding-right:30px;
    margin-right:30px;
    border-right:1px solid #f2f2f2;
    width:185px;
}

.dashboardWrap .trafficReferrals {
    max-width:220px;
}

.dashboardWrap .trafficSource .trafficGraphs {
    float:left;
}

.dashboardWrap .trafficSource .trafficGraphs .row {
    clear:both;
    margin:15px 0;
}

.dashboardWrap .trafficSource .trafficGraphs .circlifulItem {
    float:left;
}

.dashboardWrap .trafficSource .trafficGraphs .trafficValue {
    float:left;
    margin:0 10px;
    line-height:35px;
}

.dashboardWrap .trafficSource .trafficGraphs .trafficTitle {
    float:left;
    line-height:35px;
}

.dashboardWrap .trafficReferrals ol, .dashboardWrap .topLocations ol {
    margin:15px 0 0 15px;
    padding:0;
}

.dashboardWrap .trafficReferrals ol li, .dashboardWrap .topLocations ol li {
    color:<?php echo $mainColor; ?>;
    float:left;
    clear:both;
    margin-bottom:20px;
}

.dashboardWrap .topLocations {
    margin-left:30px;
    padding-left:30px;
    border-left:1px solid #f2f2f2;
}

.dashboardWrap .activeUsers {
    float:left;
    min-width:140px;
    margin:0 15px 30px 0;
    padding-right:15px;
    border-right:1px solid #f2f2f2;
}

.dashboardWrap .activeUsers .circlifulItem {
    margin:30px 0 0 0;
}

.dashboardWrap .activeUsers .circlifulItem .circle-text {
    color:<?php echo $mainColor; ?>;
}

.dashboardWrap .activeInfo .activeGraph {
    width:110px;
    background-color:#eee;
    height:28px;
    margin:0 0 0 10px;
    border-radius:0 10px 10px 0;
}

.dashboardWrap .activeInfo .activeGraph .activeGraphInner {
    background-color:<?php echo $mainColor; ?>;
    border-radius:0 10px 10px 0;
    float:left;
    height:100%;
}

.dashboardWrap .googleLoader {
    text-align:center;
    padding:50px 0 50px 0;
}

.dashboardWrap .googleContainer {
    display:none;
}

@media all and (max-width: 1530px) {
    .dashboardPanelRight .topLocations {
        border-left:none;
        margin:20px 0 0 0;
        padding-left:0;
        clear:both;
        display:none;
    }
}

@media all and (max-width: 1230px) {
    .dashboardPanelLeft .activeUsers {
        border-right:none;
    }

    .dashboardPanelLeft .activeInfo {
        clear:both;
    }
}

@media all and (max-width: 1080px) {
    .dashboardPanelRight .trafficSource {
        border-right:none;
    }

    .dashboardPanelRight .trafficReferrals {
        margin:20px 0 0 0;
        clear:both;
    }
}

/********************************************************************************
                                DASHBOARD ENDS
********************************************************************************/

/********************************************************************************
                                LOGIN PAGES START
********************************************************************************/

.loginContainer {
    display: table;
    position: absolute;
    height: 100%;
    width: 100%;
    color:#393939;
}

.loginContainer  .middle {
    display: table-cell;
    vertical-align: middle;
}

.loginContainer  .inner {
    margin-left: auto;
    margin-right: auto;
    width: 680px;
    height:320px;
}

.loginContainer h1 {
    font-family:signika_negativeregular,Arial,sans-serif;
    font-size:14px;
    color:#9e9e9e;
    text-transform:uppercase;
}

.loginLogoContainer {
    float:left;
    width:210px;
    height:100%;
    background-color:<?php echo $mainColor; ?>;
    border-radius:5px 0 0 5px;
    position:relative;
}

.loginLogoContainer .mainLogoOuter {
    display: table;
    position: absolute;
    height: 100%;
    width: 100%;
}

.loginLogoContainer .mainLogoMiddle {
    display: table-cell;
    vertical-align: middle;
}

.loginLogoContainer .mainLogoInner {
    margin-left: auto;
    margin-right: auto;
    width: 210px;
    text-align:center;
}

.loginLogoContainer .mainLogoInner img {
    max-width:85%;
    max-height:80px;
}

.loginContentContainer {
    float:left;
    padding:35px;
    background-color:#fff;
    border-radius:0 5px 5px 0;
    width:470px;
    height:100%;
    position:relative;
}

.loginContentContainer .row label {
    font-weight:normal;
    color:#393939;
    font-family:opensans_light,arial,sans-serif;
    width:85px;
    min-width:inherit;
}

.loginContentContainer .row input {
    width:310px;
    margin-right:0px;
}

.loginContentContainer #reset-pass-form .row label {
    width:130px;
}

.loginContentContainer #reset-pass-form .row input[type=text], .loginContentContainer #reset-pass-form .row input[type=password] {
    width:265px;
}

.loginContentContainer .row input[type=submit] {
    width:80px;
    background-color:<?php echo $mainColor; ?>;
    text-align:center;
    color:#fff;
    text-transform:uppercase;
    border:none;
    float:right;
    margin:30px 0 0 0;
}

.loginContentContainer .rowFixedBottom {
    position:absolute;
    bottom:15px;
    right:25px;
    width:400px;
}

.loginContentContainer .forgotPassLink {
    float:left;
    margin-top:40px;
}

.loginContentContainer .rememberMe {
    margin-left:95px;
}

.loginContentContainer .rememberMe input {
    width:auto;
    margin:8px 10px 0 0;
}

.loginContentContainer .rememberMe label {
    width:auto;
}

/********************************************************************************
                                LOGIN PAGES END
********************************************************************************/

/********************************************************************************
                                DATE PICKER STARTS
********************************************************************************/

.ui-datepicker .ui-widget-header {
    background:<?php echo $mainColor; ?>;
    color:#fff;
    font-family:signika_negativelight,Arial,sans-serif;
    font-weight:normal;
}

.ui-datepicker th, .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
    font-family:signika_negativelight,Arial,sans-serif !important;
}

.ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
    background:<?php echo $mainColor; ?> !important;
    color:#fff !important;
    border:1px solid #999 !important;
}

.ui-datepicker .ui-state-hover {
    border:none !important;
    top:3px !important;
}

.ui-datepicker .ui-icon {
    background-image: url(../img/ui-icons_ffffff_256x240.png) !important;
}

.ui-datepicker .ui-state-hover .ui-icon {
    background-image: url(../img/ui-icons_222222_256x240.png) !important;
}

/********************************************************************************
                                DATE PICKER ENDS
********************************************************************************/
