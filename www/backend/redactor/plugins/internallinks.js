if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.internallinks = {
	init: function ()
	{           
            /*var pages = new Object();
            pages['page-slug'] = 'page title';
            pages['page-slug2'] = 'page title2';
            */
            var that = this;
            var dropdown = {};

            jQuery.each(pages, function(slug, title)
            {
                dropdown['s' + slug] = { title: title, callback: function() { that.addLink(slug, title); }};
            });

            //dropdown['remove'] = { title: 'Remove font', callback: function() { that.resetFontfamily(); }};

            this.buttonAddFirst('internallink', 'Internal Link', false, dropdown);            
	},
        addLink: function (slug, title)
	{
            var selection = this.getSelectionText();
            var html = '<a href="/' + slug + '">' + selection + '</a>';
            this.exec('insertHtml', html);
        },
        getSelectionText: function()
        {
            var text = "";
            if (window.getSelection)
                text = window.getSelection().toString();
            else if (document.selection && document.selection.type != "Control")
                text = document.selection.createRange().text;

            return text;
        }
};